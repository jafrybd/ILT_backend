const express = require("express");
const isEmpty = require("is-empty");
const router = express.Router();
const moment = require("moment");
const verifyToken = require('../jwt/verify/verifyLpgToken');
const client_model = require('../model/client');
const appointment_model = require('../model/appointment');
const sales_person_model = require('../model/salesPerson');
const reminders_model = require('../model/reminders');

const verifySuperAdminCantAccess = require('../jwt/verify/verifyLpgSuperAdminCantAccess');
const verifySubAdminCantAccess = require('../jwt/verify/verifyLpgSubAdminCantAccess');
const verifySalsesPersonCantAccess = require('../jwt/verify/verifyLpgSalesPersonCantAccess');

const company_sister_concerns_model = require('../model/company_sister_concerns');
const access_corporate_client_model = require('../model/access_corporate_clients');
const access_client_model = require('../model/access_client');
const discussion_type_model = require('../model/discussionType');


/// appointment individual List

router.post(['/search', '/list'], [verifyToken, verifySuperAdminCantAccess, verifySubAdminCantAccess], async (req, res) => {


    /* let salesPersonId = req.decoded.id === undefined ? 0 : req.decoded.id;

    let result = await appointment_model.getAppointmentList(salesPersonId);

    return res.send({
        "error": false,
        "message": "Appointment List.",
        "data": result
    }); */

    let searchField = {};



    if (req.body.client_type_id !== undefined && req.body.client_type_id !== "") {
        searchField.client_type_id = req.body.client_type_id;
    }

    if (req.body.client_id !== undefined && req.body.client_id != "") {
        searchField.client_id = req.body.client_id;
    }

    if (req.body.discussion_type_id !== undefined && req.body.discussion_type_id != "") {
        searchField.discussion_type_id = req.body.discussion_type_id;
    }


    if (req.decoded.user_type == 4) { // if request user is a sales person, he can only access her data
        searchField.sales_person_id = req.decoded.id;
    }

    var d = new Date();

    // Generate Search by date
    if (req.body.from_date !== undefined && req.body.from_date != "") {
        if (req.body.to_date !== undefined && req.body.to_date != "") {
            extraWhere = " appointment_date >= '" + req.body.from_date + "' and appointment_date <= '" + req.body.to_date + "'";
        } else {
            extraWhere = " appointment_date = '" + req.body.from_date + "'";
        }
    } else {
        extraWhere = " appointment_date = '" + d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate() + "'";
    }

    const appointmentList = await appointment_model.getAppointmentList(searchField, extraWhere);




    for (let i = 0; i < appointmentList.length; i++) {

        if (appointmentList[i].client_type_id == 0) {
            appointmentList[i].client_info = await company_sister_concerns_model.getSisterConcernAndCompanyByID(appointmentList[i].client_id);
        } else
            appointmentList[i].client_info = await client_model.getClientByID(appointmentList[i].client_id);

        if (!isEmpty(appointmentList[i].client_info)) { // Delete some data
            if (appointmentList[i].client_info[0].hasOwnProperty("user_id")) delete appointmentList[i].client_info[0].user_id;
            if (appointmentList[i].client_info[0].hasOwnProperty("created_at")) delete appointmentList[i].client_info[0].created_at;
            if (appointmentList[i].client_info[0].hasOwnProperty("active_status")) delete appointmentList[i].client_info[0].active_status;
        }

        delete appointmentList[i].client_id;

    }


    return res.send({
        "error": false,
        "message": "",
        "count": appointmentList.length,
        "data": isEmpty(appointmentList) ? [] : appointmentList
    });


});


router.get('/deActiveList', [verifyToken, verifySalsesPersonCantAccess], async (req, res) => {


    let result = await appointment_model.getDeactiveAppointmentList();

    return res.send({
        "error": false,
        "message": "Deactive Appointment List.",
        "data": result
    });
});

router.get('/create/data', [verifyToken, verifySuperAdminCantAccess, verifySubAdminCantAccess], async (req, res) => {

    let userType = req.decoded.user_type === undefined ? 4 : req.decoded.user_type;
    let salesPersonId = req.decoded.id === undefined ? 0 : req.decoded.id;

    const clientList = await client_model.getList(userType, salesPersonId);
    const corporateClient = await company_sister_concerns_model.getSisterConcernListBySalespersonId(salesPersonId);
    const discussionList = await discussion_type_model.getActiveDiscussionTypeList();

    return res.send({
        "error": false,
        "message": "Client List.",
        "data": {
            "total_client": clientList.length,
            "client_list": clientList,
            "total_corporateClient": corporateClient.length,
            "corporateClient": corporateClient,
            "total_discussionList": discussionList.length,
            "discussionList": discussionList,

        }
    });
});

router.post('/add', [verifyToken, verifySuperAdminCantAccess, verifySubAdminCantAccess], async (req, res) => {

    let reqData = {
        "client_type_id": req.body.client_type_id,
        "client_id": req.body.client_id,
        "appointment_date": req.body.appointment_date,
        "car_number": req.body.car_number,
        "driver_name": req.body.driver_name,
        "discussion_type_id": req.body.discussion_type_id,
        "service_details": req.body.service_details,
        "sales_person_id": req.decoded.id
    }

    let m = moment(reqData.appointment_date, 'YYYY-MM-DD', true);
    if (!m.isValid()) {
        return res.send({
            "error": true,
            "message": "Appointment Date is Invalid"
        });
    }

    let errorMessage = "";
    let isError = 0;

    if (req.decoded.user_type != 4) {
        return res.send({
            "error": true,
            "message": "You are not a sales person."
        });
    }


    if (reqData.sales_person_id == undefined || isEmpty(await sales_person_model.getProfileById(reqData.sales_person_id))) {
        isError = 1;
        errorMessage = "Sales person not found. ";

    }

    if (reqData.client_type_id === undefined) {
        return res.send({
            "error": true,
            "message": "Must give Client type."
        });
    }

    /// Check Client Type  and Verify Client

    if (reqData.client_type_id == 0) {

        /// 
        // check sister_concern is exist
        let sister_concern_details = await company_sister_concerns_model.getSisterConcernByID(reqData.client_id);


        if (reqData.client_id == undefined || isEmpty(sister_concern_details)) {
            isError = 1;
            errorMessage += "Client not found. ";
        } else {


            if (sister_concern_details[0].active_status == 1) {
                return res.send({
                    "error": true,
                    "message": "client already deactive"
                });
            }

            // check sales person can access this sister concern

            let sister_concern_access_details = await access_corporate_client_model.getAccessCorporateClientDataBycompany_sister_concern_idAndsales_person_id(reqData.client_id, reqData.sales_person_id);

            if (isEmpty(sister_concern_access_details)) {
                return res.send({
                    "error": true,
                    "message": "This is not you company Sister concern."
                });
            }
        }

    } else {

        if (reqData.client_id == undefined || isEmpty(await client_model.getClientByID(reqData.client_id))) {
            isError = 1;
            errorMessage += "Client not found. ";
        }

        // check sales person can access this client

        let client_access_details = await access_client_model.getAccessClientDataByclient_idAndsales_person_id(reqData.client_id, reqData.sales_person_id);

        if (isEmpty(client_access_details)) {
            return res.send({
                "error": true,
                "message": "This is not your Client."
            });
        }

    }


    /// Verify Discussion Type ID

    if (reqData.discussion_type_id === undefined || isEmpty(reqData.discussion_type_id)) {
        isError = 1;
        errorMessage += "Must Give a Discussion Type. ";
    } else {
        let existingDiscussionType = await discussion_type_model.getDiscussionTypeByID(reqData.discussion_type_id);

        if (isEmpty(existingDiscussionType)) {
            isError = 1;
            errorMessage += "Discussion Type Not Found  ";
        } else if (existingDiscussionType[0].active_status == 1) {
            isError = 1;
            errorMessage += "Discussion Type is not valid ";

        }
    }

    /// Verify Appointment Date 




    let d = new Date();
    let today = new Date(d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate());

    if (today.getTime() > new Date(reqData.appointment_date).getTime()) {
        isError = 1;
        errorMessage += "Please add a Valid Date.";
    }



    if (isError == 1) {
        return res.send({
            "error": true,
            "message": errorMessage
        });
    }

    let result = await appointment_model.addNewAppointment(reqData);

    if (result.affectedRows == undefined || result.affectedRows < 1) {
        return res.send({
            "error": true,
            "message": "Some thing Wrong in system database."
        });
    }

    return res.send({
        "error": false,
        "message": "Appointment Added Successfully ."
    });

});

router.post('/update', [verifyToken, verifySuperAdminCantAccess, verifySubAdminCantAccess], async (req, res) => {

    let updateRequestData = {
        "id": req.body.id,
        "client_type_id": req.body.client_type_id,
        "client_id": req.body.client_id,
        "appointment_date": req.body.appointment_date,
        "car_number": req.body.car_number,
        "driver_name": req.body.driver_name,
        "discussion_type_id": req.body.discussion_type_id,
        "service_details": req.body.service_details,
        "sales_person_id": req.decoded.id
    }
    let m = moment(updateRequestData.appointment_date, 'YYYY-MM-DD', true);
    if (!m.isValid()) {
        return res.send({
            "error": true,
            "message": "Appointment Date is Invalid"
        });
    }

    let updateData = {};
    let pointerClientTypeChange = false;
    let errorMessage = "";
    let isError = 0; // 0 = No
    let willWeUpdate = 1; // 0 = yes;

    const existingInfo = await appointment_model.getAppointmentByID(updateRequestData.id);

    if (isEmpty(existingInfo)) {
        return res.send({
            "error": true,
            "message": " Not Exist."
        });
    }



    if (existingInfo[0].car_number !== updateRequestData.car_number) {
        if (updateRequestData.car_number === undefined) {
            updateData.car_number = "";
        } else {
            updateData.car_number = updateRequestData.car_number;
        }
        willWeUpdate = 0;


    }


    if (existingInfo[0].driver_name !== updateRequestData.driver_name) {
        if (updateRequestData.driver_name === undefined) {
            updateData.driver_name = "";
        } else {
            updateData.driver_name = updateRequestData.driver_name;
        }

        willWeUpdate = 0;
    }


    if (existingInfo[0].service_details !== updateRequestData.service_details) {
        if (updateRequestData.service_details == undefined || isEmpty(updateRequestData.service_details)) {
            isError = 1;
            errorMessage += "Service Details Should be Added. ";

        } else {
            willWeUpdate = 0;
            updateData.service_details = updateRequestData.service_details;
        }
    }

    if (existingInfo[0].appointment_date !== updateRequestData.appointment_date) {

        var d = new Date();
        var today = new Date(d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate());

        if (today.getTime() > new Date(updateRequestData.appointment_date).getTime()) {
            isError = 1;
            errorMessage += "Please add a Valid Date.";
        } else {
            willWeUpdate = 0;
            updateData.appointment_date = updateRequestData.appointment_date;

        }
    }

    if (existingInfo[0].discussion_type_id !== updateRequestData.discussion_type_id) {

        if (updateRequestData.discussion_type_id == undefined || isEmpty(updateRequestData.discussion_type_id)) {
            isError = 1;
            errorMessage += "Discussion Type Should not be Empty. ";

        } else {
            let existingDiscussionType = await discussion_type_model.getDiscussionTypeByID(updateRequestData.discussion_type_id);

            if (isEmpty(existingDiscussionType)) {
                isError = 1;
                errorMessage += " Discussion Type  not found.";
            } else if (existingDiscussionType[0].active_status == 1) {
                isError = 1;
                errorMessage += " Discussion Type  not Activated.";
            } else {
                willWeUpdate = 0;
                updateData.discussion_type_id = updateRequestData.discussion_type_id;
            }
        }

    }

    if (updateRequestData.client_type_id === undefined) {
        return res.send({
            "error": true,
            "message": "Must give Client type."
        });
    }

    if (existingInfo[0].client_type_id != updateRequestData.client_type_id) {
        updateData.client_type_id = updateRequestData.client_type_id;
        pointerClientTypeChange = true;

    }

    if (existingInfo[0].client_id != updateRequestData.client_id || pointerClientTypeChange === true) {

        if (updateRequestData.client_type_id == 0) {

            // check sister_concern is exist
            let sister_concern_details = await company_sister_concerns_model.getSisterConcernByID(updateRequestData.client_id);

            if (updateRequestData.client_id == undefined || isEmpty(sister_concern_details)) {
                return res.send({
                    "error": true,
                    "message": "Client not found. "
                });
            }

            //console.log(sister_concern_details);

            if (sister_concern_details[0].active_status == 1) {
                return res.send({
                    "error": true,
                    "message": "client already deactive"
                });
            }

            // check sales person can access this sister concern

            let sister_concern_access_details = await access_corporate_client_model.getAccessCorporateClientDataBycompany_sister_concern_idAndsales_person_id(updateRequestData.client_id, updateRequestData.sales_person_id);

            if (isEmpty(sister_concern_access_details)) {
                return res.send({
                    "error": true,
                    "message": "This is not your company Sister concern."
                });
            }
            updateData.client_id = updateRequestData.client_id;

        } else {

            if (updateRequestData.client_id == undefined || isEmpty(await client_model.getClientByID(updateRequestData.client_id))) {
                isError = 1;
                errorMessage += "Client not found. ";
            }

            // check sales person can access this client
            let client_access_details = await access_client_model.getAccessClientDataByclient_idAndsales_person_id(updateRequestData.client_id, updateRequestData.sales_person_id);

            if (isEmpty(client_access_details)) {
                return res.send({
                    "error": true,
                    "message": "This is not your Client."
                });
            }

        }
        updateData.client_id = updateRequestData.client_id;
    }


    if (isError == 1) {
        return res.send({
            "error": true,
            "message": errorMessage
        });
    }



    let result = {};
    if (willWeUpdate == 0) {
        result = await appointment_model.updateAppointmentByID(updateRequestData.id, updateData);
    }

    if (!isEmpty(result) && result.affectedRows == 0) {
        return res.send({
            "error": true,
            "message": "Update Fail try again"
        });
    } else {
        return res.send({
            "error": false,
            "message": "Update Successfully Done"
        });
    }

});


router.get('/details/:id', [verifyToken, verifySuperAdminCantAccess, verifySubAdminCantAccess], async (req, res) => {

    const id = req.params.id;
    const appointmentDetails = await appointment_model.getAppointmentByID(id);

    if (isEmpty(appointmentDetails)) {
        return res.send({
            "error": true,
            "message": "Appointment Not Exist."
        });
    } else {
        if (appointmentDetails[0].client_type_id !== 0) {

            let nonCorporateDetails = await appointment_model.getAppointmentDetailsByIDandClientType(id, appointmentDetails[0].client_type_id);
            if (nonCorporateDetails[0].active_status === 1) {
                return res.send({
                    "error": false,
                    "message": nonCorporateDetails
                });
            } else {
                delete nonCorporateDetails[0].active_status;
                delete nonCorporateDetails[0].reason_for_delete;

                return res.send({
                    "error": false,
                    "message": nonCorporateDetails
                });
            }

        } else if (appointmentDetails[0].client_type_id == 0) {
            let corporateDetails = await appointment_model.getAppointmentDetailsByIDandClientType(id, appointmentDetails[0].client_type_id);
            if (corporateDetails[0].active_status === 1) {
                return res.send({
                    "error": false,
                    "message": corporateDetails
                });
            } else {
                delete corporateDetails[0].active_status;
                delete corporateDetails[0].reason_for_delete;
                return res.send({
                    "error": false,
                    "message": corporateDetails
                });
            }

        }



        delete appointmentDetails[0].complete_time;
        return res.send({
            "error": false,
            "message": "Appointment Info Found.",
            "data": appointmentDetails[0]
        });
    }
});

router.post('/delete', [verifyToken, verifySuperAdminCantAccess, verifySubAdminCantAccess], async (req, res) => {


    let deactiveRequestData = {
        "id": req.body.id,
        "reason_for_delete": req.body.reason_for_delete

    }

    let updateData = {};
    let willWeUpdate = 1; // 0 = yes;


    let existingData = await appointment_model.getAppointmentByID(deactiveRequestData.id);


    if (isEmpty(existingData)) {
        return res.send({
            "error": true,
            "message": "Appointment ID Not Exist."
        });
    } else {

        if (existingData[0].reason_for_delete !== deactiveRequestData.reason_for_delete) {
            if (deactiveRequestData.reason_for_delete == undefined || deactiveRequestData.reason_for_delete == "") {
                return res.send({
                    "error": true,
                    "message": "Must Fill the reason behind deleting this appointment"
                });
            } else {
                updateData.reason_for_delete = deactiveRequestData.reason_for_delete;
            }
            willWeUpdate = 0;


        }
    }

    if (existingData[0].client_type_id !== 0) {
        var nonCorporateDetails = await appointment_model.getAppointmentDetailsByIDandClientType(deactiveRequestData.id, existingData[0].client_type_id);

    } else if (existingData[0].client_type_id == 0) {
        var corporateDetails = await appointment_model.getAppointmentDetailsByIDandClientType(deactiveRequestData.id, existingData[0].client_type_id);

    }

    let result = {};
    if (willWeUpdate == 0) {
        result = await appointment_model.deactiveAppointment(deactiveRequestData.id, updateData);
    }


    if (!isEmpty(result) && result.affectedRows == 0) {
        return res.send({
            "error": true,
            "message": "Deactive Fail try again"
        });
    } else {
        if (existingData[0].client_type_id !== 0) {
            return res.send({
                "error": false,
                "message": `Appointment With '${nonCorporateDetails[0].ClientName}' Deleted.`
            });
        } else {
            return res.send({
                "error": false,
                "message": `Appointment With '${corporateDetails[0].Company}-${corporateDetails[0].SisterConcern}' Deleted.`
            });
        }


    }
});

router.post(['/searchActive', '/activeList'], verifyToken, async (req, res) => {

    let searchField = {};


    if (req.body.sales_person_id !== undefined && req.body.sales_person_id != "") {
        searchField.sales_person_id = req.body.sales_person_id;
    }

    if (req.body.client_type_id !== undefined && req.body.client_type_id !== "") {
        searchField.client_type_id = req.body.client_type_id;
    }

    if (req.body.client_id !== undefined && req.body.client_id != "") {
        searchField.client_id = req.body.client_id;
    }

    if (req.body.discussion_type_id !== undefined && req.body.discussion_type_id != "") {
        searchField.discussion_type_id = req.body.discussion_type_id;
    }


    if (req.decoded.user_type == 4) { // if request user is a sales person, he can only access her data
        searchField.sales_person_id = req.decoded.id;
    }

    var d = new Date();

    // Generate Search by date
    if (req.body.from_date !== undefined && req.body.from_date != "") {
        if (req.body.to_date !== undefined && req.body.to_date != "") {
            extraWhere = " appointment_date >= '" + req.body.from_date + "' and appointment_date <= '" + req.body.to_date + "'";
        } else {
            extraWhere = " appointment_date = '" + req.body.from_date + "'";
        }
    } else {
        extraWhere = " appointment_date = '" + d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate() + "'";
    }

    const appointmentList = await appointment_model.getActiveAppointmentListBySearching(searchField, extraWhere);




    for (let i = 0; i < appointmentList.length; i++) {

        if (appointmentList[i].client_type_id == 0) {
            appointmentList[i].client_info = await company_sister_concerns_model.getSisterConcernAndCompanyByID(appointmentList[i].client_id);
        } else
            appointmentList[i].client_info = await client_model.getClientByID(appointmentList[i].client_id);

        if (!isEmpty(appointmentList[i].client_info)) { // Delete some data
            if (appointmentList[i].client_info[0].hasOwnProperty("user_id")) delete appointmentList[i].client_info[0].user_id;
            if (appointmentList[i].client_info[0].hasOwnProperty("created_at")) delete appointmentList[i].client_info[0].created_at;
            if (appointmentList[i].client_info[0].hasOwnProperty("active_status")) delete appointmentList[i].client_info[0].active_status;
        }

        delete appointmentList[i].client_id;

    }

    return res.send({
        "error": false,
        "message": "",
        "count": appointmentList.length,
        "data": isEmpty(appointmentList) ? [] : appointmentList
    });



});

router.post(['/searchDeactive', '/deactiveList'], verifyToken, async (req, res) => {

    let searchField = {};


    if (req.body.sales_person_id !== undefined && req.body.sales_person_id != "") {
        searchField.sales_person_id = req.body.sales_person_id;
    }

    if (req.body.client_type_id !== undefined && req.body.client_type_id !== "") {
        searchField.client_type_id = req.body.client_type_id;
    }

    if (req.body.client_id !== undefined && req.body.client_id != "") {
        searchField.client_id = req.body.client_id;
    }

    if (req.body.discussion_type_id !== undefined && req.body.discussion_type_id != "") {
        searchField.discussion_type_id = req.body.discussion_type_id;
    }



    if (req.decoded.user_type == 4) { // if request user is a sales person, he can only access her data
        searchField.sales_person_id = req.decoded.id;
    }



    var d = new Date();

    // Generate Search by date
    if (req.body.from_date !== undefined && req.body.from_date != "") {
        if (req.body.to_date !== undefined && req.body.to_date != "") {
            extraWhere = " appointment_date >= '" + req.body.from_date + "' and appointment_date <= '" + req.body.to_date + "'";
        } else {
            extraWhere = " appointment_date = '" + req.body.from_date + "'";
        }
    } else {
        extraWhere = " appointment_date = '" + d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate() + "'";
    }

    const deactiveAppointmentList = await appointment_model.getDeactiveAppointmentListBySearching(searchField, extraWhere);

    for (let i = 0; i < deactiveAppointmentList.length; i++) {

        if (deactiveAppointmentList[i].client_type_id == 0) {
            deactiveAppointmentList[i].client_info = await company_sister_concerns_model.getSisterConcernAndCompanyByID(deactiveAppointmentList[i].client_id);
        } else
            deactiveAppointmentList[i].client_info = await client_model.getClientByID(deactiveAppointmentList[i].client_id);

        if (!isEmpty(deactiveAppointmentList[i].client_info)) { // Delete some data
            if (deactiveAppointmentList[i].client_info[0].hasOwnProperty("user_id")) delete deactiveAppointmentList[i].client_info[0].user_id;
            if (deactiveAppointmentList[i].client_info[0].hasOwnProperty("created_at")) delete deactiveAppointmentList[i].client_info[0].created_at;
            if (deactiveAppointmentList[i].client_info[0].hasOwnProperty("active_status")) delete deactiveAppointmentList[i].client_info[0].active_status;
        }

        delete deactiveAppointmentList[i].client_id;

    }

    return res.send({
        "error": false,
        "message": "",
        "count": deactiveAppointmentList.length,
        "data": isEmpty(deactiveAppointmentList) ? [] : deactiveAppointmentList
    });

});


router.post('/complete', [verifyToken, verifySuperAdminCantAccess, verifySubAdminCantAccess], async (req, res) => {

    let salesPersonId = req.decoded.id === undefined ? 0 : req.decoded.id;
    let apponitmentId = req.body.apponitment_id;

    let appintmentInfo = await appointment_model.getAppointmentByID(apponitmentId);

    if (isEmpty(appintmentInfo)) {
        return res.send({
            "error": true,
            "message": "Appointment ID Not Exist."
        });
    }
    console.log(appintmentInfo);
    if (appintmentInfo[0].sales_person_id != salesPersonId) {
        return res.send({
            "error": true,
            "message": "Unauthorize access request."
        });
    }

    if (appintmentInfo[0].status === "complete") {
        return res.send({
            "error": true,
            "message": "Appointment Already Complete."
        });
    }

    var d = new Date();

    let updateData = {
        "status": "complete",
        "complete_time": d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate()
    }

    let result = await appointment_model.updateAppointmentByID(apponitmentId, updateData);

    if (isEmpty(result)) {
        return res.send({
            "error": true,
            "message": "Update Fail try again"
        });
    }

    // Generate Reminder Strat
    let discussionTypeInfo = await discussion_type_model.getDiscussionTypeByID(appintmentInfo[0].discussion_type_id);

    if (isEmpty(discussionTypeInfo)) {
        return res.send({
            "error": true,
            "message": "Appointment Complete Done But Dicsussion Type not found so that Reminders Not Create."
        });
    }


    let [service_year, service_month, service_day] = discussionTypeInfo[0].service_time_period.split('_');
    let [reminder_year, reminder_month, reminder_day] = discussionTypeInfo[0].reminder_time_period.split('_');

    try {
        service_day = parseInt(service_day)
    } catch (error) {
        service_day = 0;
    }
    try {
        service_month = parseInt(service_month)
    } catch (error) {
        service_month = 0;
    }
    try {
        service_year = parseInt(service_year)
    } catch (error) {
        service_year = 0;
    }

    try {
        reminder_day = parseInt(reminder_day)
    } catch (error) {
        reminder_day = 0;
    }
    try {
        reminder_month = parseInt(reminder_month)
    } catch (error) {
        reminder_month = 0;
    }
    try {
        reminder_year = parseInt(reminder_year)
    } catch (error) {
        reminder_year = 0;
    }


    // Set reminder_end_date 
    let reminder_end_date = new Date();
    if (service_day > 0) {
        reminder_end_date.setDate(reminder_end_date.getDate() + service_day);
    }
    if (service_month > 0) {
        reminder_end_date.setMonth(reminder_end_date.getMonth() + (service_month));
    }
    if (service_year > 0) {
        reminder_end_date.setFullYear(reminder_end_date.getFullYear() + service_year);
    }


    const final_reminder_end_date = reminder_end_date.getUTCFullYear() + '-' +
        ('00' + (reminder_end_date.getUTCMonth() + 1)).slice(-2) + '-' +
        ('00' + reminder_end_date.getUTCDate()).slice(-2);


    // Set reminder_start_date 
    let reminder_start_date = reminder_end_date; // May be it's take reference.
    if (reminder_day > 0) {
        reminder_start_date.setDate(reminder_start_date.getDate() - reminder_day);
    }

    if (reminder_month > 0) {
        reminder_start_date.setMonth(reminder_start_date.getMonth() - reminder_month);
    }

    if (reminder_year > 0) {
        reminder_start_date.setFullYear(reminder_start_date.getFullYear() - reminder_year);
    }


    const final_reminder_start_date = reminder_start_date.getUTCFullYear() + '-' +
        ('00' + (reminder_start_date.getUTCMonth() + 1)).slice(-2) + '-' +
        ('00' + reminder_start_date.getUTCDate()).slice(-2);

    let final_reminder_time_period = (service_year - reminder_year) + "_" + (service_month - reminder_month) + "_" + (service_day - reminder_day);
    //  final_reminder_time_period need logic change.

    const new_reminders = {
        "appointment_id": apponitmentId,
        "sales_person_id": salesPersonId,
        "reminder_start_date": final_reminder_start_date,
        "reminder_end_date": final_reminder_end_date,
        "reminder_time_period": final_reminder_time_period,
        "is_created_task": 0
    }

    let result_create_reminder = await reminders_model.addNewReminder(new_reminders);

    // Generate Reminder End

    if (isEmpty(result_create_reminder)) {
        return res.send({
            "error": true,
            "message": "Reminder Create Failed!!"
        });
    } else {
        return res.send({
            "error": false,
            "message": "Appointment Completed Successfully!!!"
        });
    }
});


router.get('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "error": true
    })
});

router.post('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "error": true
    })
});

module.exports = router;