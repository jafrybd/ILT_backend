const express = require("express");
const isEmpty = require("is-empty");
const router = express.Router();
const moment = require("moment");
const verifyToken = require('../jwt/verify/verifyLpgToken');
const client_model = require('../model/client');
const task_model = require('../model/task');
const sales_person_model = require('../model/salesPerson');
const access_client_model = require('../model/access_client');
const task_discussion_types_model = require('../model/task_discussion_types');
const task_vehicle_types_model = require('../model/task_vehicle_types');
const call_time_model = require('../model/callTime');
const discussion_type_model = require('../model/discussionType');
const leads_model = require('../model/leads');
const vehicle_type_model = require('../model/vehicleType');
const prospect_type_model = require('../model/prospectType');
const company_sister_concerns_model = require('../model/company_sister_concerns');
const access_corporate_client_model = require('../model/access_corporate_clients');

const query = require('../query/lpg');


const verifySalesPersonCantAccess = require('../jwt/verify/verifyLpgSalesPersonCantAccess');
const verifySuperAdminCantAccess = require('../jwt/verify/verifyLpgSuperAdminCantAccess');
const verifySubAdminCantAccess = require('../jwt/verify/verifyLpgSubAdminCantAccess');

router.post('/incompletTaskList', [verifyToken, verifySalesPersonCantAccess], async (req, res) => {


    let salesPersonId = req.body.sales_person_id;

    const existingSalesPerson = await sales_person_model.getProfileById2(salesPersonId);

    if (isEmpty(existingSalesPerson)) {
        return res.send({
            "error": true,
            "message": "Sales Person Not Exist."
        });
    }

    let taskList = await task_model.getIncompleteTaskListById(salesPersonId);

    for (let i = 0; i < taskList.length; i++) {

        if (taskList[i].client_type_id == 0) {
            taskList[i].client_info = await company_sister_concerns_model.getSisterConcernAndCompanyByID(taskList[i].client_id);
        } else {
            taskList[i].client_info = await client_model.getClientByID(taskList[i].client_id);
        }

        if (!isEmpty(taskList[i].client_info)) { // Delete some data
            if (taskList[i].client_info[0].hasOwnProperty("user_id")) delete taskList[i].client_info[0].user_id;
            if (taskList[i].client_info[0].hasOwnProperty("created_at")) delete taskList[i].client_info[0].created_at;
            if (taskList[i].client_info[0].hasOwnProperty("active_status")) delete taskList[i].client_info[0].active_status;
        }


        taskList[i].discussion_type = await task_discussion_types_model.getExistingDiscussion_TypeNameAndIdListByTask_id(taskList[i].id);
        taskList[i].vehicle_type = await task_vehicle_types_model.getExistingVehicle_TypeNameAndIdListByTask_id(taskList[i].id);

        delete taskList[i].client_id;

    }

    return res.send({
        "error": false,
        "message": "Sales Person Incomplete Task",
        "data": isEmpty(taskList) ? [] : taskList
    });
});


router.post('/add', [verifyToken, verifySuperAdminCantAccess, verifySubAdminCantAccess], async (req, res) => {

    let reqData = {
        "client_id": req.body.client_id,
        "client_type_id": req.body.client_type_id,
        "discussion_type_id": req.body.discussion_type,
        "vehicle_type_id": req.body.vehicle_type,
        // "call_time_type_id": req.body.call_time_type,
        "prospect_type_id": req.body.prospect_type,
        "sales_person_id": req.decoded.id,
        "created_by": req.decoded.id,
        "call_date": req.body.call_date,
        "lead_id": req.body.lead,
        "call_time": req.body.call_time,
        "active_status": 0
    }
    let m = moment(reqData.call_date, 'YYYY-MM-DD', true);
    if (!m.isValid()) {
        return res.send({
            "error": true,
            "message": "Task Date is Invalid"
        });
    }


    let newTaskData = {
        "client_id": reqData.client_id,
        "client_type_id": reqData.client_type_id,
        "prospect_type_id": reqData.prospect_type_id,
        "sales_person_id": req.decoded.id,
        "created_by": req.decoded.id,
        "call_date": reqData.call_date,
        "lead_id": reqData.lead_id,
        "call_time": reqData.call_time,
        "active_status": 0
    }

    let n = moment(newTaskData.call_date, 'YYYY-MM-DD', true);
    if (!n.isValid()) {
        return res.send({
            "error": true,
            "message": "Task Date is Invalid"
        });
    }



    if (req.decoded.user_type != 4) {
        return res.send({
            "error": true,
            "message": "You are not a sales person."
        });
    }

    let errorMessage = "";
    let isError = 0;

    if (reqData.client_type_id === undefined) {
        return res.send({
            "error": true,
            "message": "Must give Client type."
        });
    }

    if (reqData.sales_person_id == undefined || isEmpty(await sales_person_model.getProfileById(reqData.sales_person_id))) {
        isError = 1;
        errorMessage += "Sales person not found. ";
    }

    if (reqData.client_type_id == 0) {

        // check sister_concern is exist
        let sister_concern_details = await company_sister_concerns_model.getSisterConcernByID(reqData.client_id);

        if (reqData.client_id == undefined || isEmpty(sister_concern_details)) {
            isError = 1;
            errorMessage += "Client not found. ";
        } else {
            if (sister_concern_details[0].active_status == 1) {
                return res.send({
                    "error": true,
                    "message": "client already deactive"
                });
            }

            // check sales person can access this sister concern

            let sister_concern_access_details = await access_corporate_client_model.getAccessCorporateClientDataBycompany_sister_concern_idAndsales_person_id(reqData.client_id, reqData.sales_person_id);

            if (isEmpty(sister_concern_access_details)) {
                return res.send({
                    "error": true,
                    "message": "This is not you company Sister concer."
                });
            }
        }

    } else {
        if (reqData.client_id == undefined || isEmpty(await client_model.getClientByID(reqData.client_id))) {
            isError = 1;
            errorMessage += "Client not found. ";
        }

        // check sales person can access this client

        let client_access_details = await access_client_model.getAccessClientDataByclient_idAndsales_person_id(reqData.client_id, reqData.sales_person_id);

        if (isEmpty(client_access_details)) {
            return res.send({
                "error": true,
                "message": "This is not your Client."
            });
        }
    }


    // check discussion type
    if (reqData.discussion_type_id == undefined) {
        isError = 1;
        errorMessage += "Discussion type not found. ";
    } else {
        // check discussion type is array
        if (Array.isArray(reqData.discussion_type_id) && reqData.discussion_type_id.length > 0) {

            // check discussion type, is all type active
            for (let i = 0; i < reqData.discussion_type_id.length; i++) {
                let check_discussion_type = await discussion_type_model.getDiscussionTypeByID(reqData.discussion_type_id[i]);

                if (isEmpty(check_discussion_type) || check_discussion_type[0].active_status == 1) {
                    isError = 1;
                    errorMessage += `Discussion type id ${reqData.discussion_type_id[i]} is not exist.`;
                }
            }
        } else {
            isError = 1;
            errorMessage += "Must Select a Discussion Type. ";
        }
    }



    // check vehicle type
    if (reqData.vehicle_type_id == undefined) {
        isError = 1;
        errorMessage += "Vehicle type not found. ";
    } else {
        // check vehicle type is array
        if (Array.isArray(reqData.vehicle_type_id) && reqData.vehicle_type_id.length > 0) {

            // check vehicle type, is all type active ?

            for (let i = 0; i < reqData.vehicle_type_id.length; i++) {
                let check_vehicle_type = await vehicle_type_model.getVehicleTypeByID(reqData.vehicle_type_id[i]);

                if (isEmpty(check_vehicle_type) || check_vehicle_type[0].active_status == 1) {
                    isError = 1;
                    errorMessage += `Vehicle type id ${reqData.vehicle_type_id[i]} not exist. `;
                }
            }
        } else {
            isError = 1;
            errorMessage += "Must Select a Vehicle Type. ";
        }
    }



    // let check_vehicle_type = await vehicle_type_model.getVehicleTypeByID(reqData.vehicle_type_id);

    // if (reqData.vehicle_type_id == undefined || isEmpty(check_vehicle_type) || check_vehicle_type[0].active_status === 1) {
    //     isError = 1;
    //     errorMessage += "Vehicle type not found. ";
    // }

    /*let check_call_time_type = await call_time_model.getCallTimeByID(reqData.call_time_type_id);

    if (reqData.call_time_type_id == undefined || isEmpty(check_call_time_type) || check_call_time_type[0].active_status === 1) {
        isError = 1;
        errorMessage += "Call type not found. ";
    }*/

    // if( reqData.prospect_type_id == undefined || isEmpty(await prospect_type_model.getProspectTypeByID(reqData.prospect_type_id))) {
    //     isError = 1;
    //     errorMessage = "Prospect type not found. ";
    // }

    var d = new Date();
    var today = new Date(d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate());

    if (today.getTime() > new Date(reqData.call_date).getTime()) {
        isError = 1;
        errorMessage += "Please add a day which is greater then yesterday.";
    }

    if (reqData.call_time == undefined || isEmpty(reqData.call_time)) {
        isError = 1;
        errorMessage += "Time Should be Added. ";

    }


    let check_lead = await leads_model.getLeadByID(reqData.lead_id);
    if (reqData.lead_id == undefined || isEmpty(check_lead) || check_lead[0].active_status === 1) {
        isError = 1;
        errorMessage += "Leads not found. ";
    }

    let check_prospect_type = await prospect_type_model.getProspectTypeByID(reqData.prospect_type_id);

    if (reqData.prospect_type_id == undefined || reqData.prospect_type_id === 0) {
        newTaskData.prospect_type_id = 0;
    } else {
        if (isEmpty(check_prospect_type) || check_prospect_type[0].active_status === 1) {
            isError = 1;
            errorMessage += "Prospect Type not found. ";
        }
    }




    if (isError == 1) {
        return res.send({
            "error": true,
            "message": errorMessage
        });
    }



    // return res.send({
    //     "error": true,
    //     "message": "it's come.",
    //     "errorMessage": newTaskData
    // });


    let result = await task_model.addNewTask(newTaskData);

    if (result.affectedRows == undefined || result.affectedRows < 1) {
        return res.send({
            "error": true,
            "message": "Some thing Wrong in system database."
        });
    }

    let taskId = result.insertId;

    let newTaskDiscussion_Type = {
        "task_id": taskId,
        "discussion_type_id": 0
    }

    for (let i = 0; i < reqData.discussion_type_id.length; i++) {

        newTaskDiscussion_Type.discussion_type_id = reqData.discussion_type_id[i];
        result = await task_discussion_types_model.addNew(newTaskDiscussion_Type);

        if (result.affectedRows == undefined || result.affectedRows < 1) {
            return res.send({
                "error": true,
                "message": "Some thing Wrong in system database. [Discussion]. "
            });
        }
    }

    let newTaskVehicle_Type = {
        "task_id": taskId,
        "vehicle_type_id": 0
    }

    for (let i = 0; i < reqData.vehicle_type_id.length; i++) {

        newTaskVehicle_Type.vehicle_type_id = reqData.vehicle_type_id[i];
        result = await task_vehicle_types_model.addNew(newTaskVehicle_Type);

        if (result.affectedRows == undefined || result.affectedRows < 1) {
            return res.send({
                "error": true,
                "message": "Some thing Wrong in system database. [Vehical]. "
            });
        }
    }

    return res.send({
        "error": false,
        "message": "Task Added Successfully Done."
    });

});


router.post('/update', [verifyToken, verifySuperAdminCantAccess, verifySubAdminCantAccess], async (req, res) => {

    let updateRequestData = {
        "id": req.body.id,
        "client_id": req.body.client_id,
        "client_type_id": req.body.client_type_id,
        "discussion_type_id": req.body.discussion_type_id,
        "vehicle_type_id": req.body.vehicle_type_id,
        //"call_time_type_id": req.body.call_time_type_id,
        "prospect_type_id": req.body.prospect_type,
        "call_date": req.body.call_date,
        "call_time": req.body.call_time,
        "lead_id": req.body.lead_id
    }

    let m = moment(updateRequestData.call_date, 'YYYY-MM-DD', true);
    if (!m.isValid()) {
        return res.send({
            "error": true,
            "message": "Task Date is Invalid"
        });
    }


    let userType = req.decoded.user_type === undefined ? 0 : req.decoded.user_type;
    let salesPersonId = req.decoded.id === undefined ? 0 : req.decoded.id;


    let updateData = {};

    let errorMessage = "";
    let isError = 0; // 0 = No
    let willWeUpdate = 1; // 0 = yes;
    let pointerClientTypeChange = false;

    const existingInfo = await task_model.getTaskByID(updateRequestData.id, userType, salesPersonId);

    if (updateRequestData.client_type_id === undefined) {
        return res.send({
            "error": true,
            "message": "Must give Client type."
        });
    }

    if (isEmpty(existingInfo)) {
        return res.send({
            "error": true,
            "message": "Unknown Task"
        });
    }


    if (existingInfo[0].client_type_id != updateRequestData.client_type_id) {
        updateData.client_type_id = updateRequestData.client_type_id;
        pointerClientTypeChange = true;
    }


    /// Client ID check
    if (existingInfo[0].client_id != updateRequestData.client_id || pointerClientTypeChange === true) {

        if (updateRequestData.client_type_id == 0) {

            // check sister_concern is exist
            let sister_concern_details = await company_sister_concerns_model.getSisterConcernByID(updateRequestData.client_id);

            if (updateRequestData.client_id == undefined || isEmpty(sister_concern_details)) {
                return res.send({
                    "error": true,
                    "message": "Client not found. "
                });
            }

            console.log(sister_concern_details);

            if (sister_concern_details[0].active_status == 1) {
                return res.send({
                    "error": true,
                    "message": "client already deactive"
                });
            }

            // check sales person can access this sister concern

            let sister_concern_access_details = await access_corporate_client_model.getAccessCorporateClientDataBycompany_sister_concern_idAndsales_person_id(updateRequestData.client_id, salesPersonId);

            if (isEmpty(sister_concern_access_details)) {
                return res.send({
                    "error": true,
                    "message": "This is not you company Sister concer."
                });
            }

        } else {

            if (updateRequestData.client_id == undefined || isEmpty(await client_model.getClientByID(updateRequestData.client_id))) {
                isError = 1;
                errorMessage += "Client not found. ";
            }

            // check sales person can access this client
            let client_access_details = await access_client_model.getAccessClientDataByclient_idAndsales_person_id(updateRequestData.client_id, salesPersonId);

            if (isEmpty(client_access_details)) {
                return res.send({
                    "error": true,
                    "message": "This is not your Client."
                });
            }
        }
        updateData.client_id = updateRequestData.client_id;
    }


    if ((updateRequestData.call_date !== undefined) && (existingInfo[0].call_date !== updateRequestData.call_date)) {

        var d = new Date();
        var today = new Date(d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate());

        if (today.getTime() > new Date(updateRequestData.call_date).getTime()) {
            isError = 1;
            errorMessage += "Please add a Valid Date.";
        } else {
            willWeUpdate = 0;
            updateData.call_date = updateRequestData.call_date;

        }
    }

    // check discussion type

    if (updateRequestData.discussion_type_id == undefined) {
        isError = 1;
        errorMessage += "Discussion type not found. ";
    } else {
        // check discussion type is array
        if (Array.isArray(updateRequestData.discussion_type_id) && updateRequestData.discussion_type_id.length > 0) {

            // check discussion type, is all type active
            for (let i = 0; i < updateRequestData.discussion_type_id.length; i++) {
                let check_discussion_type = await discussion_type_model.getDiscussionTypeByID(updateRequestData.discussion_type_id[i]);

                if (isEmpty(check_discussion_type) || check_discussion_type[0].active_status == 1) {
                    isError = 1;
                    errorMessage += `Discussion type id ${updateRequestData.discussion_type_id[i]} is not exist.`;
                }
            }
        } else {
            isError = 1;
            errorMessage += "Must Select a Discussion Type. ";
        }
    }

    /// Vehicle Type Check

    if (updateRequestData.vehicle_type_id == undefined) {
        isError = 1;
        errorMessage += "Vehicle type not found. ";
    } else {
        // check vehicle type is array
        if (Array.isArray(updateRequestData.vehicle_type_id) && updateRequestData.vehicle_type_id.length > 0) {

            // check vehicle type, is all type active ?

            for (let i = 0; i < updateRequestData.vehicle_type_id.length; i++) {
                let check_vehicle_type = await vehicle_type_model.getVehicleTypeByID(updateRequestData.vehicle_type_id[i]);

                if (isEmpty(check_vehicle_type) || check_vehicle_type[0].active_status == 1) {
                    isError = 1;
                    errorMessage += `Vehicle type id ${updateRequestData.vehicle_type_id[i]} not exist. `;
                }
            }
        } else {
            isError = 1;
            errorMessage += "Must Select a Vehicle Type. ";
        }
    }



    if (existingInfo[0].call_time !== updateRequestData.call_time) {
        if (updateRequestData.call_time == undefined || isEmpty(updateRequestData.call_time)) {
            isError = 1;
            errorMessage += "Time Should be Added. ";

        } else {
            willWeUpdate = 0;
            updateData.call_time = updateRequestData.call_time;
        }
    }

    if ((updateRequestData.prospect_type_id !== undefined) && (existingInfo[0].prospect_type_id !== updateRequestData.prospect_type_id)) {


        if (updateRequestData.prospect_type_id == undefined || isEmpty(updateRequestData.prospect_type_id)) {
            isError = 1;
            errorMessage += "Prospect Should not be Empty. ";

        } else {
            let prospectInfo = await prospect_type_model.getProspectTypeByID(updateRequestData.prospect_type_id);

            if (isEmpty(prospectInfo) || (prospectInfo[0].active_status === 1)) {
                isError = 1;
                errorMessage += " Prospect  not found.";
            } else {
                willWeUpdate = 0;
                updateData.prospect_type_id = updateRequestData.prospect_type_id;

            }
        }

    }

    if ((updateRequestData.lead_id !== undefined) && (existingInfo[0].lead_id !== updateRequestData.lead_id)) {

        if (updateRequestData.lead_id == undefined || isEmpty(updateRequestData.lead_id)) {
            isError = 1;
            errorMessage += "Lead Should not be Empty. ";

        } else {
            let leadInfo = await leads_model.getLeadByID(updateRequestData.lead_id);

            if (isEmpty(leadInfo) || (leadInfo[0].active_status === 1)) {
                isError = 1;
                errorMessage += " Lead  not found.";
            } else {
                willWeUpdate = 0;
                updateData.lead_id = updateRequestData.lead_id;

            }
        }

    }

    if (isError == 1) {
        return res.send({
            "error": true,
            "message": errorMessage
        });
    }

    let result = {};
    if (willWeUpdate == 0) {
        result = await task_model.updateTaskByID(updateRequestData.id, updateData);
    }

    /// Check with task_discussion_type Table

    let existing_task_discussion_list = await task_discussion_types_model.getExistingDiscussionListByTask_id(updateRequestData.id);

    for (let i = 0; i < existing_task_discussion_list.length; i++) {
        let needDelete = 0; /// 0 =  delete, 1 = no delete
        for (let j = 0; j < updateRequestData.discussion_type_id.length; j++) {
            if (existing_task_discussion_list[i] == updateRequestData.discussion_type_id[j]) {
                updateRequestData.discussion_type_id.splice(j, 1);
                needDelete = 1;
                break;
            }
        }
        if (needDelete == 0) {
            result = await task_discussion_types_model.deleteDiscussionTypeById(existing_task_discussion_list[i].id);
        }

    }
    let newTaskDiscussion_Type = {
        "task_id": updateRequestData.id,
        "discussion_type_id": 0
    }
    for (let i = 0; i < updateRequestData.discussion_type_id.length; i++) {
        newTaskDiscussion_Type.discussion_type_id = updateRequestData.discussion_type_id[i];
        result = await task_discussion_types_model.addNew(newTaskDiscussion_Type);
    }

    /// Check with task_vehicle_types table 

    let existing_task_vehicle_list = await task_vehicle_types_model.getExistingVehicleListByTask_id(updateRequestData.id);


    for (let i = 0; i < existing_task_vehicle_list.length; i++) {
        let needDelete = 0; /// 0 =  delete, 1 = no delete
        for (let j = 0; j < updateRequestData.vehicle_type_id.length; j++) {
            if (existing_task_vehicle_list[i] == updateRequestData.vehicle_type_id[j]) {
                updateRequestData.vehicle_type_id.splice(j, 1);
                needDelete = 1;
                break;
            }
        }

        if (needDelete == 0) {
            result = await task_vehicle_types_model.deleteVehicleTypeById(existing_task_vehicle_list[i].id);

        }

    }
    let newTaskVehicle_Type = {
        "task_id": updateRequestData.id,
        "vehicle_type_id": 0
    }
    for (let i = 0; i < updateRequestData.vehicle_type_id.length; i++) {
        newTaskVehicle_Type.vehicle_type_id = updateRequestData.vehicle_type_id[i];
        result = await task_vehicle_types_model.addNew(newTaskVehicle_Type);
    }


    if (!isEmpty(result) && result.affectedRows == 0) {
        return res.send({
            "error": true,
            "message": "Update Fail try again"
        });
    } else {
        return res.send({
            "error": false,
            "message": "Update Successfully Done"
        });
    }

});


router.get('/create/data', [verifyToken, verifySuperAdminCantAccess, verifySubAdminCantAccess], async (req, res) => {

    let userType = req.decoded.user_type === undefined ? 4 : req.decoded.user_type;
    let salesPersonId = req.decoded.id === undefined ? 0 : req.decoded.id;

    const clientList = await client_model.getList(userType, salesPersonId);
    const corporateClient = await company_sister_concerns_model.getSisterConcernListBySalespersonId(salesPersonId);
    const vehicleTypeList = await vehicle_type_model.getActiveVehicleTypeList();
    const discussionList = await discussion_type_model.getActiveDiscussionTypeList();
    const leadsList = await leads_model.getActiveList();

    return res.send({
        "error": false,
        "message": "Client List.",
        "data": {
            "client_list": clientList,
            "corporateClient": corporateClient,
            "leads_list": leadsList,
            "discussionList": discussionList,
            "vehicle_type_list": vehicleTypeList
        }
    });
});

router.get('/details/:id', verifyToken, async (req, res) => {

    const id = req.params.id;
    let userType = req.decoded.user_type === undefined ? 0 : req.decoded.user_type;
    let salesPersonId = req.decoded.id === undefined ? 0 : req.decoded.id;

    const taskInfo = await task_model.getTaskByID(id, userType, salesPersonId);

    for (let i = 0; i < taskInfo.length; i++) {

        if (taskInfo[i].client_type_id == 0) {
            taskInfo[i].client_info = await company_sister_concerns_model.getSisterConcernAndCompanyByID(taskInfo[i].client_id);
        } else
            taskInfo[i].client_info = await client_model.getClientByID(taskInfo[i].client_id);

        if (!isEmpty(taskInfo[i].client_info)) { // Delete some data
            if (taskInfo[i].client_info[0].hasOwnProperty("user_id")) delete taskInfo[i].client_info[0].user_id;
            if (taskInfo[i].client_info[0].hasOwnProperty("created_at")) delete taskInfo[i].client_info[0].created_at;
            if (taskInfo[i].client_info[0].hasOwnProperty("active_status")) delete taskInfo[i].client_info[0].active_status;
        }


        taskInfo[i].discussion_type = await task_discussion_types_model.getExistingDiscussion_TypeNameAndIdListByTask_id(taskInfo[i].id);
        taskInfo[i].vehicle_type = await task_vehicle_types_model.getExistingVehicle_TypeNameAndIdListByTask_id(taskInfo[i].id);

        delete taskInfo[i].client_id;

    }

    return res.send({
        "error": isEmpty(taskInfo) ? true : false,
        "message": isEmpty(taskInfo) ? "No data Found" : "Data Found.",
        "data": isEmpty(taskInfo) ? {} : taskInfo[0]
    });

});

router.post(['/search', '/list'], verifyToken, async (req, res) => {

    let searchField = {};
    if (req.body.discussion_type_id !== undefined && Array.isArray(req.body.discussion_type_id) && req.body.discussion_type_id.length > 0) {
        searchField.discussion_type_id = req.body.discussion_type_id;
    }

    if (req.body.vehicle_type_id !== undefined && Array.isArray(req.body.vehicle_type_id) && req.body.vehicle_type_id.length > 0) {
        searchField.vehicle_type_id = req.body.vehicle_type_id;
    }



    if (req.body.prospect_type_id !== undefined && req.body.prospect_type_id !== "") {
        searchField.prospect_type_id = req.body.prospect_type_id;
    }

    if (req.body.client_type_id !== undefined && req.body.client_type_id !== "") {
        searchField.client_type_id = req.body.client_type_id;
    }

    if (req.body.sales_person_id !== undefined && req.body.sales_person_id !== "") {
        searchField.sales_person_id = req.body.sales_person_id;
    }

    if (req.body.lead_id !== undefined && req.body.lead_id !== "") {
        searchField.lead_id = req.body.lead_id;
    }

    if (req.decoded.user_type == 4) { // if request user is a sales person, she/he can only access his/her task data
        searchField.sales_person_id = req.decoded.id;
    }

    var d = new Date();

    // Generate Search by date
    if (req.body.from_date !== undefined && req.body.from_date != "" && req.body.from_date != null) {
        if (req.body.to_date !== undefined && req.body.to_date != "") {
            extraWhere = " call_date >= '" + req.body.from_date + "' and call_date <= '" + req.body.to_date + "'";
        } else {
            extraWhere = " call_date = '" + req.body.from_date + "'";
        }
    } else {
        extraWhere = " call_date = '" + d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate() + "'";
    }

    const taskList = await task_model.getTaskListBySearching(searchField, extraWhere);

    for (let i = 0; i < taskList.length; i++) {

        if (taskList[i].client_type_id == 0) {
            taskList[i].client_info = await company_sister_concerns_model.getSisterConcernAndCompanyByID(taskList[i].client_id);
        } else
            taskList[i].client_info = await client_model.getClientByID(taskList[i].client_id);

        if (!isEmpty(taskList[i].client_info)) { // Delete some data
            if (taskList[i].client_info[0].hasOwnProperty("user_id")) delete taskList[i].client_info[0].user_id;
            if (taskList[i].client_info[0].hasOwnProperty("created_at")) delete taskList[i].client_info[0].created_at;
            if (taskList[i].client_info[0].hasOwnProperty("active_status")) delete taskList[i].client_info[0].active_status;
        }


        taskList[i].discussion_type = await task_discussion_types_model.getExistingDiscussion_TypeNameAndIdListByTask_id(taskList[i].id);
        taskList[i].vehicle_type = await task_vehicle_types_model.getExistingVehicle_TypeNameAndIdListByTask_id(taskList[i].id);

        delete taskList[i].client_id;

    }

    return res.send({
        "error": false,
        "message": "",
        "count" : taskList.length,
        "data": isEmpty(taskList) ? [] : taskList
    });

});

router.post('/reassign', [verifyToken, verifySalesPersonCantAccess], async (req, res) => {

    let reqData = {
        "present_saler_id": req.body.present_saler_id,
        "new_saler_id": req.body.new_saler_id,
        "is_assign_all": req.body.is_assign_all,
        "task_id": req.body.task_id
    }

    let errorMessage = "";
    let isError = 0;

    if (reqData.present_saler_id == undefined || isEmpty(await sales_person_model.getProfileById2(reqData.present_saler_id))) {
        isError = 1;
        errorMessage = "Present Sales person not found. ";
    }

    if (reqData.new_saler_id == undefined || isEmpty(await sales_person_model.getProfileById(reqData.new_saler_id))) {
        isError = 1;
        errorMessage += "Present Sales person not found. ";
    }

    if (reqData.new_saler_id == reqData.present_saler_id) {
        isError = 1;
        errorMessage += "Please select different sales person. ";
    }

    if (reqData.is_assign_all == undefined) {
        isError = 1;
        errorMessage += "is_assign_all is undefined. ";
    } else if (reqData.is_assign_all == false) {

        if (reqData.task_id == undefined) {
            isError = 1;
            errorMessage += "task_id is undefined. ";
        } else if (reqData.task_id.length < 1) {
            isError = 1;
            errorMessage += "Please Select Task for assign. ";
        }

    } else if (reqData.is_assign_all != true) {

        isError = 1;
        errorMessage += "is_assign_all must be True or False ";
    }

    if (isError == 1) {
        return res.send({
            "error": true,
            "message": errorMessage
        });
    }

    let result = await task_model.reassignTask(reqData.present_saler_id, reqData.new_saler_id, reqData.is_assign_all, reqData.task_id);

    return res.send({
        "error": false,
        "message": "Task Successfully assign",
        "data": ""
    });
})


router.post('/complete', [verifyToken, verifySuperAdminCantAccess, verifySubAdminCantAccess], async (req, res) => {

    let salesPersonId = req.decoded.id === undefined ? 0 : req.decoded.id;
    let taskId = req.body.task_id;

    const taskInfo = await task_model.getTaskByID(taskId, 4, salesPersonId);

    if (isEmpty(taskInfo)) {
        return res.send({
            "error": true,
            "message": "No Task Found."
        });
    }

    if (taskInfo[0].sales_person_id != salesPersonId) {
        return res.send({
            "error": true,
            "message": "Unauthorize access request."
        });
    }

    let updateData = {
        "status": "complete"
    }

    let result = await task_model.updateTaskByID(taskId, updateData);

    if (isEmpty(result)) {
        return res.send({
            "error": true,
            "message": "Update Fail try again"
        });
    } else {
        return res.send({
            "error": false,
            "message": "Update Successfully Done"
        });
    }
});

router.get('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "error": true
    })
});

router.post('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "error": true
    })
});

module.exports = router;