const express = require("express");
const isEmpty = require("is-empty");
const router = express.Router();
const verifyToken = require('../jwt/verify/verifyLpgToken');
const department_model = require('../model/departments');



router.get('/list', verifyToken, async (req, res) => {

    let result = await department_model.getList();

    return res.send({
        "error": false,
        "message": "Department List.",
        "data": result
    });
});


router.get('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "error": true
    })
});

router.post('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "error": true
    })
});


module.exports = router;