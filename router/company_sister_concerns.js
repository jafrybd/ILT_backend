const express = require("express");
const isEmpty = require("is-empty");
const router = express.Router();
const verifyToken = require('../jwt/verify/verifyLpgToken');
const sales_person_model = require('../model/salesPerson');
const companies_model = require('../model/companies');
const company_sister_concerns_model = require('../model/company_sister_concerns');
const access_corporate_client_model = require('../model/access_corporate_clients');
const access_client_model = require('../model/access_client');
const verifySalesPersonCantAccess = require('../jwt/verify/verifyLpgSalesPersonCantAccess');
const verifySuperAdminCantAccess = require('../jwt/verify/verifyLpgSuperAdminCantAccess');
const verifySubAdminCantAccess = require('../jwt/verify/verifyLpgSubAdminCantAccess');
const e = require("express");


router.get('/allList', verifyToken, async (req, res) => {

    let user_type = req.decoded.user_type;

    if(user_type == 2 || user_type == 3){
        let result = await company_sister_concerns_model.getAllList();

        return res.send({
            "error": false,
            "message": "All Sister Concern List",
            "count": result.length,
            "data": result
        });
    }
    if(user_type == 4 )
    {
        salesPersonId = req.decoded.id;
        let corporateClientList = await company_sister_concerns_model.getSisterConcernListBySalespersonId(salesPersonId);
        return res.send({
            "error": false,
            "message": "All Sister Concern List",
            "count": corporateClientList.length,
            "data": corporateClientList
        });
    }

    
});

router.get('/activeList', verifyToken, async (req, res) => {

    let result = await company_sister_concerns_model.getActiveSisterConcernList();

    return res.send({
        "error": false,
        "message": "Active Sister Concern List.",
        "count": result.length,
        "data": result
    });
});

router.get('/deactiveList', verifyToken, async (req, res) => {

    let result = await company_sister_concerns_model.getDeactiveSisterConcernList();

    return res.send({
        "error": false,
        "message": "Deactive Sister Concern List.",
        "data": result
    });
});


router.post('/addSisterConcern', [verifyToken, verifySalesPersonCantAccess], async (req, res) => {

    let reqData = {
        "title": req.body.title,
        "company_id": req.body.company_id,
        "user_id": req.decoded.user_id,
        "active_status": 0
    }

    let sales_person_id = req.body.sales_person_id;

    let errorMessage = "";
    let isError = 0;

    let companyInfo = await companies_model.getCompanyByID(reqData.company_id);

    if (reqData.company_id == undefined || isEmpty(companyInfo) || companyInfo[0].active_status == 1) {

        isError = 1;
        errorMessage += "Invalid Company ";
    }

    if (reqData.title == undefined || reqData.title.length < 2) {
        isError = 1;
        errorMessage += "Need valid Sister Concern Name and length getter then 2. ";
    }

    let existingData = await company_sister_concerns_model.getSisterConcernByTitleAndID(reqData.company_id, reqData.title);

    if (!isEmpty(existingData) && existingData[0].company_id === reqData.company_id) {
        return res.send({
            "error": true,
            "message": existingData[0].active_status == "0" ? "Sister Concern Already Exist." : "Sister Concern Exist but Deactivate"
        });
    }


    if (isError == 1) {
        return res.send({
            "error": true,
            "message": errorMessage
        });
    }



    let result = await company_sister_concerns_model.addNewSisterConcern(reqData);

    if (result.affectedRows == undefined || result.affectedRows < 1) {
        return res.send({
            "error": true,
            "message": "Some thing Wrong in system database."
        });
    }




    if (!isEmpty(sales_person_id) || sales_person_id !== undefined) {
        let access_corporate_clients = {
            "sales_person_id": req.body.sales_person_id,
            "company_id": reqData.company_id,
            "company_sister_concern_id": result.insertId,
            "remove_reason": ""
        }

        let salesPersonInfo = await sales_person_model.getProfileById(access_corporate_clients.sales_person_id);

        if (isEmpty(salesPersonInfo)) {
            return res.send({
                "error": false,
                "message": "Company Sister Concern Added "
            });
        }

        let companyInfo = await companies_model.getCompanyByID(access_corporate_clients.company_id);
        if (isEmpty(companyInfo)) {
            return res.send({
                "error": true,
                "message": "Company Not Exist"
            });
        }

        result = await access_corporate_client_model.addNew(access_corporate_clients);
    }


    return res.send({
        "error": false,
        "message": "Sister Concern Added Successfully ."
    });


});


router.post('/editSisterConcern', [verifyToken, verifySalesPersonCantAccess], async (req, res) => {

    let reqData = {
        "id": req.body.id,
        "title": req.body.title,
        "sales_person_id": req.body.sales_person_id,
        "remove_reason": req.body.remove_reason
    }

    let errorMessage = "";
    let isError = 0;

    let sisterConcertData = await company_sister_concerns_model.getSisterConcernByID(reqData.id);

    if (isEmpty(sisterConcertData)) {
        return res.send({
            "error": true,
            "message": "Sister concern not found."
        });
    }

    let updateData = {};

    if (sisterConcertData[0].title != reqData.title) {
        let existingData = await company_sister_concerns_model.getSisterConcernByTitleAndID(sisterConcertData[0].company_id, reqData.title);

        if (!isEmpty(existingData)) {
            return res.send({
                "error": true,
                "message": existingData[0].active_status == "0" ? "Sister Concern Already Exist." : "Sister Concern Exist but Deactivate"

            });
        }

        updateData.title = reqData.title;
    }

    let result = {};
    if (Object.keys(updateData).length > 0) {
        result = await company_sister_concerns_model.updateCompanySisterConcernByID(reqData.id, updateData);
    }
    let date = new Date();
    const nowDateTime = date.getUTCFullYear() + '-' +
        ('00' + (date.getUTCMonth() + 1)).slice(-2) + '-' +
        ('00' + date.getUTCDate()).slice(-2) + ' ' +
        ('00' + date.getUTCHours()).slice(-2) + ':' +
        ('00' + date.getUTCMinutes()).slice(-2) + ':' +
        ('00' + date.getUTCSeconds()).slice(-2);

    let access_corporate_client_data = await access_corporate_client_model.getAccessCorporateClientDataByCompanySisterConcerId(reqData.id);
    let access_corporate_client_updateData = {
        "active_status": 1,
        "end_date": nowDateTime,
        "remove_reason": reqData.remove_reason === undefined ? "" : reqData.remove_reason
    }




    if (reqData.sales_person_id !== undefined) {

        let salesPersonInfo = await sales_person_model.getProfileById(reqData.sales_person_id);

        if (isEmpty(salesPersonInfo)) {
            return res.send({
                "error": true,
                "message": "Sales Person Not Exist"
            });
        } else if (salesPersonInfo[0].active_status == 1) {
            return res.send({
                "error": true,
                "message": "Sales Person ALready Delete"
            });
        }

        let access_corporate_clients_newData = {
            "sales_person_id": reqData.sales_person_id,
            "company_id": sisterConcertData[0].company_id,
            "company_sister_concern_id": reqData.id,
            "remove_reason": ""
        }

        if (isEmpty(access_corporate_client_data)) {
            result = await access_corporate_client_model.addNew(access_corporate_clients_newData);
        } else if (access_corporate_client_data[0].sales_person_id !== reqData.sales_person_id) {

            result = await access_corporate_client_model.updateAccessCorporateClientById(access_corporate_client_data[0].id, access_corporate_client_updateData);
            result = await access_corporate_client_model.addNew(access_corporate_clients_newData);

        }

    } else {
        if (!isEmpty(access_corporate_client_data)) {
            result = await access_corporate_client_model.updateAccessCorporateClientById(access_corporate_client_data[0].id, access_corporate_client_updateData);
        }
    }

    return res.send({
        "error": false,
        "message": "Sister Concern Update Successfully Done."
    });
});

router.get('/details/:id', [verifyToken, verifySalesPersonCantAccess], async (req, res) => {

    const sister_concern_id = req.params.id;

    const sisterConcernInf0 = await company_sister_concerns_model.getSisterConcernAndCompanyByID(sister_concern_id);

    if (isEmpty(sisterConcernInf0)) {
        return res.send({
            "error": true,
            "message": "Sister Concern Not Exist."
        });
    }
    /* else if(sisterConcernInf0[0].active_status==1)
    {
        return res.send({
            "error": true,
            "message": "Sister Concern is not Activated."
        });
    } */
    else {
        return res.send({
            "error": true,
            "message": "Found",
            "data": sisterConcernInf0
        });
    }

});

router.post(['/deactive', '/delete'], [verifyToken, verifySalesPersonCantAccess], async (req, res) => {

    let id = req.body.id;

    try {
        id = parseInt(id);
    } catch (e) {
        id = "";
    }

    if (!id || typeof (id) !== "number" || id < 1) {
        return res.send({
            "error": true,
            "message": "invalid id."
        });
    }

    let existingData = await company_sister_concerns_model.getSisterConcernByID(id);

    if (isEmpty(existingData)) {
        return res.send({
            "error": true,
            "message": "Sister Concern Not Exist."
        });
    }

    let result = await company_sister_concerns_model.deactiveSisterConcern(id);

    return res.send({
        "error": false,
        "message": `Sister Concern '${existingData[0].title}' has been deleted!!.`

    });
});


router.post('/active', [verifyToken, verifySalesPersonCantAccess], async (req, res) => {

    let id = req.body.id;

    try {
        id = parseInt(id);
    } catch (e) {
        id = "";
    }

    if (!id || typeof (id) !== "number" || id < 1) {
        return res.send({
            "error": true,
            "message": "invalid id."
        });
    }

    let existingData = await company_sister_concerns_model.getSisterConcernByID(id);

    if (isEmpty(existingData)) {
        return res.send({
            "error": true,
            "message": "Sister Concern Not Exist."
        });
    }

    let result = await company_sister_concerns_model.activeSisterConcern(id);

    return res.send({
        "error": false,
        "message": "Sister Concern has been activated!!."

    });
});


router.post('/search', [verifyToken, verifySalesPersonCantAccess], async (req, res) => {

    let searchField = {
        "sister_concern_title": req.body.sister_concern_title,
        "company_title": req.body.company_title
    };

    if (req.body.sister_concern_title == undefined || isEmpty(req.body.sister_concern_title)) {
        searchField.sister_concern_title = 0;
    }

    if (req.body.company_title == undefined || isEmpty(req.body.company_title)) {
        searchField.company_title = 0;
    }

    if (searchField.company_title === 0 && searchField.sister_concern_title === 0) {
        return res.send({
            "error": true,
            "message": "You have to select at least on field",
        });
    }

    if (req.body.sister_concern_title !== undefined && req.body.sister_concern_title !== "") {
        if (req.body.sister_concern_title < 1) {
            return res.send({
                "error": true,
                "message": "Need valid Sister Concern name and Length should be at least 1",
            });
        } else {
            searchField.sister_concern_title = req.body.sister_concern_title;
        }

    }

    if (req.body.company_title !== undefined && req.body.company_title !== "") {
        if (req.body.company_title < 1) {
            return res.send({
                "error": true,
                "message": "Need valid Company name and Length should be at least 1",
            });
        } else {
            searchField.company_title = req.body.company_title;
        }

    }


    const result = await company_sister_concerns_model.getSisterConcernListBySearcing(searchField);

    return res.send({
        "error": false,
        "message": "Sister Concern list",
        "count": result.length,
        "data": result

    });

});

router.post('/getAccessHistory', [verifyToken, verifySalesPersonCantAccess], async (req, res) => {

    const company_sister_concern_id = req.body.company_sister_concern_id;
    let existingData = await company_sister_concerns_model.getSisterConcernByID(company_sister_concern_id);

    if (isEmpty(existingData)) {
        return res.send({
            "error": true,
            "message": "Client Not Exist."
        });
    }


    let access_corporate_client_data = await access_corporate_client_model.getAccessCorporateClientAllAccessHistoryByCompanySisterConcerId(company_sister_concern_id);

    return res.send({
        "error": false,
        "message": "Client access History.",
        "data": isEmpty(access_corporate_client_data) ? [] : access_corporate_client_data

    });
});

//// All Sister Concern List For Global Search

router.get('/:company_id/allSisterConcernListForGlobalSearch', verifyToken, async (req, res) => {

    const company_id = req.params.company_id;
    const result = await company_sister_concerns_model.getSisterConcernByCompanyID(company_id);

    return res.send({
        "error": false,
        "message": "Active Sister Concern List.",
        "count": result.length,
        "data": result
    });
});

/// Admin and Sales Person can search by Sister Concern name and can see Sister Concern list and Assigned SalesPerson

router.post('/:id/searchSisterConcern', [verifyToken], async (req, res) => {

    const company_id = req.params.id;

    let sister_concern_title = req.body.title;

    const companyInfo = await companies_model.getCompanyByID(company_id);

    if (isEmpty(companyInfo)) {
        return res.send({
            "error": true,
            "message": "Company Not Exist."
        });
    } else if (companyInfo[0].active_status == 1) {
        return res.send({
            "error": true,
            "message": "Company is not Activated."
        });
    } else {

        if (sister_concern_title == undefined || sister_concern_title.length < 1) {
            return res.send({
                "error": true,
                "message": "Need valid Sister name and Length should be at least 1",
            });
        }


        let result = await company_sister_concerns_model.getSisterConcernListBySearchingWithCompanyID(company_id, sister_concern_title);

        return res.send({
            "error": false,
            "message": "Found",
            "count": result.length,
            "data": result

        });
    }
});

router.post('/:id/searchSisterConcernBySalesPerson', [verifyToken, verifySuperAdminCantAccess, verifySubAdminCantAccess], async (req, res) => {

    const company_id = req.params.id;
    let sister_concern_title = req.body.title;
    let sales_person_id = req.decoded.id;

    const companyInfo = await companies_model.getCompanyByID(company_id);


    if (isEmpty(companyInfo)) {
        return res.send({
            "error": true,
            "message": "Company Not Exist."
        });
    } else if (companyInfo[0].active_status == 1) {
        return res.send({
            "error": true,
            "message": "Company is not Activated."
        });
    } else {

        if (sister_concern_title == undefined || sister_concern_title.length < 1) {
            return res.send({
                "error": true,
                "message": "Need valid Sister name and Length should be at least 1",
            });
        }


        let result = await company_sister_concerns_model.getSisterConcernListBySearchingWithCompanyIDAndSalesPersonID(sales_person_id, company_id, sister_concern_title);

        return res.send({
            "error": false,
            "message": "Found",
            "count": result.length,
            "data": result

        });
    }
});

router.get('/:client_type/:sales_person_id', [verifyToken, verifySalesPersonCantAccess], async (req, res) => {

    const client_type = req.params.client_type == undefined ? 0 : req.params.client_type;
    const sales_person_id = req.params.sales_person_id == undefined ? 0 : req.params.sales_person_id;

    if (client_type == 0) {

        let result = await access_corporate_client_model.getAccessCompany_SisterConcern_By_SalesPersonID(sales_person_id);

        return res.send({
            "error": false,
            "message": "Found",
            "count": result.length,
            "data": result

        });
    } else {
        let result = await access_client_model.getAccessClientInformationBySalesPersonID(sales_person_id);

        return res.send({
            "error": false,
            "message": "Found",
            "count": result.length,
            "data": result

        });
    }
});

router.get('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "error": true
    })
});

router.post('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "error": true
    })
});

module.exports = router;