const express = require("express");
const isEmpty = require("is-empty");
const router = express.Router();
const client_type_model = require('../model/clientType');
const verifyToken = require('../jwt/verify/verifyLpgToken');
const verifySalesPersonCantAccess = require('../jwt/verify/verifyLpgSalesPersonCantAccess');



router.get('/list', verifyToken, async (req, res) => { /// Active Client Type List

    let result = await client_type_model.getActiveClientTypeList();

    return res.send({
        "error": false,
        "message": "Client type List.",
        "data": result
    });
});

//// add Non Corporate Client --- Get Client Type List Without Corporate Client

router.get('/clientTypeListWithoutCorporate', verifyToken, async (req, res) => { /// Active Client Type List

    let result = await client_type_model.getActiveClientTypeList();
    
    if(result[0].id == 0)
    {
         result.shift();
    }

    return res.send({
        "error": false,
        "message": "Client type List.",
        "data": result
    });
});


router.get('/allList', verifyToken, async (req, res) => {

    let result = await client_type_model.getAllList();

    return res.send({
        "error": false,
        "message": "Client Type  List.",
        "data": result
    });
});

router.get('/deactiveList', verifyToken, async (req, res) => {

    let result = await client_type_model.getDeactiveClientTypeList();

    return res.send({
        "error": false,
        "message": "Deactive Client Type List.",
        "data": result
    });
});

router.get('/activeList', verifyToken, async (req, res) => {

    let result = await client_type_model.getActiveClientTypeList();

    return res.send({
        "error": false,
        "message": "Active Client Type  List.",
        "data": result
    });
});



router.post('/add', [verifyToken, verifySalesPersonCantAccess], async (req, res) => {

    let reqData = {
        "type": req.body.type,
        "active_status": 0
    }


    if (reqData.type == undefined || reqData.type.length < 2) {
        return res.send({
            "error": true,
            "message": "Need valid Client Type and length must then 1.",
        });
    }

    let existingData = await client_type_model.getClientTypeByType(reqData.type);

    if (!isEmpty(existingData)) {
        return res.send({
            "error": true,
            "message": existingData[0].active_status == "0" ? "Client type Already Exist." : "Client type Exist but Deactivate, Please contect to Admin."
        });
    }

    let result = await client_type_model.addNewClientType(reqData);

    if (result.affectedRows == undefined || result.affectedRows < 1) {
        return res.send({
            "error": true,
            "message": "Some thing Wrong in system database."
        });
    }

    return res.send({
        "error": false,
        "message": "Client type Added."
    });
});

router.post('/delete', [verifyToken, verifySalesPersonCantAccess], async (req, res) => {

    let id = req.body.id;

    try {
        id = parseInt(id);
    } catch (e) {
        id = "";
    }

    if (!id || typeof (id) !== "number" || id < 1) {
        return res.send({
            "error": true,
            "message": "invalid id."
        });
    }

    let existingData = await client_type_model.getClientTypeByID(id);

    if (isEmpty(existingData)) {
        return res.send({
            "error": true,
            "message": "Client type ID Not Exist."
        });
    }

    let result = await client_type_model.deactiveClientType(id);

    return res.send({
        "error": false,
        "message": `Client type '${existingData[0].type}' Deleted Successfully `,
        "data": []
    });
});

router.post('/active', [verifyToken, verifySalesPersonCantAccess], async (req, res) => {

    let id = req.body.id;

    try {
        id = parseInt(id);
    } catch (e) {
        id = "";
    }

    if (!id || typeof (id) !== "number" || id < 1) {
        return res.send({
            "error": true,
            "message": "invalid id."
        });
    }

    let existingData = await client_type_model.getClientTypeByID(id);

    if (isEmpty(existingData)) {
        return res.send({
            "error": true,
            "message": "Client  ID Not Exist."
        });
    }


    if (existingData[0].active_status === 0) {
        return res.send({
            "error": true,
            "message": "Already this is Active"
        });
    }
    let result = await client_type_model.activeClientType(id);

    return res.send({
        "error": false,
        "message": "Client Type Activated."

    });
});

router.post('/update', [verifyToken, verifySalesPersonCantAccess], async (req, res) => {

    let type = req.body.type;
    let id = req.body.id;

    if (type == undefined || type.length < 2) {
        return res.send({
            "error": true,
            "message": "Need valid Client Type and length must then 1.",
        });
    }

    let existingData = await client_type_model.getClientTypeByType(type);

    if (!isEmpty(existingData) && existingData[0].id !== id) {
        return res.send({
            "error": true,
            "message": existingData[0].active_status == "0" ? "Client type Already Exist." : "Client type Exist but Deactivate, Please contect to Admin."
        });
    }

    existingData = await client_type_model.getClientTypeByID(id);

    if (isEmpty(existingData)) {
        return res.send({
            "error": true,
            "message": "Client type ID Not Exist."
        });
    }

    let result = await client_type_model.updateClientTypeByID(id, type);

    if (result.affectedRows == undefined || result.affectedRows == 0) {
        return res.send({
            "error": true,
            "message": "Some thing Wrong in system database."
        });
    }

    return res.send({
        "error": false,
        "message": "Client Update Successfully Done."
    });

});

router.get('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "error": true
    })
});

router.post('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "error": true
    })
});


module.exports = router;