const express = require("express");
const isEmpty = require("is-empty");
const router = express.Router();
const verifyToken = require('../jwt/verify/verifyLpgToken');
const companies_model = require('../model/companies');
const company_sister_concerns_model = require('../model/company_sister_concerns');
const access_corporate_client_model = require('../model/access_corporate_clients');

const verifySalesPersonCantAccess = require('../jwt/verify/verifyLpgSalesPersonCantAccess');


router.get('/allList', verifyToken, async (req, res) => {

    let user_type = req.decoded.user_type;

    if(user_type == 2 || user_type == 3)
    {
        let result = await companies_model.getAllList();

    return res.send({
        "error": false,
        "message": "Company name List.",
        "data": result
    });
    }

    else if(user_type == 4) {
        let salesPersonId = req.decoded.id;
        
        let companyList = await access_corporate_client_model.getCompanyListBySalespersonID(salesPersonId);

        if (isEmpty(companyList)) {
            return res.send({
                "error": true,
                "message": "You have no Company"
            });
        }
        else{
            return res.send({
                "error": false,
                "message": "Company name List.",
                "data": companyList
            });
        }
    }    
});



 router.get('/deactiveList', verifyToken, async (req, res) => {

    let result = await companies_model.getDeactiveCompanyList();

    return res.send({
        "error": false,
        "message": "Deactive Company List.",
        "data": result
    });
});

router.get('/activeList', verifyToken, async (req, res) => {

    let result = await companies_model.getActiveCompanyList();

    return res.send({
        "error": false,
        "message": "Active Company List.",
        "count": result.length,
        "data": result
    });
});


router.post('/add', [verifyToken, verifySalesPersonCantAccess], async (req, res) => {

    let reqData = {
        "title": req.body.title,
        "user_id" :req.decoded.user_id,
        "active_status": 0
    }



    if (reqData.title == undefined || reqData.title.length < 2) {
        return res.send({
            "error": true,
            "message": "Need valid Company name and length must greater than 1.",
        });
    }

    let existingData = await companies_model.getCompanyByTitle(reqData.title);


    if (!isEmpty(existingData)) {
        return res.send({
            "error": true,
            "message": existingData[0].active_status == "0" ? "Company name Already Exist." : "Company name Exist but Deactivate"
        });
    }

    let result = await companies_model.addNewCompany(reqData);

    if (result.affectedRows == undefined || result.affectedRows < 1) {
        return res.send({
            "error": true,
            "message": "Some thing Wrong in system database."
        });
    }

    return res.send({
        "error": false,
        "message": "Company name Added."
    });
});

router.post('/update', [verifyToken, verifySalesPersonCantAccess], async (req, res) => {

    let title = req.body.title;
    let id = req.body.id;
    let user_id = req.decoded.user_id;

    if (title == undefined || title.length < 2) {
        return res.send({
            "error": true,
            "message": "Need valid Company name and length must greater than 1.",
        });
    }
    

    let existingData = await companies_model.getCompanyByTitle(title);

    if (!isEmpty(existingData) && existingData[0].id !== id ) {
        return res.send({
            "error": true,
            "message": existingData[0].active_status == "0" ? "Company name Already Exist." : "Company name Exist but Deactivate"
        });
    }
    

    existingData = await companies_model.getCompanyByID(id);

    if (isEmpty(existingData)) {
        return res.send({
            "error": true,
            "message": "Company ID Not Exist."
        });
    }


    let result = await companies_model.updateCompanyByID(id,title,user_id);

    if (result.affectedRows == undefined || result.affectedRows == 0) {
        return res.send({
            "error": true,
            "message": "Some thing Wrong in system database."
        });
    }

    return res.send({
        "error": false,
        "message": "Company Name Update Successfully Done."
    });

}); 

router.post(['/deactive', '/delete'], [verifyToken, verifySalesPersonCantAccess], async (req, res) => {

    let id = req.body.id;

    try {
        id = parseInt(id);
    } catch (e) {
        id = "";
    }

    if (!id || typeof (id) !== "number" || id < 1) {
        return res.send({
            "error": true,
            "message": "invalid id."
        });
    }

    let existingData = await companies_model.getCompanyByID(id);

    if (isEmpty(existingData)) {
        return res.send({
            "error": true,
            "message": "Company Not Exist."
        });
    }

    let result = await companies_model.deactiveCompany(id);

    return res.send({
        "error": false,
        "message": `Company '${existingData[0].title}' has been deleted!!.`

    });
});


router.post('/active', [verifyToken, verifySalesPersonCantAccess], async (req, res) => {

    let id = req.body.id;

    try {
        id = parseInt(id);
    } catch (e) {
        id = "";
    }

    if (!id || typeof (id) !== "number" || id < 1) {
        return res.send({
            "error": true,
            "message": "invalid id."
        });
    }

    let existingData = await companies_model.getCompanyByID(id);

    if (isEmpty(existingData)) {
        return res.send({
            "error": true,
            "message": "Company Not Exist."
        });
    }

    let result = await companies_model.activeCompany(id);

    return res.send({
        "error": false,
        "message": "Company has been activated!!."

    });
});

///// Company Search Will be Used for  Global Search

router.post('/globalSearch', [verifyToken], async (req, res) => {

    let title = req.body.title;
    let user_type = req.decoded.user_type;

    if (title == undefined || title.length < 1) {
        return res.send({
            "error": true,
            "message": "Need valid Company name and Length should be at least 1",
        });
    }
    
    let result = await companies_model.getCompanyListBySearching(title);

    return res.send({
        "error": false,
        "message": "",
        "count" : result.length,
        "data" : result
    });
    
});

//// Normal Search 
router.post('/search', [verifyToken], async (req, res) => {

    let title = req.body.title;
    let user_type = req.decoded.user_type;

    if (title == undefined || title.length < 1) {
        return res.send({
            "error": true,
            "message": "Need valid Company name and Length should be at least 1",
        });
    }

    if(user_type == 2 || user_type == 3)
    {
        let result = await companies_model.getCompanyListBySearching(title);

    return res.send({
        "error": false,
        "message": "",
        "count" : result.length,
        "data" : result

    });
    }
    else if(user_type == 4) {
        let sales_person_id = req.decoded.id;
        
        let companyList = await companies_model.getCompanyListBySearchingBySalesPerson(sales_person_id,title);

        if (isEmpty(companyList)) {
            return res.send({
                "error": true,
                "message": "You have no Company"
            });
        }
        else{
            return res.send({
                "error": false,
                "message": "Company name List.",
                "count" : companyList.length,
                "data": companyList
            });
        }
    }    

});

router.get('/details/:id', verifyToken, async (req, res) => {

    const company_id = req.params.id;
    const user_type = req.decoded.user_type; 
    

    const companyInfo = await companies_model.getCompanyByID(company_id);
    
    if (isEmpty(companyInfo)) {
        return res.send({
            "error": true,
            "message": "Company Not Exist."
        });
    }
    else if(companyInfo[0].active_status==1)
    {
        return res.send({
            "error": true,
            "message": "Company is not Activated."
        });
    }
    else {
        if(user_type == 2 || user_type == 3)
        {
            const sisterConcernInfo = await company_sister_concerns_model.getSisterConcernByCompanyID(company_id);

            if (isEmpty(sisterConcernInfo)) {
                return res.send({
                    "error": false,
                    "message" : "No Sister Concern in This Company",
                    "data": []
                });
            }
            return res.send({
                "error": false,
                "message": " Sister Concern List.",
                "count" : sisterConcernInfo.length,
                "data": sisterConcernInfo
            });
        }

        if(user_type == 4)
        {
            let salesPersonId = req.decoded.id;
            const sisterConcernInfo = await company_sister_concerns_model.getSisterConcernByCompanyIDAndSalesPersonID(salesPersonId,company_id);
            if (isEmpty(sisterConcernInfo)) {
                return res.send({
                    "error": true,
                    "message": "No Data for him"
                });
            }
            
            return res.send({
                "error": false,
                "message": " Sister Concern List.",
                "count" : sisterConcernInfo.length,
                "data": sisterConcernInfo
            });
        }
    }
});




router.get('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "error": true
    })
});


router.post('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "error": true
    })
});


module.exports = router;