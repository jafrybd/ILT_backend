const express = require("express");
const isEmpty = require("is-empty");
const router = express.Router();
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const common_model = require('../model/common');
const salesPerson_model = require('../model/salesPerson');
const superAdmin_model = require('../model/superAdmin');
const subAdmin_model = require('../model/subAdmin');
const mdSir_model = require('../model/md_sir');
// global.config = require('../jwt/config/config');


router.post('/login', async (req, res) => {

    let loginData = {
        "password": req.body.password,
        "phone_number": req.body.phone
    }

    let errorMessage = "";
    let isError = 0;


    if (loginData.password == undefined || loginData.password.length < 6) {
        isError = 1;
        errorMessage += "Need valid password.";
    }

    if (loginData.phone_number == undefined || loginData.phone_number.length != 11) {
        isError = 1;
        errorMessage += "Phone number invalid.";
    }

    if (isError == 1) {
        return res.send({
            "error": true,
            "message": errorMessage
        });
    }

    let userData = await common_model.getUserByPhone(loginData.phone_number);

    if (isEmpty(userData)) {
        return res.send({
            "error": true,
            "message": "No user found."
        });
    }

    if (bcrypt.compareSync(loginData.password, userData[0].password)) {

        let profileData = "";


        if (userData[0].user_type == 2) {

            profileData = await superAdmin_model.getProfileById(userData[0].profile_id);

        } else if (userData[0].user_type == 3) {

            profileData = await subAdmin_model.getProfileById(userData[0].profile_id);

        } else if (userData[0].user_type == 4) {

            profileData = await salesPerson_model.getProfileById(userData[0].profile_id);
            if (!isEmpty(profileData)){
                delete profileData[0].password_recovery_questions_id;
                delete profileData[0].question_ans_no;
            }
            

        } else {
            return res.send({
                "error": true,
                "message": "You can't login this sytem, using this route."
            });
        }

        

        if (isEmpty(profileData)) {
            return res.send({
                "error": true,
                "message": "Your account is Delete, You can't access this System."
            });
        } else if (profileData[0].active_status == 1) {

            return res.send({
                "error": true,
                "message": "Your account is Delete, You can't access this System."
            });

        } else {

            // Delete some data, as Client no need to access this data
            delete userData[0].password;
            delete userData[0].profile_id;
            userData[0].profile = profileData[0];
            delete userData[0].profile.active_status;
            delete userData[0].phone_number;


            delete loginData.password;

            // Add Data in Token
            loginData.user_id = userData[0].id;
            loginData.id = userData[0].profile.id;
            loginData.name = userData[0].profile.name;
            loginData.user_type = userData[0].user_type;


            //  "Generate Token"
            let token = jwt.sign(loginData, global.config.secretKey, {
                algorithm: global.config.algorithm,
                expiresIn: '1440m' // one day
            });

            userData[0].token = token;

            return res.send({
                "error": false,
                "message": errorMessage,
                'data': userData[0]
            });
        }

    } else {
        return res.send({
            "error": true,
            "message": "Wrong Password"
        });
    }

});


router.get('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "error": true
    })
});

router.post('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "error": true
    })
});

module.exports = router;