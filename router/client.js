const express = require("express");
const isEmpty = require("is-empty");
const router = express.Router();
const verifyToken = require('../jwt/verify/verifyLpgToken');
const client_model = require('../model/client');
const client_type_model = require('../model/clientType');
const task_model = require('../model/task');
const sales_person_model = require('../model/salesPerson');
const access_client_model = require('../model/access_client');
const access_corporate_client_model = require('../model/access_corporate_clients');
const company_sister_concerns_model = require('../model/company_sister_concerns');
const task_discussion_types_model = require('../model/task_discussion_types');
const task_vehicle_types_model = require('../model/task_vehicle_types');

const verifySuperAdminCantAccess = require('../jwt/verify/verifyLpgSuperAdminCantAccess');
const verifySubAdminCantAccess = require('../jwt/verify/verifyLpgSubAdminCantAccess');
const verifySalesPersonCantAccess = require('../jwt/verify/verifyLpgSalesPersonCantAccess');


router.get('/list', verifyToken, async (req, res) => {

    let userType = req.decoded.user_type === undefined ? 4 : req.decoded.user_type;
    let salesPersonId = req.decoded.id === undefined ? 0 : req.decoded.id;

    let result = await client_model.getList(userType, salesPersonId);

    return res.send({
        "error": false,
        "message": "Client List.",
        "data": result
    });
});


router.post('/add', [verifyToken, verifySuperAdminCantAccess, verifySubAdminCantAccess], async (req, res) => {

    let reqData = {
        "name": req.body.name,
        "primary_phone_no": req.body.primary_phone_no,
        "secondary_phone_no": req.body.secondary_phone_no,
        "email": req.body.email,
        "address": req.body.address,
        "client_type_id": req.body.client_type_id,
        "active_status": 0
    }

    let errorMessage = "";
    let isError = 0;

    if (req.body.sales_person_id == undefined || isEmpty(await sales_person_model.getProfileById(req.body.sales_person_id))) {
        isError = 1;
        errorMessage = "Sales person not found. ";
    }

    if (reqData.name == undefined || reqData.name.length < 2) {
        isError = 1;
        errorMessage += "Need valid name and length getter then 2. ";
    }

    if (reqData.primary_phone_no == undefined || reqData.primary_phone_no.length != 11) {
        isError = 1;
        errorMessage += "Primary Phone number invalid.";
    }

    if (reqData.secondary_phone_no !== undefined && reqData.secondary_phone_no.length != 11 && reqData.secondary_phone_no != "") {
        isError = 1;
        errorMessage += "Secondary Phone number invalid.";
    }

    if (reqData.primary_phone_no === reqData.secondary_phone_no) {
        reqData.secondary_phone_no = "";
    }

    if (reqData.email !== undefined && reqData.email != "") {
        const re = /\S+@\S+\.\S+/;
        if (!re.test(reqData.email)) {
            isError = 1;
            errorMessage += " Email is invalid.";
        }
    } else reqData.email == "";

    let clientTypeInfo = await client_type_model.getClientTypeByID(reqData.client_type_id);

    if (isEmpty(clientTypeInfo)) {
        isError = 1;
        errorMessage += " Client type not found.";
    }

    if (isError == 1) {
        return res.send({
            "error": true,
            "message": errorMessage
        });
    }

    let userExistingInfo = await client_model.getClientByPhone(reqData.primary_phone_no);
    if (!isEmpty(userExistingInfo)) {

        let findExisting = 0;

        for (let i = 0; i < userExistingInfo.length; i++) {
            if (userExistingInfo[i].active_status == 0) {
                findExisting = 1;
                break;
            }
        }

        if (findExisting === 1) {
            return res.send({
                "error": true,
                "message": "Primary phone number already Use."
            });
        }
    }

    if (reqData.secondary_phone_no != "") {
        userExistingInfo = await client_model.getClientByPhone(reqData.secondary_phone_no);
        if (!isEmpty(userExistingInfo)) {

            let findExisting = 0;

            for (let i = 0; i < userExistingInfo.length; i++) {
                if (userExistingInfo[i].active_status == 0) {
                    findExisting = 1;
                    break;
                }
            }

            if (findExisting === 1) {
                return res.send({
                    "error": true,
                    "message": "Secondary phone number already Use."
                });
            }
        }
    }

    let result = await client_model.addNewClient(reqData);

    if (result.affectedRows == undefined || result.affectedRows < 1) {
        return res.send({
            "error": true,
            "message": "Some thing Wrong in system database."
        });
    }

    let addAccessClient = {
        "sales_person_id": req.body.sales_person_id,
        "client_id": result.insertId
    }

    result = await access_client_model.addNew(addAccessClient);

    return res.send({
        "error": false,
        "message": "Client Added Successfully Done."
    });

});

router.post('/profile/update', [verifyToken, verifySuperAdminCantAccess, verifySubAdminCantAccess], async (req, res) => {

    let updateRequestData = {
        "id": req.body.id,
        "name": req.body.name,
        "primary_phone_no": req.body.primary_phone_no,
        "secondary_phone_no": req.body.secondary_phone_no,
        "email": req.body.email,
        "address": req.body.address,
        "client_type_id": req.body.client_type_id
    }

    let updateData = {};

    let errorMessage = "";
    let isError = 0; // 0 = No
    let willWeUpdate = 1; // 0 = yes;

    const existinUserInfo = await client_model.getClientByID(updateRequestData.id);

    if (isEmpty(existinUserInfo)) {
        return res.send({
            "error": true,
            "message": "User Not Exist."
        });
    }

    if (updateRequestData.primary_phone_no === updateRequestData.secondary_phone_no) {
        updateRequestData.secondary_phone_no = "";
    }

    if (existinUserInfo[0].name !== updateRequestData.name) {

        if (updateRequestData.name == undefined || updateRequestData.name.length < 2) {
            isError = 1;
            errorMessage = "Need valid name and length getter then 2. ";
        } else {
            willWeUpdate = 0;
            updateData.name = updateRequestData.name;
        }
    }

    if (existinUserInfo[0].primary_phone_no !== updateRequestData.primary_phone_no) {

        if (updateRequestData.primary_phone_no == undefined || updateRequestData.primary_phone_no.length != 11) {
            isError = 1;
            errorMessage += "Primary Phone number invalid.";
        } else {

            let findExisting = 0;
            let userINPhoneNumber = await client_model.getClientByPhone(updateRequestData.primary_phone_no);

            for (let i = 0; i < userINPhoneNumber.length; i++) {
                if (userINPhoneNumber[i].active_status == 0 && userINPhoneNumber[i].id !== updateRequestData.id) {
                    findExisting = 1;
                    break;
                }
            }

            if (findExisting === 0) {
                willWeUpdate = 0;
                updateData.primary_phone_no = updateRequestData.primary_phone_no;
            } else {
                isError = 1;
                errorMessage += "Primary Phone number already use.";
            }

        }
    }

    if (existinUserInfo[0].secondary_phone_no !== updateRequestData.secondary_phone_no) {

        if (updateRequestData.secondary_phone_no != "") {
            if (updateRequestData.secondary_phone_no == undefined || updateRequestData.secondary_phone_no.length != 11) {
                isError = 1;
                errorMessage += "Secondary Phone number invalid.";
            } else {

                let userINPhoneNumber = await client_model.getClientByPhone(updateRequestData.secondary_phone_no);
                let findExisting = 0;

                for (let i = 0; i < userINPhoneNumber.length; i++) {
                    if (userINPhoneNumber[i].active_status == 0 && userINPhoneNumber[i].id !== updateRequestData.id) {
                        findExisting = 1;
                        break;
                    }
                }

                if (findExisting === 0) {
                    willWeUpdate = 0;
                    updateData.secondary_phone_no = updateRequestData.secondary_phone_no;
                } else {
                    isError = 1;
                    errorMessage += "Secondary Phone number already use.";
                }
            }
        }
    }

    if (existinUserInfo[0].email !== updateRequestData.email && !isEmpty(updateRequestData.email)) {

        const re = /\S+@\S+\.\S+/;
        if (!re.test(updateRequestData.email)) {
            isError = 1;
            errorMessage += " Email is invalid.";
        } else {
            willWeUpdate = 0;
            updateData.email = updateRequestData.email;
        }
    }

    if (existinUserInfo[0].address !== updateRequestData.address) {
        willWeUpdate = 0;
        updateData.address = updateRequestData.address;
    }



    if (existinUserInfo[0].client_type_id !== updateRequestData.client_type_id) {

        let clientTypeInfo = await client_type_model.getClientTypeByID(updateRequestData.client_type_id);

        if (isEmpty(clientTypeInfo)) {
            isError = 1;
            errorMessage += " Client type not found.";
        } else {
            willWeUpdate = 0;
            updateData.client_type_id = updateRequestData.client_type_id;
        }
    }

    if (isError == 1) {
        return res.send({
            "error": true,
            "message": errorMessage
        });
    }




    let result = {};
    if (willWeUpdate == 0) {
        result = await client_model.updateClientByID(updateRequestData.id, updateData);
    }

    if (!isEmpty(result) && result.affectedRows == 0) {
        return res.send({
            "error": true,
            "message": "Update Fail try again"
        });
    } else {
        return res.send({
            "error": false,
            "message": "Update Successfully Done"
        });
    }

});

router.post('/profile/delete', [verifyToken, verifySuperAdminCantAccess, verifySubAdminCantAccess], async (req, res) => {

    let id = req.body.id;
    let salesPersonId = req.decoded.id;

    try {
        id = parseInt(id);
    } catch (e) {
        id = "";
    }

    if (!id || typeof (id) !== "number" || id < 1) {
        return res.send({
            "error": true,
            "message": "invalid id."
        });
    }

    let existingData = await client_model.getClientByID(id);

    if (isEmpty(existingData)) {
        return res.send({
            "error": true,
            "message": "Client ID Not Exist."
        });
    }
    if (existingData) {
        let exixtingAccessClientList = await access_client_model.getAccessClientDataByclient_idAndsales_person_id(id, salesPersonId);

        if (isEmpty(exixtingAccessClientList)) {
            return res.send({
                "error": true,
                "message": "This is not Your Client."
            });
        }

        let result = await client_model.deactiveClient(id);

        let deactiveAccessClient = await access_client_model.deactiveAccessClient(exixtingAccessClientList[0].id);


        return res.send({
            "error": false,
            "message": `Client '${existingData[0].name}' Deleted Successfully .`
        });
    }

});


router.get('/profile/view/:client_type/:client_id', verifyToken, async (req, res) => {

    const client_id = req.params.client_id;
    const user_type = req.decoded.user_type;
    const client_type = req.params.client_type;

    let clientInfo;

    if (client_type == 0)
        clientInfo = await company_sister_concerns_model.getSisterConcernAndCompanyByID(client_id);
    else
        clientInfo = await client_model.getClientByID(client_id);

    if (isEmpty(clientInfo)) {
        return res.send({
            "error": true,
            "message": "Client Not Exist."
        });
    } else {
        // delete clientInfo[0].active_status;

        if (clientInfo[0].hasOwnProperty("created_at")) delete clientInfo[0].created_at;

        clientInfo[0].clientType = await client_type_model.getClientTypeByID(clientInfo[0].client_type_id);

        if (user_type == 4) {
            let salesPersonId = req.decoded.id === undefined ? 0 : req.decoded.id;
            clientInfo[0].task = await task_model.getTaskListByClientIDAndSalesPersonId(clientInfo[0].id, salesPersonId);
        } else {
            clientInfo[0].task = await task_model.getTaskListByClientID(clientInfo[0].id);
        }

        for (let i = 0; i < clientInfo[0].task.length; i++) {
            clientInfo[0].task[i].discussion_type = await task_discussion_types_model.getExistingDiscussion_TypeNameAndIdListByTask_id(clientInfo[0].task[i].id);
            clientInfo[0].task[i].vehicle_type = await task_vehicle_types_model.getExistingVehicle_TypeNameAndIdListByTask_id(clientInfo[0].task[i].id);

            if (clientInfo[0].task[i].hasOwnProperty("client_id")) delete clientInfo[0].task[i].client_id;
        }


        return res.send({
            "error": false,
            "message": "Client Info Found.",
            "data": clientInfo[0]
        });
    }
});

router.post('/getAccessHistory', [verifyToken, verifySalesPersonCantAccess], async (req, res) => {

    const client_id = req.body.client_id;
    let existingData = await client_model.getClientByID(client_id);

    if (isEmpty(existingData)) {
        return res.send({
            "error": true,
            "message": "Client Not Exist."
        });
    }


    let access_client_data = await access_client_model.getAccessClientHistoryByClient_id(client_id);

    return res.send({
        "error": false,
        "message": "Sister Concern access History.",
        "data": isEmpty(access_client_data) ? [] : access_client_data

    });
});



router.post('/reassign', [verifyToken, verifySalesPersonCantAccess], async (req, res) => {

    let reqData = {
        "present_saler_id": req.body.present_saler_id,
        "new_saler_id": req.body.new_saler_id,
        "is_assign_all": req.body.is_assign_all,
        "client_type": req.body.client_type,
        "client_id": req.body.client_id,
        "remove_reason": req.body.remove_reason
    }

    let errorMessage = "";
    let isError = 0;

    if (reqData.present_saler_id == undefined || isEmpty(await sales_person_model.getProfileById2(reqData.present_saler_id))) {
        isError = 1;
        errorMessage = "Present Sales person not found. ";
    }

    if (reqData.new_saler_id == undefined || isEmpty(await sales_person_model.getProfileById(reqData.new_saler_id))) {
        isError = 1;
        errorMessage += "Present Sales person not found. ";
    }

    if (reqData.new_saler_id == reqData.present_saler_id) {
        isError = 1;
        errorMessage += "Please select different sales person. ";
    }

    if (reqData.is_assign_all == undefined) {
        isError = 1;
        errorMessage += "is_assign_all is undefined. ";
    } else if (reqData.is_assign_all === false) {

        if (reqData.client_id == undefined) {
            isError = 1;
            errorMessage += "client_id is undefined. ";
        } else if (reqData.client_id.length < 1) {
            isError = 1;
            errorMessage += "Please Select Task for assign. ";
        }

    } else if (reqData.is_assign_all !== true) {

        isError = 1;
        errorMessage += "is_assign_all must be True or False ";
    }

    if (isError == 1) {
        return res.send({
            "error": true,
            "message": errorMessage
        });
    }

    let result = {};
    let date = new Date();
    const nowDateTime = date.getUTCFullYear() + '-' +
        ('00' + (date.getUTCMonth() + 1)).slice(-2) + '-' +
        ('00' + date.getUTCDate()).slice(-2) + ' ' +
        ('00' + date.getUTCHours()).slice(-2) + ':' +
        ('00' + date.getUTCMinutes()).slice(-2) + ':' +
        ('00' + date.getUTCSeconds()).slice(-2);


    // return res.send({
    //     'status': 400,
    //     'message': nowDateTime,
    //     "error": true
    // })

    if (reqData.client_type == 0) { // 0 = corporate , other = others type client

        let access_corporate_client_updateData = {
            "active_status": 1,
            "end_date": nowDateTime,
            "remove_reason": reqData.remove_reason === undefined ? "Client Exchange By admin" : reqData.remove_reason
        }

        let access_corporate_clients_newData = {
            "sales_person_id": reqData.new_saler_id,
            "company_id": 0,
            "company_sister_concern_id": 0,
            "remove_reason": ""
        }


        if (reqData.is_assign_all == true) {

            // first get all access corporate client list, then update & add new row for each update.

            let exixtingAccessCorporateList = await access_corporate_client_model.getAccessCorporateClientDataBysales_person_id(reqData.present_saler_id);
            //console.log(exixtingAccessCorporateList);

            for (let i = 0; i < exixtingAccessCorporateList.length; i++) {
                //update existing access info
                result = await access_corporate_client_model.updateAccessCorporateClientBySales_person_idAndCompany_sister_concern_id(reqData.present_saler_id, exixtingAccessCorporateList[i].company_sister_concern_id, access_corporate_client_updateData);

                access_corporate_clients_newData.company_id = exixtingAccessCorporateList[i].company_id;
                access_corporate_clients_newData.company_sister_concern_id = exixtingAccessCorporateList[i].company_sister_concern_id;

                // Add new access
                result = await access_corporate_client_model.addNew(access_corporate_clients_newData);
            }

        } else {

            for (let i = 0; i < reqData.client_id.length; i++) {

                // Check is it already assign in New Sales person ?
                result = await access_corporate_client_model.getAccessCorporateClientDataBycompany_sister_concern_idAndsales_person_id(reqData.client_id[i], reqData.new_saler_id);

                // Check is he owner of this client?
                let result2 = await access_corporate_client_model.getAccessCorporateClientDataBycompany_sister_concern_idAndsales_person_id(reqData.client_id[i], reqData.present_saler_id);

                if (isEmpty(result) && !isEmpty(result2)) {

                    //update existing access info
                    result = await access_corporate_client_model.updateAccessCorporateClientBySales_person_idAndCompany_sister_concern_id(reqData.present_saler_id, reqData.client_id[i], access_corporate_client_updateData);

                    let sisterConcertInfo = await company_sister_concerns_model.getSisterConcernByID(reqData.client_id[i]);

                    if (!isEmpty(sisterConcertInfo)) {
                        access_corporate_clients_newData.company_id = sisterConcertInfo[0].company_id;
                        access_corporate_clients_newData.company_sister_concern_id = reqData.client_id[i];

                        // Add new access
                        result = await access_corporate_client_model.addNew(access_corporate_clients_newData);

                    }
                }
            }
        }

    } else {

        let access_client_updateData = {
            "active_status": 1,
            "end_date": nowDateTime,
            "remove_reason": reqData.remove_reason === undefined ? "Client Exchange" : reqData.remove_reason
        }

        let access_clients_newData = {
            "sales_person_id": reqData.new_saler_id,
            "client_id": 0,
            "remove_reason": ""
        }


        if (reqData.is_assign_all == true) {

            // first get all access client list, then update & add new row for each update.

            let exixtingAccessClientList = await access_client_model.getAccessClientDataBysales_person_id(reqData.present_saler_id);

            for (let i = 0; i < exixtingAccessClientList.length; i++) {
                //update existing access info
                result = await access_client_model.updateAccessClientBySales_person_idAndClient_id(reqData.present_saler_id, exixtingAccessClientList[i].client_id, access_client_updateData);

                access_clients_newData.client_id = exixtingAccessClientList[i].client_id;

                // Add new access
                result = await access_client_model.addNew(access_clients_newData);
            }

        } else {

            for (let i = 0; i < reqData.client_id.length; i++) {

                // Check is it already assign in New Sales person ?
                result = await access_client_model.getAccessClientDataByclient_idAndsales_person_id(reqData.client_id[i], reqData.new_saler_id);

                // Check is he owner of this client?
                let result2 = await access_client_model.getAccessClientDataByclient_idAndsales_person_id(reqData.client_id[i], reqData.present_saler_id);

                if (isEmpty(result) && !isEmpty(result2)) { // check is sales person  already assign in this client

                    //update existing access info
                    result = await access_client_model.updateAccessClientBySales_person_idAndClient_id(reqData.present_saler_id, reqData.client_id[i], access_client_updateData);
                    access_clients_newData.client_id = reqData.client_id[i];

                    // Add new access
                    result = await access_client_model.addNew(access_clients_newData);


                }
            }
        }

    }

    // result = await task_model.reassignTask(reqData.present_saler_id, reqData.new_saler_id, reqData.is_assign_all, reqData.task_id);cc

    return res.send({
        "error": false,
        "message": "Client Change Successfully done",
        "data": ""
    });
})

router.post('/:id/searchIndividualClient', verifyToken, async (req, res) => {

    const client_type_id = req.params.id;

    if (client_type_id != 0) {

        let client_name = req.body.name;
        let userType = req.decoded.user_type === undefined ? 4 : req.decoded.user_type;
        let salesPersonId = req.decoded.id === undefined ? 0 : req.decoded.id;

        if (client_name == undefined || client_name.length < 1) {
            return res.send({
                "error": true,
                "message": "Need valid Input and Length should be at least 1",
            });
        }

        let result = await client_model.getListBySearching(userType, salesPersonId, client_name);

        return res.send({
            "error": false,
            "message": "Client List.",
            "count": result.length,
            "data": result
        });
    }

});


router.get('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "error": true
    })
});

router.post('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "error": true
    })
});

module.exports = router;