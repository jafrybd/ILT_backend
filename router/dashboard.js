const express = require("express");
const isEmpty = require("is-empty");
const router = express.Router();
const verifyToken = require('../jwt/verify/verifyLpgToken');
const task_model = require('../model/task');
const appointment_model = require('../model/appointment');
const sales_person_model = require('../model/salesPerson');
const common_model = require('../model/common');
const prospect_type_model = require('../model/prospectType');

router.get('/data', verifyToken, async (req, res) => {
    let userType = req.decoded.user_type === undefined ? 4 : req.decoded.user_type;
    let userId = req.decoded.id === undefined ? 0 : req.decoded.id;

    const d = new Date();
    const today = "'" + d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate() + "'";
    const fromDate = "'" + d.getFullYear() + '-' + (d.getMonth() + 1) + "-1'";

    let data = {};
    if (userType == 3 || userType == 2) {

        let prospectType = await prospect_type_model.getActiveProspectTypeList();

        // replace string -> perspect type 1 to perspect_type_1.
        for (let i = 0; i < prospectType.length; i++) {
            prospectType[i].type = prospectType[i].type.split(" ").join("_").replace(/[^a-zA-Z ]/g, "") + "_"+i;
        }

        data.prospestWiseTask = await task_model.getProspectTypeWiseTaskCount(prospectType);

        let leadsInfo = await common_model.getTotalCountTableRow('leads', 0, {
            "active_status": 0
        });
        data.totalLeads = isEmpty(leadsInfo) ? 0 : leadsInfo[0].total;

        let completeAppointmentListInfo = await common_model.getTotalCountTableRow('appointment', today, {
            "active_status": 0,
            "status": "complete"
        });
        data.totalCompleteAppointment = isEmpty(completeAppointmentListInfo) ? 0 : completeAppointmentListInfo[0].total;

        data.salesPersonWiseAppintmentList = await appointment_model.getSalesPersonWiseAppointmentListByDate(today);

        return res.send({
            'status': 200,
            'message': "",
            'data': data,
            "error": false
        });

    } else if (userType == 4) { // Sales Person

        let pendingTaskInfo = await common_model.getTotalCountTableRow('task', today, {
            'sales_person_id': userId,
            'status': "incomplete",
            "active_status": 0
        });
        data.totalPendingTask = isEmpty(pendingTaskInfo) ? 0 : pendingTaskInfo[0].total;

        let appointmentListInfo = await common_model.getTotalCountTableRow('appointment', today, {
            'sales_person_id': userId,
            "active_status": 0
        });
        data.totalAppointment = isEmpty(appointmentListInfo) ? 0 : appointmentListInfo[0].total;

        let prospectType = await prospect_type_model.getActiveProspectTypeList();

        for (let i = 0; i < prospectType.length; i++) {
            prospectType[i].type = prospectType[i].type.split(" ").join("_").replace(/[^a-zA-Z ]/g, "")+ "_"+i;;
        }
        
        data.prospestWiseTask = await task_model.getProspectTypeWiseTaskCount(prospectType, userId);

        let clientsListInfo = await common_model.getTotalCountTableRow('access_client', today, {
            'sales_person_id': userId,
            "active_status": 0
        });
        
       
        data.totalClients = isEmpty(clientsListInfo) ? 0 : clientsListInfo[0].total;

        return res.send({
            'status': 200,
            'message': "",
            'data': data,
            "error": false
        });



    } else if (userType == 1) {

        const salesPersonList = await sales_person_model.getSalesPersonList();
        let data = [];

        if (!isEmpty(salesPersonList)) {
            salesPersonList.map((salesPerson) => {
                data.push({
                    "sales_person_id": salesPerson.id,
                    "sales_name": salesPerson.name,
                    "total_task": 0,
                    "total_complete_task": 0,
                    "total_appointment": 0,
                    "total_complete_appointment": 0
                });
            })
        }

        const totalAppointmentList = await appointment_model.getSalesPersonWiseAppointmentListByDate(fromDate, today);

        for (let i = 0; i < totalAppointmentList.length; i++) {
            for (let j = 0; j < data.length; j++) {
                if (totalAppointmentList[i].sales_person_id == data[j].sales_person_id) {
                    data[j].total_appointment = totalAppointmentList[i].total_appointment;
                }
            }
        }

        const totalCompleteAppointmentList = await appointment_model.getSalesPersonWiseCompleteAppointmentListByDate(fromDate, today);

        for (let i = 0; i < totalCompleteAppointmentList.length; i++) {
            for (let j = 0; j < data.length; j++) {
                if (totalCompleteAppointmentList[i].sales_person_id == data[j].sales_person_id) {
                    data[j].total_complete_appointment = totalCompleteAppointmentList[i].total_appointment;
                }
            }
        }

        const totalTaskList = await task_model.getSalesPersonWiseTaskListByDate(fromDate, today);

        for (let i = 0; i < totalTaskList.length; i++) {
            for (let j = 0; j < data.length; j++) {
                if (totalTaskList[i].sales_person_id == data[j].sales_person_id) {
                    data[j].total_task = totalTaskList[i].total_task;
                }
            }
        }

        const totalCompleteTaskList = await task_model.getSalesPersonWiseCompleteTaskListByDate(fromDate, today);

        for (let i = 0; i < totalCompleteTaskList.length; i++) {
            for (let j = 0; j < data.length; j++) {
                if (totalCompleteTaskList[i].sales_person_id == data[j].sales_person_id) {
                    data[j].total_complete_task = totalCompleteTaskList[i].total_task;
                }
            }
        }


        return res.send({
            'status': 200,
            'message': "",
            'data': data,
            "error": false
        });

    } else {
        return res.send({
            'status': 200,
            'message': "unknown reqiest",
            "error": false
        })
    }

});

router.get('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "error": true
    })
});

router.post('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "error": true
    })
});

module.exports = router;