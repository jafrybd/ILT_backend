const express = require("express");
const isEmpty = require("is-empty");
const router = express.Router();
const verifyToken = require('../jwt/verify/verifyLpgToken');
const prospect_type_model = require('../model/prospectType');

const verifySalesPersonCantAccess = require('../jwt/verify/verifyLpgSalesPersonCantAccess');


router.get('/list', verifyToken, async (req, res) => { /// Active Prospect type

    let result = await prospect_type_model.getActiveProspectTypeList();

    return res.send({
        "error": false,
        "message": "Prospect  type List.",
        "data": result
    });
});

router.get('/allList', verifyToken, async (req, res) => {

    let result = await prospect_type_model.getAllList();

    return res.send({
        "error": false,
        "message": "Prospect Type  List.",
        "data": result
    });
});


router.get('/deactiveList', verifyToken, async (req, res) => {

    let result = await prospect_type_model.getDeactiveProspectTypeList();

    return res.send({
        "error": false,
        "message": "Deactive Prospect Type List.",
        "data": result
    });
});

router.get('/activeList', verifyToken, async (req, res) => {

    let result = await prospect_type_model.getActiveProspectTypeList();

    return res.send({
        "error": false,
        "message": "Active Prospect Type  List.",
        "data": result
    });
});


router.post('/add', [verifyToken, verifySalesPersonCantAccess], async (req, res) => {

    let reqData = {
        "type": req.body.type,
        "active_status": 0
    }


    if (reqData.type == undefined || reqData.type.length < 2) {
        return res.send({
            "error": true,
            "message": "Need valid Prospect Type Type and length must then 1.",
        });
    }

    let existingData = await prospect_type_model.getProspectTypeByType(reqData.type);

    if (!isEmpty(existingData)) {
        return res.send({
            "error": true,
            "message": existingData[0].active_status == "0" ? "Prospect Type type Already Exist." : "Prospect Type type Exist but Deactivate, Please contect to Admin."
        });
    }

    let result = await prospect_type_model.addNewProspectType(reqData);

    if (result.affectedRows == undefined || result.affectedRows < 1) {
        return res.send({
            "error": true,
            "message": "Some thing Wrong in system database."
        });
    }

    return res.send({
        "error": false,
        "message": "Prospect Type type Added."
    });
});

router.post('/delete', [verifyToken, verifySalesPersonCantAccess], async (req, res) => {

    let id = req.body.id;

    try {
        id = parseInt(id);
    } catch (e) {
        id = "";
    }

    if (!id || typeof (id) !== "number" || id < 1) {
        return res.send({
            "error": true,
            "message": "invalid id."
        });
    }

    let existingData = await prospect_type_model.getProspectTypeByID(id);

    if (isEmpty(existingData)) {
        return res.send({
            "error": true,
            "message": "Prospect Type ID Not Exist."
        });
    }

    let result = await prospect_type_model.deactiveProspectType(id);

    return res.send({
        "error": false,
        "message": `Prospect Type '${existingData[0].type}' Deleted Successfully .`,
        "data": []
    });
});

router.post('/active', [verifyToken, verifySalesPersonCantAccess], async (req, res) => {

    let id = req.body.id;

    try {
        id = parseInt(id);
    } catch (e) {
        id = "";
    }

    if (!id || typeof (id) !== "number" || id < 1) {
        return res.send({
            "error": true,
            "message": "invalid id."
        });
    }

    let existingData = await prospect_type_model.getProspectTypeByID(id);

    if (isEmpty(existingData)) {
        return res.send({
            "error": true,
            "message": "Prospect  ID Not Exist."
        });
    }


    if (existingData[0].active_status === 0) {
        return res.send({
            "error": true,
            "message": "Already this is Active"
        });
    }
    let result = await prospect_type_model.activeProspectType(id);

    return res.send({
        "error": false,
        "message": "Prospect Type Activated."

    });
});

router.post('/update', [verifyToken, verifySalesPersonCantAccess], async (req, res) => {

    let type = req.body.type;
    let id = req.body.id;

    if (type == undefined || type.length < 2) {
        return res.send({
            "error": true,
            "message": "Need valid Prospect Type Type and length must then 1.",
        });
    }

    let existingData = await prospect_type_model.getProspectTypeByType(type);

    if (!isEmpty(existingData) && existingData[0].id !== id) {
        return res.send({
            "error": true,
            "message": existingData[0].active_status == "0" ? "Prospect Type type Already Exist." : "Prospect Type type Exist but Deactivate, Please contect to Admin."
        });
    }

    existingData = await prospect_type_model.getProspectTypeByID(id);

    if (isEmpty(existingData)) {
        return res.send({
            "error": true,
            "message": "Prospect Type ID Not Exist."
        });
    }


    let result = await prospect_type_model.updateProspectTypeByID(id, type);

    if (result.affectedRows == undefined || result.affectedRows == 0) {
        return res.send({
            "error": true,
            "message": "Some thing Wrong in system database."
        });
    }

    return res.send({
        "error": false,
        "message": "Prospect Type Update Successfully Done."
    });

});

router.get('/name/:id', verifyToken, async (req, res) => {

    const id = req.params.id;


    const prospect_name = await prospect_type_model.getProspectNameByID(id);

    if (isEmpty(prospect_name)) {
        return res.send({
            "error": true,
            "message": "Prospect Not Exist."
        });
    } else {

        return res.send({
            "error": false,
            "message": "Prospect Name Found.",
            "data": prospect_name[0]
        });
    }
});

router.get('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "error": true
    })
});

router.post('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "error": true
    })
});

module.exports = router;