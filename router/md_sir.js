const express = require("express");
const isEmpty = require("is-empty");
const router = express.Router();
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const mdSir_model = require('../model/md_sir');
const common_model = require('../model/common');
const task_discussion_types_model = require('../model/task_discussion_types');
const task_vehicle_types_model = require('../model/task_vehicle_types');
const company_sister_concerns_model = require('../model/company_sister_concerns');
const client_model = require('../model/client');

const verifyToken = require('../jwt/verify/verifyLpgToken');
const verifySalesPersonCantAccess = require('../jwt/verify/verifyLpgSalesPersonCantAccess');
const verifySuperAdminCantAccess = require('../jwt/verify/verifyLpgSuperAdminCantAccess');
const verifySubAdminCantAccess = require('../jwt/verify/verifyLpgSubAdminCantAccess');


router.get('/profile/view/:id', verifyToken, async (req, res) => {

    const id = req.params.id;
    const userInfo = await mdSir_model.getProfileById(id);

    if (isEmpty(userInfo)) {
        return res.send({
            "error": true,
            "message": "Md Sir Profile Not Exist."
        });
    } else {
        delete userInfo[0].active_status;
        return res.send({
            "error": false,
            "message": "Md Sir Profile Found.",
            "data": userInfo[0]
        });
    }
});


router.post('/profile/pinChange', [verifyToken, verifySubAdminCantAccess, verifySalesPersonCantAccess], async (req, res) => {

    let updateRequestData = {
        "id": req.body.id,
        "new_pin": req.body.new_pin,
    }

    let existingMdSirProfileInfo = await mdSir_model.getProfileById(updateRequestData.id);

    if (isEmpty(existingMdSirProfileInfo)) {
        return res.send({
            "error": true,
            "message": "User Not Exist."
        });
    }

    const existinUserInfo = await common_model.getUserByProfileIdAndUserType(existingMdSirProfileInfo[0].id, 1);

    if (isEmpty(existinUserInfo)) {
        return res.send({
            "error": true,
            "message": "User Not Exist."
        });
    }

    if (updateRequestData.new_pin == undefined || updateRequestData.new_pin.length < 4) {
        return res.send({
            "error": true,
            "message": "Give Valid Pin and Length should be greater than 4."
        });

    } else {

        updateRequestData.new_pin = bcrypt.hashSync(updateRequestData.new_pin, 10); // hasing Pin
        let result = await common_model.passwordChangeForUser(existinUserInfo[0].id, updateRequestData.new_pin);

        if (!isEmpty(result) && result.affectedRows == 0) {
            return res.send({
                "error": true,
                "message": "Pin Change Fail try again"
            });
        } else {
            return res.send({
                "error": false,
                "message": "Pin Change Successfully Done"
            });
        }
    }
});

router.post('/login', async (req, res) => {

    let loginData = {
        "password": req.body.pin
    }

    let errorMessage = "";

    if (loginData.password == undefined || loginData.password.length < 4) {
        return res.send({
            "error": true,
            "message": "Need valid pin."
        });
    }

    let userData = await common_model.getUserByUserType(1);

    if (isEmpty(userData)) {
        return res.send({
            "error": true,
            "message": "No user found."
        });
    }

    if (bcrypt.compareSync(loginData.password, userData[0].password)) {

        let profileData =  await mdSir_model.getProfileById(userData[0].profile_id);
        

        if (profileData[0].active_status == 1) {
            return res.send({
                "error": true,
                "message": "Your account is Delete, You can't access this System."
            });

        } else {

            // Delete some data, as Client no need to access this data
            delete userData[0].password;
            delete userData[0].profile_id;
            userData[0].profile = profileData[0];
            delete userData[0].profile.active_status;
            delete userData[0].phone_number;


            delete loginData.password;

            // Add Data in Token
            loginData.user_id = userData[0].id;
            loginData.id = userData[0].profile.id;
            loginData.name = userData[0].profile.name;
            loginData.user_type = userData[0].user_type;


            //  "Generate Token"
            let token = jwt.sign(loginData, global.config.secretKey, {
                algorithm: global.config.algorithm,
                expiresIn: '1440m' // one day
            });

            userData[0].token = token;

            return res.send({
                "error": false,
                "message": errorMessage,
                'data': userData[0]
            });
        }

    } else {
        return res.send({
            "error": true,
            "message": "Wrong Password"
        });
    }

});


router.post('/searchTask',  [verifyToken, verifySubAdminCantAccess, verifySalesPersonCantAccess], async (req, res) => {

    let searchField = {
        "month": req.body.month,
        "year": req.body.year
    };

    if (isEmpty(req.body.month)) {
        searchField.month = 0;
    }

    if (isEmpty(req.body.year)) {
        return res.send({
            "error": true,
            "message": "Please Select a Year"
        });
    }

    if (req.body.month !== undefined && req.body.month != "") {
        searchField.month = req.body.month;
    }

    if (req.body.year !== undefined && req.body.year != "") {
        searchField.year = req.body.year;
    }


    const taskList = await mdSir_model.getTaskListBySearching(searchField);

    for (let i = 0; i < taskList.length; i++) {

        if (taskList[i].client_type_id == 0) {
            taskList[i].client_info = await company_sister_concerns_model.getSisterConcernAndCompanyByID(taskList[i].client_id);
        } else
            taskList[i].client_info = await client_model.getClientByID(taskList[i].client_id);

        if (!isEmpty(taskList[i].client_info)) { // Delete some data
            if (taskList[i].client_info[0].hasOwnProperty("user_id")) delete taskList[i].client_info[0].user_id;
            if (taskList[i].client_info[0].hasOwnProperty("created_at")) delete taskList[i].client_info[0].created_at;
            if (taskList[i].client_info[0].hasOwnProperty("active_status")) delete taskList[i].client_info[0].active_status;
        }


        taskList[i].discussion_type = await task_discussion_types_model.getExistingDiscussion_TypeNameAndIdListByTask_id(taskList[i].id);
        taskList[i].vehicle_type = await task_vehicle_types_model.getExistingVehicle_TypeNameAndIdListByTask_id(taskList[i].id);

        delete taskList[i].client_id;

    }

    return res.send({
        "error": false,
        "message": "Task list",
        "count" : taskList.length,
        "data": taskList
    });

});


router.post('/searchAppointment',  [verifyToken, verifySubAdminCantAccess, verifySalesPersonCantAccess], async (req, res) => {

    let searchField = {
        "month": req.body.month,
        "year": req.body.year
    };

    if (isEmpty(req.body.month)) {
        searchField.month = 0;
    }

    if (isEmpty(req.body.year)) {
        return res.send({
            "error": true,
            "message": "Please Select a Year"
        });
    }
    
    
    if (req.body.month !== undefined && req.body.month != "") {
        searchField.month = req.body.month;
    }

    if (req.body.year !== undefined && req.body.year != "") {
        searchField.year = req.body.year;
    }

    


    const appointmentList = await mdSir_model.getAppointmentListBySearching(searchField);

    for (let i = 0; i < appointmentList.length; i++) {

        if (appointmentList[i].client_type_id == 0) {
            appointmentList[i].client_info = await company_sister_concerns_model.getSisterConcernAndCompanyByID(appointmentList[i].client_id);
        } else
            appointmentList[i].client_info = await client_model.getClientByID(appointmentList[i].client_id);

        if (!isEmpty(appointmentList[i].client_info)) { // Delete some data
            if (appointmentList[i].client_info[0].hasOwnProperty("user_id")) delete appointmentList[i].client_info[0].user_id;
            if (appointmentList[i].client_info[0].hasOwnProperty("created_at")) delete appointmentList[i].client_info[0].created_at;
            if (appointmentList[i].client_info[0].hasOwnProperty("active_status")) delete appointmentList[i].client_info[0].active_status;
        }


        delete appointmentList[i].client_id;

    }

    return res.send({
        "error": false,
        "message": "Appointment list",
        "count" : appointmentList.length,
        "data": appointmentList
    });

});



router.get('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "error": true
    })
});


router.post('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "error": true
    })
});
module.exports = router;