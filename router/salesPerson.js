const express = require("express");
const isEmpty = require("is-empty");
const router = express.Router();
const bcrypt = require('bcrypt');
const salesPerson_model = require('../model/salesPerson');
const common_model = require('../model/common');
const verifyToken = require('../jwt/verify/verifyLpgToken');

const verifySalesPersonCantAccess = require('../jwt/verify/verifyLpgSalesPersonCantAccess');
const verifySuperAdminCantAccess = require('../jwt/verify/verifyLpgSuperAdminCantAccess');
const verifySubAdminCantAccess = require('../jwt/verify/verifyLpgSubAdminCantAccess');

router.post('/registration', [verifyToken, verifySalesPersonCantAccess], async (req, res) => {

    let userData = {
        "password": req.body.password,
        "phone_number": req.body.phone,
        "department_id": 1,
        "user_type": 4,
        "profile_id": 0
    }

    let extraUserInfo = {
        "name": req.body.name,
        "email": req.body.email
    };

    let errorMessage = "";
    let isError = 0;

    if (extraUserInfo.name == undefined || extraUserInfo.name.length < 2) {
        isError = 1;
        errorMessage = "Need valid name and length getter then 2. ";
    }

    if (userData.password == undefined || userData.password.length < 6) {
        isError = 1;
        errorMessage += "Need valid password and length getter then 6. ";
    }

    if (userData.phone_number == undefined || userData.phone_number.length != 11) {
        isError = 1;
        errorMessage += "Phone number invalid.";
    }

    if (isError == 1) {
        return res.send({
            "error": true,
            "message": errorMessage
        });
    }


    let getUserData = await common_model.getUserByPhone(userData.phone_number);
    if (!isEmpty(getUserData)) {
        return res.send({
            "error": true,
            "message": "Phone number Already Use."
        });
    } else {

        let salesPersonData = {
            "name": extraUserInfo.name,
            "phone_number": userData.phone_number,
            "used_phone_no": userData.phone_number,
            "email": extraUserInfo.email,
            "image": "sales_person.jpg",
            "active_status": 0,
            "password_recovery_questions_id": 0,
            "question_ans": ""
        }

        let result = await salesPerson_model.addNewSalesPerson(salesPersonData);

        if (result.affectedRows == undefined || result.affectedRows < 1) {
            return res.send({
                "error": true,
                "message": "Some thing Wrong in system database."
            });
        }

        userData.profile_id = result.insertId;
        userData.password = bcrypt.hashSync(userData.password, 10); // hasing Password
        result = await common_model.addNewUser(userData);

        if (result.affectedRows == undefined || result.affectedRows < 1) {
            return res.send({
                "error": true,
                "message": "Some thing Wrong in system database."
            });
        }


        return res.send({
            "error": false,
            "message": "Sales Person successfully added.",
            "data": {
                "phone": userData.phone_number,
                "name": salesPersonData.name
            }
        });
    }
});


router.get('/list', verifyToken, async (req, res) => {

    let result = await salesPerson_model.getSalesPersonList();

    return res.send({
        "error": false,
        "message": "Sales List.",
        "data": result
    });
});

router.get('/allList', verifyToken, async (req, res) => {

    let result = await salesPerson_model.getAllSalesPersonList();

    return res.send({
        "error": false,
        "message": "Sales Person List.",
        "data": result
    });
});

router.get('/delete_list', verifyToken, async (req, res) => {

    let result = await salesPerson_model.getSalesPersonDeleteList();

    return res.send({
        "error": false,
        "message": "Sales Delete List.",
        "data": result
    });
});

router.post('/delete', [verifyToken, verifySalesPersonCantAccess], async (req, res) => {

    let id = req.body.id;

    try {
        id = parseInt(id);
    } catch (e) {
        id = "";
    }

    if (!id || typeof (id) !== "number" || id < 1) {
        return res.send({
            "error": true,
            "message": "invalid id."
        });
    }

    let userData = await common_model.getUserByProfileIdAndUserType(id, 4);

    if (!isEmpty(userData)) {

        if (userData[0].phone_number === "0100") {
            return res.send({
                "error": true,
                "message": "Salesperson Profile already Deleted."
            });
        }

        let salesPersonProfile = await salesPerson_model.getProfileById2(id);
        if (isEmpty(salesPersonProfile)) {
            return res.send({
                "error": false,
                "message": "Unknown SalesPerson"
            });
        }

        let result = await salesPerson_model.deactiveAccount(id);

        if (isEmpty(result) || !result.hasOwnProperty("affectedRows") || (result.affectedRows == 0)) {
            console.log(result);
            return res.send({
                "error": true,
                "message": "Account Delete Fail No account found."
            });
        }

        result = await salesPerson_model.updateProfileById(id, { "phone_number": "0100" });

        if (isEmpty(result) || !result.hasOwnProperty("affectedRows") || (result.affectedRows == 0)) {
            return res.send({
                "error": true,
                "message": "Account Delete Fail try again."
            });
        }

        result = await common_model.updateUserById(userData[0].id, { "phone_number": "0100" });

        if (isEmpty(result) || !result.hasOwnProperty("affectedRows") || (result.affectedRows == 0)) {
            return res.send({
                "error": true,
                "message": "Account Delete Fail try again."
            });
        }


        return res.send({
            "error": false,
            "message": "Sales Person (Phone No: " + userData[0].phone_number + ", Name: " + salesPersonProfile[0].name + ") Delete Successfully Done."
        });

    } else {
        return res.send({
            "error": false,
            "message": "Some thing wrong User Not found.",
            "data": []
        });
    }
});

router.post('/profile/update', [verifyToken], async (req, res) => {

    const updateRequestData = {
        "id": req.body.id,
        "phone_number": req.body.phone,
        "name": req.body.name,
        "email": req.body.email
    }

    const existinUserInfo = await salesPerson_model.getProfileById(updateRequestData.id);

    if (isEmpty(existinUserInfo)) {
        return res.send({
            "error": true,
            "message": "User Not Exist."
        });
    }

    let updateData = {};

    let errorMessage = "";
    let isError = 0; // 0 = No
    let willWeUpdate = 1; // 0 = yes;

    if (existinUserInfo[0].name !== updateRequestData.name) {

        if (updateRequestData.name == undefined || updateRequestData.name.length < 2) {
            isError = 1;
            errorMessage = "Need valid name and length getter then 2. ";
        } else {
            willWeUpdate = 0;
            updateData.name = updateRequestData.name;
        }
    }

    //use in future 
    // if (existinUserInfo[0].phone_number !== updateRequestData.phone_number) {

    //     if (updateRequestData.phone_number == undefined || updateRequestData.phone_number.length != 11) {
    //         isError = 1;
    //         errorMessage += "Phone number invalid. ";
    //     } else {

    //         let userINPhoneNumber = await common_model.getUserByPhone(updateRequestData.phone_number);

    //         if (!isEmpty(userINPhoneNumber)){
    //             isError = 1;
    //             errorMessage += "Phone number already use.";
    //         } else {
    //             willWeUpdate = 0;
    //             updateData.phone_number = updateRequestData.phone_number;
    //         }

    //     }
    // }

    if (existinUserInfo[0].email !== updateRequestData.email) {

        if (isEmpty(updateRequestData.email)) {
            willWeUpdate = 0;
            updateData.email = updateRequestData.email;
        } else {
            const re = /\S+@\S+\.\S+/;
            if (!re.test(updateRequestData.email)) {
                isError = 1;
                errorMessage += " Email is invalid.";
            } else {
                willWeUpdate = 0;
                updateData.email = updateRequestData.email;
            }
        }
    }

    if (isError == 1) {
        return res.send({
            "error": true,
            "message": errorMessage
        });
    }

    let result = {};
    if (willWeUpdate == 0) {
        result = await salesPerson_model.updateProfileById(updateRequestData.id, updateData);
    }

    if (!isEmpty(result) && result.affectedRows == 0) {
        return res.send({
            "error": true,
            "message": "Update Fail try again"
        });
    } else {
        return res.send({
            "error": false,
            "message": "Update Successfully Done"
        });
    }

});


//// Forget Password Question Update

router.post('/profile/questionUpdate', [verifyToken], async (req, res) => {

    const updateRequestData = {
        "id": req.body.id,
        "password_recovery_questions_id": req.body.password_recovery_questions_id,
        "question_ans": req.body.question_ans

    }

    const existinUserInfo = await salesPerson_model.getProfileById(updateRequestData.id);

    if (isEmpty(existinUserInfo)) {
        return res.send({
            "error": true,
            "message": "User Not Exist."
        });
    }

    if (updateRequestData.question_ans === undefined || isEmpty(updateRequestData.question_ans) || updateRequestData.question_ans.length < 4 || updateRequestData.question_ans.length > 25) {
        return res.send({
            "error": true,
            "message": "Question answer length must gather then 4 and less then 25."
        });
    }


    let updateData = {};

    let errorMessage = "";
    let isError = 0; // 0 = No
    let willWeUpdate = 1; // 0 = yes;

    if (existinUserInfo[0].password_recovery_questions_id !== updateRequestData.password_recovery_questions_id) {


        let passwordRecoveryQuestionId = await salesPerson_model.getPasswordRecoveryQuestionId(updateRequestData.password_recovery_questions_id);

        if (updateRequestData.password_recovery_questions_id == undefined || isEmpty(passwordRecoveryQuestionId)) {
            isError = 1;
            errorMessage += " Question is not valid.";
        }
        else {
            willWeUpdate = 0;
            updateData.password_recovery_questions_id = updateRequestData.password_recovery_questions_id;
        }
    }

    updateData.question_ans = bcrypt.hashSync(updateRequestData.question_ans, 10);

    if (isError == 1) {
        return res.send({
            "error": true,
            "message": errorMessage
        });
    }

    let result = {};
    if (willWeUpdate == 0) {
        result = await salesPerson_model.updateProfileById(updateRequestData.id, updateData);
    }

    if (!isEmpty(result) && result.affectedRows == 0) {
        return res.send({
            "error": true,
            "message": "Question Update Fail try again"
        });
    } else {
        return res.send({
            "error": false,
            "message": "Question Update Successfully Done"
        });
    }

});


router.get('/profile/view/:id', verifyToken, async (req, res) => {

    const id = req.params.id;
    const user_type = req.decoded.user_type;

    if (user_type === 4) {
        const userInfo = await salesPerson_model.getProfileById(id);



        if (isEmpty(userInfo)) {
            return res.send({
                "error": true,
                "message": "Sales Person Not Exist."
            });
        } else {

            return res.send({
                "error": false,
                "message": "Sales Person Found.",
                "data": userInfo[0]
            });
        }
    }
    else {

        const userInfo = await salesPerson_model.getSalesPersonProfileByIdForOthers(id);
        const taskCount = await salesPerson_model.getTaskCountBySalespersonId(id);
        userInfo[0].taskCount = taskCount[0];

        if (isEmpty(userInfo)) {
            return res.send({
                "error": true,
                "message": "Sales Person Not Exist."
            });
        } else {

            return res.send({
                "error": false,
                "message": "Sales Person Found.",
                "data": userInfo[0]
            });
        }
    }


});


router.post('/profile/passwordChange', [verifyToken, verifySuperAdminCantAccess, verifySubAdminCantAccess], async (req, res) => {

    let updateRequestData = {
        "id": req.body.id,
        "old_password": req.body.old_password,
        "new_password": req.body.new_password,
    }

    let existingSalesPersonInfo = await salesPerson_model.getProfileById(updateRequestData.id);

    if (isEmpty(existingSalesPersonInfo)) {
        return res.send({
            "error": true,
            "message": "User Not Exist."
        });
    }

    const existinUserInfo = await common_model.getUserByProfileIdAndUserType(existingSalesPersonInfo[0].id, 4);

    if (isEmpty(existinUserInfo)) {
        return res.send({
            "error": true,
            "message": "User Not Exist."
        });
    }


    if (bcrypt.compareSync(updateRequestData.old_password, existinUserInfo[0].password)) {
        if (updateRequestData.new_password == undefined || updateRequestData.new_password.length < 6) {
            return res.send({
                "error": true,
                "message": "Give Valid Password and Length should be greater than 6."
            });
        } else {

            updateRequestData.new_password = bcrypt.hashSync(updateRequestData.new_password, 10); // hasing Password
            let result = await common_model.passwordChangeForUser(existinUserInfo[0].id, updateRequestData.new_password);

            if (!isEmpty(result) && result.affectedRows == 0) {
                return res.send({
                    "error": true,
                    "message": "Password Change Fail try again"
                });
            } else {
                return res.send({
                    "error": false,
                    "message": "Password Change Successfully Done"
                });
            }

        }
    } else {
        return res.send({
            "error": true,
            "message": "Existing Password is wrong"
        });
    }

});


router.post('/profile/resetPassword', [verifyToken, verifySalesPersonCantAccess], async (req, res) => {

    let updateRequestData = {
        "id": req.body.id
    }

    let existingSalesPersonInfo = await salesPerson_model.getProfileById(updateRequestData.id);


    if (isEmpty(existingSalesPersonInfo)) {
        return res.send({
            "error": true,
            "message": "User Not Exist."
        });
    }

    const existinUserInfo = await common_model.getUserByProfileIdAndUserType(existingSalesPersonInfo[0].id, 4);

    if (isEmpty(existinUserInfo)) {
        return res.send({
            "error": true,
            "message": "User Not Exist."
        });
    }

    existinUserInfo[0].password = bcrypt.hashSync("123456", 10); // hasing Password


    let result = await common_model.passwordChangeForUser(existinUserInfo[0].id, existinUserInfo[0].password);

    if (!isEmpty(result) && result.affectedRows == 0) {
        return res.send({
            "error": true,
            "message": "Update Fail try again"
        });
    } else {
        return res.send({
            "error": false,
            "message": "Password Reset Successfully Done"
        });
    }

});


router.post('/profile/forgetPassword', async (req, res) => {

    let updateRequestData = {
        "phone": req.body.phone,
        "password_recovery_questions_id": req.body.password_recovery_questions_id,
        "question_ans": req.body.question_ans,
        "new_password": req.body.new_password,

    }

    let existingSalesPersonInfo = await salesPerson_model.getProfileByPhoneNumber(updateRequestData.phone);

    if (isEmpty(existingSalesPersonInfo)) {
        return res.send({
            "error": true,
            "message": "User Not Exist."
        });
    }



    if (!isEmpty(existingSalesPersonInfo[0].password_recovery_questions_id)) {
        let passwordRecoveryQuestionId = await salesPerson_model.getPasswordRecoveryQuestionId(updateRequestData.password_recovery_questions_id);


        if (!isEmpty(passwordRecoveryQuestionId)) {
            if (existingSalesPersonInfo[0].password_recovery_questions_id === updateRequestData.password_recovery_questions_id) {
                let existingAnswer = existingSalesPersonInfo[0].question_ans;
                let givenAnswer = updateRequestData.question_ans;


                if (bcrypt.compareSync(givenAnswer, existingAnswer)) {

                    if (updateRequestData.new_password == undefined || updateRequestData.new_password.length < 6) {
                        return res.send({
                            "error": true,
                            "message": "Give Valid Password and Length should be greater than 6."
                        });
                    } else {

                        const existinUserInfo = await common_model.getUserByProfileIdAndUserType(existingSalesPersonInfo[0].id, 4);

                        if (isEmpty(existinUserInfo)) {
                            return res.send({
                                "error": true,
                                "message": "User Not Exist."
                            });
                        }

                        updateRequestData.new_password = bcrypt.hashSync(updateRequestData.new_password, 10); // hasing Password
                        let result = await common_model.passwordChangeForUser(existinUserInfo[0].id, updateRequestData.new_password);

                        if (!isEmpty(result) && result.affectedRows == 0) {
                            return res.send({
                                "error": true,
                                "message": "Update Fail try again."
                            });
                        } else {
                            return res.send({
                                "error": false,
                                "message": "Password Reset Successfully Done"
                            });
                        }

                    }

                } else {
                    return res.send({
                        "error": true,
                        "message": "Answer is not matched"
                    });
                }
            }
            else {
                return res.send({
                    "error": true,
                    "message": "Question is not matched"
                });
            }

        }
        else {
            return res.send({
                "error": true,
                "message": "Invalid Question"
            });
        }
    }

    else {
        return res.send({
            "error": true,
            "message": "Please Select a Password Recovery Question."
        });
    }

});



router.get('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "error": true
    })
});


router.post('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "error": true
    })
});
module.exports = router;