const express = require("express");
const isEmpty = require("is-empty");
const router = express.Router();
const verifyToken = require('../jwt/verify/verifyLpgToken');
const vehicle_type_model = require('../model/vehicleType');

const verifySalesPersonCantAccess = require('../jwt/verify/verifyLpgSalesPersonCantAccess');
const verifySuperAdminCantAccess = require('../jwt/verify/verifyLpgSuperAdminCantAccess');
const verifySubAdminCantAccess = require('../jwt/verify/verifyLpgSubAdminCantAccess');


router.get('/list', verifyToken, async (req, res) => { /// Active Vehicle type

    let result = await vehicle_type_model.getActiveVehicleTypeList();

    return res.send({
        "error": false,
        "message": "Vehicle type List.",
        "data": result
    });
});


router.get('/allList', verifyToken, async (req, res) => {

    let result = await vehicle_type_model.getAllList();

    return res.send({
        "error": false,
        "message": "Vehicle type List.",
        "data": result
    });
});

router.get('/deactiveList', verifyToken, async (req, res) => {
    let result = await vehicle_type_model.getDeactiveVehicleTypeList();

    return res.send({
        "error": false,
        "message": "Deactive Vehicle Type List.",
        "data": result
    });
});

router.get('/activeList', verifyToken, async (req, res) => {
    let result = await vehicle_type_model.getActiveVehicleTypeList();

    return res.send({
        "error": false,
        "message": "Active Vehicle Type List.",
        "data": result
    });
});

router.post('/add', [verifyToken, verifySalesPersonCantAccess], async (req, res) => {

    let reqData = {
        "type": req.body.type,
        "active_status": 0
    }


    if (reqData.type == undefined || reqData.type.length < 2) {
        return res.send({
            "error": true,
            "message": "Need valid Vehicle Type and length must then 1.",
        });
    }

    let existingData = await vehicle_type_model.getVehicleTypeByType(reqData.type);

    if (!isEmpty(existingData)) {
        return res.send({
            "error": true,
            "message": existingData[0].active_status == "0" ? "Vehicle type Already Exist." : "Vehicle type Exist but Deactivate, Please contect to Admin."
        });
    }

    let result = await vehicle_type_model.addNewVehicleType(reqData);

    if (result.affectedRows == undefined || result.affectedRows < 1) {
        return res.send({
            "error": true,
            "message": "Some thing Wrong in system database."
        });
    }

    return res.send({
        "error": false,
        "message": "Vehicle type Added."
    });
});

router.post('/delete', [verifyToken, verifySalesPersonCantAccess], async (req, res) => {

    let id = req.body.id;

    try {
        id = parseInt(id);
    } catch (e) {
        id = "";
    }

    if (!id || typeof (id) !== "number" || id < 1) {
        return res.send({
            "error": true,
            "message": "invalid id."
        });
    }

    let existingData = await vehicle_type_model.getVehicleTypeByID(id);

    if (isEmpty(existingData)) {
        return res.send({
            "error": true,
            "message": "Vehical ID Not Exist."
        });
    }

    let result = await vehicle_type_model.deactiveVehicleType(id);

    return res.send({
        "error": false,
        "message": `Vehicle type '${existingData[0].type}' Deleted Successfully .`,
        "data": []
    });
});

router.post('/active', [verifyToken, verifySalesPersonCantAccess], async (req, res) => {

    let id = req.body.id;

    try {
        id = parseInt(id);
    } catch (e) {
        id = "";
    }

    if (!id || typeof (id) !== "number" || id < 1) {
        return res.send({
            "error": true,
            "message": "invalid id."
        });
    }

    let existingData = await vehicle_type_model.getVehicleTypeByID(id);

    if (isEmpty(existingData)) {
        return res.send({
            "error": true,
            "message": "Vehicle ID Not Exist."
        });
    }


    if (existingData[0].active_status === 0) {
        return res.send({
            "error": true,
            "message": "Already this is Active"
        });
    }
    let result = await vehicle_type_model.activeVehicleType(id);

    return res.send({
        "error": false,
        "message": "Vehicle Type Activated."

    });
});

router.post('/update', [verifyToken, verifySalesPersonCantAccess], async (req, res) => {

    let type = req.body.type;
    let id = req.body.id;

    if (type == undefined || type.length < 2) {
        return res.send({
            "error": true,
            "message": "Need valid Vehicle Type and length must then 1.",
        });
    }

    let existingData = await vehicle_type_model.getVehicleTypeByType(type);

    if (!isEmpty(existingData) && existingData[0].id !== id) {
        return res.send({
            "error": true,
            "message": existingData[0].active_status == "0" ? "Vehicle type Already Exist." : "Vehicle type Exist but Deactivate, Please contect to Admin."
        });
    }

    existingData = await vehicle_type_model.getVehicleTypeByID(id);

    if (isEmpty(existingData)) {
        return res.send({
            "error": true,
            "message": "Vehical ID Not Exist."
        });
    }


    let result = await vehicle_type_model.updateVehicleTypeByID(id, type);

    if (result.affectedRows == undefined || result.affectedRows == 0) {
        return res.send({
            "error": true,
            "message": "Some thing Wrong in system database."
        });
    }

    return res.send({
        "error": false,
        "message": "Vehicle Update Successfully Done."
    });

});


router.get('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "error": true
    })
});

router.post('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "error": true
    })
});


module.exports = router;