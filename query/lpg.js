// Super Admin

let getSuperAdminList = () => {
    return "SELECT id, name, image, phone_number, email FROM `super_admins` where visibility = 0 and active_status = 0";
}

let getSuperAdminDeactiveList = () => {
    return "SELECT id, name, image, used_phone_no, email FROM `super_admins` where active_status = 1";
}

let registerSuperAdminAccount = () => {
    return "INSERT INTO `super_admins` SET ? ";
}

let deactiveSuperAccount = () => {
    return "UPDATE `super_admins` set active_status = 1 where id = ?";
}

let getSuperAdminProfileById = () => {
    return "SELECT id, name, image, phone_number, email, active_status FROM `super_admins` where id = ? and active_status = 0";
}



let updateSuperAdminProfileById = (data) => {
    let keys = Object.keys(data);
    let query = "update `super_admins` set " + keys[0] + " = '" + data[keys[0]] + "'";

    for (let i = 1; i < keys.length; i++) {
        query += ", " + keys[i] + " = '" + data[keys[i]] + "'";
    }

    query += " where id = ?";
    return query;
}

// MD Sir

let getMdSirProfileById = () => {
    return "SELECT id, name, image, phone_number, email FROM `md_accounts` where id = ?";
}

let getTaskListBySearching = (searchField) => {

    const month = searchField.month;
    const year = searchField.year;

    let query = `Select task.id,  task.call_date, task.client_id,task.client_type_id, task.call_time,
    task.prospect_type_id, task.lead_id, task.status, task.sales_person_id,client_type.type as client_type, 
    sales_person.name as sales_person,leads.name as leads, task.created_at,  task.active_status
    FROM task join sales_person on (task.sales_person_id = sales_person.id)
    join client_type on (task.client_type_id = client_type.id)
    
    join leads on (task.lead_id = leads.id)`;


    query += `where MONTH(task.call_date) = ${month}  and YEAR(task.call_date) = ${year} `

    //console.log(query);

    return query;
}
let getTaskListBySearchingWithYear = (searchField) => {

    //const month =  searchField.month;
    const year = searchField.year;

    let query = `Select task.id,  task.call_date, task.client_id, task.call_time,task.client_type_id,
     task.prospect_type_id, task.lead_id, task.status, task.sales_person_id,client_type.type as client_type,
     sales_person.name as sales_person,leads.name as leads, task.created_at,  task.active_status
    FROM task join sales_person on (task.sales_person_id = sales_person.id)
    join client_type on (task.client_type_id = client_type.id)
    join leads on (task.lead_id = leads.id)`;



    query += `where  YEAR(task.call_date) = ${year} `

    //console.log(query);

    return query;
}

let getAppointmentListBySearching = (searchField) => {

    const month = searchField.month;
    const year = searchField.year;

    let query = `Select appointment.id, appointment.client_id,appointment.client_type_id,client_type.type as client_type, appointment.appointment_date, 
    appointment.car_number, appointment.driver_name, appointment.discussion_type_id, discussion_type.type as discussion_type,
    appointment.service_details, appointment.sales_person_id,appointment.status,
    sales_person.name as sales_person,   appointment.created_at,appointment.active_status,appointment.reason_for_delete
    FROM appointment join sales_person on (appointment.sales_person_id = sales_person.id)
    join client_type on (appointment.client_type_id = client_type.id)
    join discussion_type on (appointment.discussion_type_id = discussion_type.id)
     `;


    query += `where MONTH(appointment.appointment_date) = ${month}  and YEAR(appointment.appointment_date) = ${year} `

    //console.log(query);

    return query;
}
let getAppointmentListBySearchingWithYear = (searchField) => {

    //const month =  searchField.month;
    const year = searchField.year;

    let query = `Select appointment.id, appointment.client_id, appointment.client_type_id,client_type.type as client_type,appointment.appointment_date, 
    appointment.car_number, appointment.driver_name,appointment.discussion_type_id,discussion_type.type as discussion_type,
     appointment.service_details, appointment.sales_person_id,appointment.status, 
    sales_person.name as sales_person,  appointment.created_at,appointment.active_status,appointment.reason_for_delete
    FROM appointment join sales_person on (appointment.sales_person_id = sales_person.id)
    join client_type on (appointment.client_type_id = client_type.id)
    join discussion_type on (appointment.discussion_type_id = discussion_type.id)
     `;


    query += `where  YEAR(appointment.appointment_date) = ${year} `

    //console.log(query);

    return query;
}





// user
let getUserByPhone = () => {
    return "SELECT id, phone_number, department_id, password, user_type, profile_id FROM `users` where phone_number = ? ";
}

let registerUserAccount = () => {
    return "INSERT INTO `users` SET ? ";
}

let passwordChangeForUser = () => {
    return "UPDATE `users` set password = ? where id =  ? ";
}

let getUserByProfileIdAndUserType = () => {
    return "SELECT id, phone_number, department_id, password, user_type, profile_id FROM `users` where profile_id = ? and user_type = ? ";
}

let getUserByUserType = () => {
    return "SELECT id, phone_number, department_id, password, user_type, profile_id FROM `users` where user_type = ? ";
}

let updateUserById = (data) => {
    let keys = Object.keys(data);
    let query = "update `users` set " + keys[0] + " = '" + data[keys[0]] + "'";

    for (let i = 1; i < keys.length; i++) {
        query += ", " + keys[i] + " = '" + data[keys[i]] + "'";
    }

    query += " where id = ?";

    return query;
}



// Sub Admin
let registerSubAdminAccount = () => {
    return "INSERT INTO `sub_admins` SET ? ";
}

let getSubAdminList = () => {
    return "SELECT id, name, image, phone_number, email FROM `sub_admins`  where active_status = 0";
}

let getSubAdminDeactiveList = () => {
    return "SELECT id, name, image, used_phone_no, email FROM `sub_admins`  where active_status = 1";
}

let deactiveSubAccount = () => {
    return "UPDATE `sub_admins` set active_status = 1 where id = ?";
}

let getSubAdminProfileById = () => {
    return "SELECT id, name, image, phone_number, email, active_status FROM `sub_admins` where id = ? and active_status = 0";
}

let updateSubAdminProfileById = (data) => {
    let keys = Object.keys(data);
    let query = "update `sub_admins` set " + keys[0] + " = '" + data[keys[0]] + "'";

    for (let i = 1; i < keys.length; i++) {
        query += ", " + keys[i] + " = '" + data[keys[i]] + "'";
    }

    query += " where id = ?";

    return query;
}


// Sales person

let registerSalesPersonAccount = () => {
    return "INSERT INTO `sales_person` SET ? ";
}

let getSalesPersonList = () => {
    return "SELECT id, name, image, phone_number, email FROM `sales_person` where active_status = 0";
}

let getAllSalesPersonList = () => {
    return "SELECT id, name, image, phone_number, email,active_status FROM `sales_person` ";
}

let getSalesPersonDeleteList = () => {
    return "SELECT id, name, image, used_phone_no, email FROM `sales_person` where active_status = 1";
}

let deactiveSalesPersonAccount = () => {
    return "UPDATE `sales_person` set active_status = 1 where id = ?";
}

let getSalesPersonProfileById = () => {
    return "SELECT id, name, image, phone_number, email,password_recovery_questions_id, question_ans, active_status FROM `sales_person` where id = ? and active_status = 0";
}

let getSalesPersonProfileByIdAllType = () => {
    return "SELECT id, name, image, phone_number, email,password_recovery_questions_id, question_ans, active_status FROM `sales_person` where id = ?";
}



let getProfileByPhoneNumber = () => {
    return "SELECT id, name, image, phone_number, email,password_recovery_questions_id, question_ans, active_status FROM `sales_person` where phone_number = ? and active_status = 0";
}

let getSalesPersonProfileByIdForOthers = () => {
    return "SELECT id, name, image, phone_number, email FROM `sales_person` where id = ? and active_status = 0";
}

let getTaskCountBySalespersonId = () => {
    return "SELECT count(prospect_type_id) as totalTask, sum(case when prospect_type_id=0 then 1 end) as taskIncomplete, sum(case when prospect_type_id !=0 then 1 end) as taskComplete,sum(case when call_date=CURDATE() then 1 end) as taskToday FROM task where sales_person_id= ? ";
}


let updateSalesPersonProfileById = (data) => {
    let keys = Object.keys(data);
    let query = "update `sales_person` set " + keys[0] + " = '" + data[keys[0]] + "'";

    for (let i = 1; i < keys.length; i++) {
        query += ", " + keys[i] + " = '" + data[keys[i]] + "'";
    }

    query += " where id = ?";

    return query;
}

let getPasswordRecoveryQuestionId = () => {
    return "SELECT id FROM `password_recovery_questions` where id = ? ";
}


// Password Rcovery Question

let getPasswordRecoveryList = () => {
    return "SELECT id, question_details  FROM `password_recovery_questions`";
}

// Client Type

let getClientTypeList = () => {
    return "SELECT id, type  FROM `client_type` where active_status = 0";
}

let getClientTypeAllList = () => {
    return "SELECT id, type,active_status  FROM `client_type`";
}

let addClientType = () => {
    return "INSERT INTO `client_type` SET ?";
}

let getClientTypeByTypeName = () => {
    return "SELECT * FROM `client_type` where type = ?";
}

let deactiveClientType = () => {
    return "UPDATE `client_type` set active_status = 1 where id = ?";
}

let activeClientType = () => {
    return "UPDATE `client_type` set active_status = 0 where id = ?";
}


let updateupdateClientTypeById = () => {
    return "UPDATE `client_type` set type = ? where id = ?";
}

let getClientTypeByID = () => {
    return "SELECT * FROM `client_type` where id = ? "; // and active_status = 0
}

let getDeactiveClientTypeList = () => {
    return "SELECT id, type  FROM `client_type` where active_status = 1";
}

let getActiveClientTypeList = () => {
    return "SELECT id, type  FROM `client_type` where active_status = 0";
}


// Discussion Type

let getDiscussionTypeList = () => {
    return "SELECT id, type  FROM `discussion_type` where active_status = 0";
}

let getDiscussionTypeAllList = () => {
    return "SELECT id, type,service_time_period,reminder_time_period,active_status  FROM `discussion_type`";
}

let addDiscussionType = () => {
    return "INSERT INTO `discussion_type` SET ?";
}

let getDiscussionTypeByTypeName = () => {
    return "SELECT * FROM `discussion_type` where type = ?";
}

let deactiveDiscussionType = () => {
    return "UPDATE `discussion_type` set active_status = 1 where id = ?";
}

let activeDiscussionType = () => {
    return "UPDATE `discussion_type` set active_status = 0 where id = ?";
}


let updateDiscussionTypeById = (updateData) => {
    //return "UPDATE `discussion_type` set type = ? where id = ?";
    let keys = Object.keys(updateData);
    let query = "update `discussion_type` set " + keys[0] + " = '" + updateData[keys[0]] + "'";

    for (let i = 1; i < keys.length; i++) {
        query += ", " + keys[i] + " = '" + updateData[keys[i]] + "'";
    }

    query += " where id = ?";

    return query;
}

let getDiscussionTypeByID = () => {
    return "SELECT * FROM `discussion_type` where id = ?";
}

let getDeactiveDiscussionTypeList = () => {
    return "SELECT id, type,service_time_period,reminder_time_period  FROM `discussion_type` where active_status = 1";
}
let getActiveDiscussionTypeList = () => {
    return "SELECT id, type,service_time_period,reminder_time_period  FROM `discussion_type` where active_status = 0";
}


//  VehicleType

let getVehicleTypeList = () => {
    return "SELECT id, type  FROM `vehicle_type` where active_status = 0";
}



let getVehicleTypeAllList = () => {
    return "SELECT id, type,active_status  FROM `vehicle_type`";
}

let addVehicleType = () => {
    return "INSERT INTO `vehicle_type` SET ?";
}

let getVehicleTypeByTypeName = () => {
    return "SELECT * FROM `vehicle_type` where type = ?";
}

let deactiveVehicleType = () => {
    return "UPDATE `vehicle_type` set active_status = 1 where id = ?";
}



let activeVehicleType = () => {
    return "UPDATE `vehicle_type` set active_status = 0 where id = ?";
}


let updateVehicleTypeById = () => {
    return "UPDATE `vehicle_type` set type = ? where id = ?";
}

let getVehicleTypeByID = () => {
    return "SELECT * FROM `vehicle_type` where id = ?";
}

let getDeactiveVehicleTypeList = () => {
    return "SELECT id, type  FROM `vehicle_type` where active_status = 1";
}
let getActiveVehicleTypeList = () => {
    return "SELECT id, type  FROM `vehicle_type` where active_status = 0";
}

//  call_time_type

let getCallTimeList = () => {
    return "SELECT id, type  FROM `call_time_type` where active_status = 0";
}

let addCallTime = () => {
    return "INSERT INTO `call_time_type` SET ?";
}

let getCallTimeByTypeName = () => {
    return "SELECT * FROM `call_time_type` where type = ?";
}

let deactiveCallTime = () => {
    return "UPDATE `call_time_type` set active_status = 1 where id = ?";
}

let updateCallTimeById = () => {
    return "UPDATE `call_time_type` set type = ? where id = ?";
}


let getCallTimeByID = () => {
    return "SELECT * FROM `call_time_type` where id = ?";
}

// prospect_type

let getProspectTypeList = () => {
    return "SELECT id, type  FROM `prospect_type` where active_status = 0";
}

let getProspectTypeAllList = () => {
    return "SELECT id, type,active_status  FROM `prospect_type`";
}

let addProspectType = () => {
    return "INSERT INTO `prospect_type` SET ?";
}

let getProspectTypeByTypeName = () => {
    return "SELECT * FROM `prospect_type` where type = ?";
}

let deactiveProspectType = () => {
    return "UPDATE `prospect_type` set active_status = 1 where id = ?";
}

let activeProspectType = () => {
    return "UPDATE `prospect_type` set active_status = 0 where id = ?";
}

let updateProspectTypeById = () => {
    return "UPDATE `prospect_type` set type = ? where id = ?";
}

let getProspectTypeByID = () => {
    return "SELECT * FROM `prospect_type` where id = ?";
}

let getProspectNameByID = () => {
    return "SELECT type FROM `prospect_type` where id = ?";
}

let getDeactiveProspectTypeList = () => {
    return "SELECT id, type  FROM `prospect_type` where active_status = 1";
}
let getActiveProspectTypeList = () => {
    return "SELECT id, type  FROM `prospect_type` where active_status = 0";
}

let getProspectTypeWiseCount = (prospectTypeList, isAddSalesPerson = 0) => {
    let query = "SELECT ";
    const prospectTypeListLength = prospectTypeList.length;
    query += `sum(CASE WHEN prospect_type_id = 0 THEN 1 ELSE 0 END) as total_default`
    for (let i = 0; i < prospectTypeListLength; i++) {
        query += `, sum(CASE WHEN prospect_type_id = ${prospectTypeList[i].id} THEN 1 ELSE 0 END) as total_${prospectTypeList[i].type}`
    }
    query += " FROM `task`";

    if (isAddSalesPerson === 1) {
        query += " where sales_person_id = ? ";
    }
    //console.log(query);
    return query;
}




// leads

let getLeadList = () => {
    return "SELECT id, name  FROM `leads` ";
}

let getLeadAllList = () => {
    return "SELECT id, name,active_status  FROM `leads`";
}


let getDeactiveList = () => {
    return "SELECT id, name  FROM `leads` where active_status = 1";
}
let getActiveList = () => {
    return "SELECT id, name  FROM `leads` where active_status = 0";
}

let addLead = () => {
    return "INSERT INTO `leads` SET ?";
}

let getLeadByName = () => {
    return "SELECT * FROM `leads` where name = ?";
}

let deactiveLead = () => {
    return "UPDATE `leads` set active_status = 1 where id = ?";
}

let activeLead = () => {
    return "UPDATE `leads` set active_status = 0 where id = ?";
}

let updateLeadById = () => {
    return "UPDATE `leads` set name = ? where id = ?";
}

let getLeadByID = () => {
    return "SELECT * FROM `leads` where id = ?";
}

/// Companies

let getAllCompanyList = () => {
    return "SELECT id,title,active_status  FROM `companies` ";
}

let getDeactiveCompanyList = () => {
    return "SELECT id,title  FROM `companies` where active_status = 1";
}
let getActiveCompanyList = () => {
    return "SELECT id,title  FROM `companies` where active_status = 0";
}

let addCompany = () => {
    return "INSERT INTO `companies` SET ?";
}

let getCompanyByTitle = () => {
    return "SELECT * FROM `companies` where title = ?";
}

let updateCompanyByID = () => {
    return "UPDATE `companies` set title = ?,user_id =? where id = ?";
}

let getCompanyByID = () => {
    return "SELECT * FROM `companies` where id = ?";
}


let deactiveCompany = () => {
    return "UPDATE `companies` set active_status = 1 where id = ?";
}

let activeCompany = () => {
    return "UPDATE `companies` set active_status = 0 where id = ?";
}

let searchCompany = (title) => {
    return `SELECT id,title,active_status FROM companies where title LIKE '%${title}%'`;
}

let searchCompanyBySalesPerson = (sales_person_id, title) => {
    return `SELECT DISTINCT
    access_corporate_clients.sales_person_id,
    access_corporate_clients.company_id,
    companies.company_title  as title
FROM
    access_corporate_clients
JOIN(
    SELECT companies.id,
        companies.title AS company_title,
        companies.active_status
    FROM
        companies
    WHERE
        companies.active_status = 0
) companies
ON
    companies.id = access_corporate_clients.company_id
WHERE
    access_corporate_clients.sales_person_id = ${sales_person_id} AND access_corporate_clients.active_status = 0
     and companies.company_title LIKE '%${title}%'
    
    ORDER By access_corporate_clients.company_id ASC`;
}

///   Sister Concern

let getAllSisterConcernList = () => {
    return `SELECT company_sister_concerns.id, company_sister_concerns.title as sister_concern,companies.title AS company,company_sister_concerns.active_status
    FROM company_sister_concerns
    JOIN companies ON companies.id = company_sister_concerns.company_id    
    ORDER BY company_sister_concerns.id`;
}



let getActiveSisterConcernList = () => {
    return `SELECT company_sister_concerns.id, company_sister_concerns.title,companies.title AS company_title
    FROM company_sister_concerns
    JOIN companies ON companies.id = company_sister_concerns.company_id
    WHERE company_sister_concerns.active_status =0
    ORDER BY company_sister_concerns.id`;
}


let getDeactiveSisterConcernList = () => {
    return `SELECT company_sister_concerns.id, company_sister_concerns.title,companies.title AS company_title
    FROM company_sister_concerns
    JOIN companies ON companies.id = company_sister_concerns.company_id
    WHERE company_sister_concerns.active_status =1
    ORDER BY company_sister_concerns.id`;
}

let addSisterConcern = () => {
    return "INSERT INTO `company_sister_concerns` SET ?";
}

let getSisterConcernByID = () => {
    return "SELECT * FROM `company_sister_concerns` where id = ?";
}

let getSisterConcernAndCompanyByID = () => {
    return `SELECT company_sister_concerns.id,company_sister_concerns.title,companies.title AS company_title,company_sister_concerns.active_status
    FROM company_sister_concerns
    JOIN companies ON companies.id = company_sister_concerns.company_id
    WHERE company_sister_concerns.id = ?`;
}


let updateCompanySisterConcernByID = (data) => {
    let keys = Object.keys(data);
    let query = "update `company_sister_concerns` set " + keys[0] + " = '" + data[keys[0]] + "'";

    for (let i = 1; i < keys.length; i++) {
        query += ", " + keys[i] + " = '" + data[keys[i]] + "'";
    }

    query += " where id = ?";

    return query;
}

let getSisterConcernByTitleAndID = () => {
    return "SELECT * FROM `company_sister_concerns` where company_id = ? and title = ?";
}

let getSisterConcernByCompanyID = (company_id) => { /// For Admin View
    return `SELECT
    company_sister_concerns.id,
    company_sister_concerns.title,
    company_sister_concerns.company_id,
    companies.title AS company_title,
    access_corporate_clients.sales_person_id AS salesperson_id,
    access_corporate_clients.sales_person_name,
    access_corporate_clients.contact_number
FROM
    company_sister_concerns
JOIN companies ON(
        company_sister_concerns.company_id = companies.id
    )
LEFT JOIN ( SELECT access_corporate_clients.id, sales_person_id,sales_person.name as sales_person_name ,
    sales_person.phone_number as contact_number, access_corporate_clients.company_sister_concern_id, access_corporate_clients.active_status FROM access_corporate_clients

JOIN sales_person ON sales_person.id=access_corporate_clients.sales_person_id
where access_corporate_clients.active_status = 0 and access_corporate_clients.company_id =${company_id}) access_corporate_clients
ON (
        company_sister_concerns.id = access_corporate_clients.company_sister_concern_id
  ) WHERE
    company_sister_concerns.active_status = 0 AND company_sister_concerns.company_id =${company_id}`;
}

let getSisterConcernByCompanyIDAndSalesPersonID = (sales_person_id, company_id) => { // For Sales Person
    return `SELECT
    access_corporate_clients.id,
    access_corporate_clients.sales_person_id,
    access_corporate_clients.company_id,
    access_corporate_clients.company_sister_concern_id,
    company_sister_concerns.sister_concern_title
    
FROM
    access_corporate_clients
JOIN sales_person ON sales_person.id = access_corporate_clients.sales_person_id
JOIN(
    SELECT
        companies.id,
        companies.title AS company_title,
        companies.active_status
    FROM
        companies
    WHERE
        companies.active_status = 0
) companies
ON
    companies.id = access_corporate_clients.company_id
JOIN(
    SELECT
        company_sister_concerns.id,
        company_sister_concerns.title AS sister_concern_title,
        company_sister_concerns.active_status
    FROM
        company_sister_concerns
    WHERE
        company_sister_concerns.active_status = 0
) company_sister_concerns
ON
    company_sister_concerns.id = access_corporate_clients.company_sister_concern_id
WHERE
    access_corporate_clients.sales_person_id = ${sales_person_id} AND access_corporate_clients.company_id = ${company_id}
    and access_corporate_clients.active_status=0 `;
}



let getSisterConcernListBySearchingWithCompanyID = (company_id, sister_concern_title) => {

    return `SELECT
    company_sister_concerns.id,
    company_sister_concerns.title,
    company_sister_concerns.company_id,
    companies.title AS company_title,
    access_corporate_clients.sales_person_id AS salesperson_id,
    access_corporate_clients.sales_person_name,
    access_corporate_clients.contact_number
FROM
    company_sister_concerns
JOIN companies ON
    (
        company_sister_concerns.company_id = companies.id
    )
LEFT JOIN(
    SELECT access_corporate_clients.id,
        sales_person_id,
        sales_person.name AS sales_person_name,
        sales_person.phone_number AS contact_number,
        access_corporate_clients.company_sister_concern_id,
        access_corporate_clients.active_status
    FROM
        access_corporate_clients
    JOIN sales_person ON sales_person.id = access_corporate_clients.sales_person_id
    WHERE
        access_corporate_clients.active_status = 0 AND access_corporate_clients.company_id = ${company_id}
) access_corporate_clients
ON
    (
        company_sister_concerns.id = access_corporate_clients.company_sister_concern_id
    )
WHERE
    company_sister_concerns.active_status = 0 AND company_sister_concerns.company_id =${company_id} AND company_sister_concerns.title LIKE  '%${sister_concern_title}%' `;


}


let getSisterConcernListBySearchingWithCompanyIDAndSalesPersonID = (sales_person_id, company_id, sister_concern_title) => {
    return `SELECT
    access_corporate_clients.id,
    access_corporate_clients.company_id,
    access_corporate_clients.sales_person_id,
    sales_person.name AS sales_person,
    access_corporate_clients.company_sister_concern_id,
    company_sister_concerns.sister_concern_title
FROM
    access_corporate_clients
JOIN sales_person ON sales_person.id = access_corporate_clients.sales_person_id
JOIN(
    SELECT
        companies.id,
        companies.title AS company_title,
        companies.active_status
    FROM
        companies
    WHERE
        companies.active_status = 0
) companies
ON
    companies.id = access_corporate_clients.company_id
JOIN(
    SELECT
        company_sister_concerns.id,
        company_sister_concerns.title AS sister_concern_title,
        company_sister_concerns.active_status
    FROM
        company_sister_concerns
    WHERE
        company_sister_concerns.active_status = 0
) company_sister_concerns
ON
    company_sister_concerns.id = access_corporate_clients.company_sister_concern_id
WHERE
    access_corporate_clients.sales_person_id = ${sales_person_id} AND access_corporate_clients.company_id = ${company_id} AND
     access_corporate_clients.active_status = 0 AND company_sister_concerns.sister_concern_title LIKE '%${sister_concern_title}%'`;

}


let deactiveSisterConcern = () => {
    return "UPDATE `company_sister_concerns` set active_status = 1 where id = ?";
}

let activeSisterConcern = () => {
    return "UPDATE `company_sister_concerns` set active_status = 0 where id = ?";
}


let getSisterConcernListBySearching = (searchField) => {
    const company_title = searchField.company_title;
    const sister_concern_title = searchField.sister_concern_title;
    return `SELECT company_sister_concerns.id , company_sister_concerns.active_status,company_sister_concerns.title as sister_concern,companies.title  as company from company_sister_concerns
    
    JOIN companies ON companies.id = company_sister_concerns.company_id
    WHERE company_sister_concerns.title  LIKE '%${sister_concern_title}%' and companies.title LIKE '%${company_title}%' `;
}


let getSisterConcernListBySearchingWithSisterConcern = (searchField) => {

    const sister_concern_title = searchField.sister_concern_title;
    return `SELECT company_sister_concerns.id ,company_sister_concerns.active_status, company_sister_concerns.title as sister_concern,companies.title  as company from company_sister_concerns
    JOIN companies ON companies.id = company_sister_concerns.company_id
    WHERE company_sister_concerns.title  LIKE '%${sister_concern_title}%'  `;
}


let getSisterConcernListBySearchingWithCompany = (searchField) => {

    const company_title = searchField.company_title;
    return `SELECT company_sister_concerns.id , company_sister_concerns.active_status,company_sister_concerns.title as sister_concern,companies.title  as company from company_sister_concerns
    JOIN companies ON companies.id = company_sister_concerns.company_id
    WHERE companies.title   LIKE '%${company_title}%'  `;
}

let getSisterConcernListBySalespersonId = () => {

    return `SELECT company_sister_concerns.id,company_sister_concerns.title AS company_sister_concerns_title,companies.title AS company_title
    FROM company_sister_concerns JOIN companies ON companies.id = company_sister_concerns.company_id 
    where company_sister_concerns.id IN 
    (SELECT company_sister_concern_id FROM access_corporate_clients WHERE sales_person_id = ? and active_status = 0) and company_sister_concerns.active_status = 0`;
}


// Sister Concern Contact Person 

let addNewContactPerson = () => {
    return "INSERT INTO `company_sister_concern_contact_persons` SET ?";
}

let updateContactPersonByID = (data) => {
    let keys = Object.keys(data);
    let query = "update `company_sister_concern_contact_persons` set " + keys[0] + " = '" + data[keys[0]] + "'";

    for (let i = 1; i < keys.length; i++) {
        query += ", " + keys[i] + " = '" + data[keys[i]] + "'";
    }

    query += " where id = ?";

    return query;
}

let getContactList = (company_sister_concern_id) => {
    return `SELECT * FROM company_sister_concern_contact_persons WHERE company_sister_concern_id= ${company_sister_concern_id}`;
}

let getContactDetailsByID = () => {
    return "SELECT * FROM `company_sister_concern_contact_persons` where id = ?";
}

let deleteContactPerson = () => {
    return `DELETE FROM company_sister_concern_contact_persons WHERE id= ?`;
}




/// Access Corporate Client

let addAccessCorporateClient = () => {
    return "INSERT INTO `access_corporate_clients` SET ?";
}

let getAccessCorporateClientDataByCompanySisterConcerId = () => {
    return "SELECT * FROM `access_corporate_clients` where company_sister_concern_id = ? and active_status = 0 order by id desc"
}

let getAccessCorporateClientAllAccessHistoryByCompanySisterConcerId = () => {
    return "SELECT *  FROM `access_corporate_clients` join (select id as sales_person_id2, name as sales_person_name from sales_person) sales_person ON access_corporate_clients.sales_person_id = sales_person.sales_person_id2 where company_sister_concern_id = ? order by access_corporate_clients.id desc"
}


let getAccessCorporateClientDataBycompany_sister_concern_idAndsales_person_id = () => {
    return "SELECT * FROM `access_corporate_clients` where company_sister_concern_id = ? and sales_person_id = ? and active_status = 0 order by id desc"
}

let getAccessCorporateClientDataBysales_person_id = () => {
    return "SELECT * FROM `access_corporate_clients` where sales_person_id = ? and active_status = 0 order by id desc"
}




let updateAccessCorporateClientById = (data) => {
    let keys = Object.keys(data);
    let query = "update `access_corporate_clients` set " + keys[0] + " = '" + data[keys[0]] + "'";

    for (let i = 1; i < keys.length; i++) {
        query += ", " + keys[i] + " = '" + data[keys[i]] + "'";
    }

    query += " where id = ?";

    return query;
}

let getCompanyListBySalespersonID = (salesPersonId) => {

    return `SELECT DISTINCT
    access_corporate_clients.sales_person_id,
    access_corporate_clients.company_id,
    companies.company_title as title
FROM
    access_corporate_clients
JOIN(
    SELECT companies.id,
        companies.title AS company_title,
        companies.active_status
    FROM
        companies
    WHERE
        companies.active_status = 0
) companies
ON
    companies.id = access_corporate_clients.company_id
WHERE
    access_corporate_clients.sales_person_id = ${salesPersonId} AND access_corporate_clients.active_status = 0
    
    ORDER By access_corporate_clients.company_id ASC`;
}

let updateAccessCorporateClientBySales_person_idAndCompany_sister_concern_id = (data) => {
    let keys = Object.keys(data);
    let query = "update `access_corporate_clients` set " + keys[0] + " = '" + data[keys[0]] + "'";

    for (let i = 1; i < keys.length; i++) {
        query += ", " + keys[i] + " = '" + data[keys[i]] + "'";
    }

    query += " where sales_person_id = ? and company_sister_concern_id = ? and active_status = 0";

    return query;
}

let getAccessCompany_SisterConcern_By_SalesPersonID = (sales_person_id) => {

    return `SELECT
    access_corporate_clients.id,
    access_corporate_clients.company_id,
    access_corporate_clients.sales_person_id,
    sales_person.name AS sales_person,
    access_corporate_clients.company_sister_concern_id,
    companies.company_title,
    company_sister_concerns.sister_concern_title
FROM
    access_corporate_clients
JOIN sales_person ON sales_person.id = access_corporate_clients.sales_person_id
JOIN(
    SELECT
        companies.id,
        companies.title AS company_title,
        companies.active_status
    FROM
        companies
    WHERE
        companies.active_status = 0
) companies
ON
    companies.id = access_corporate_clients.company_id
JOIN(
    SELECT
        company_sister_concerns.id,
        company_sister_concerns.title AS sister_concern_title,
        company_sister_concerns.active_status
    FROM
        company_sister_concerns
    WHERE
        company_sister_concerns.active_status = 0
) company_sister_concerns
ON
    company_sister_concerns.id = access_corporate_clients.company_sister_concern_id
WHERE
    access_corporate_clients.sales_person_id = ${sales_person_id}  AND
     access_corporate_clients.active_status = 0 `;

}


// department Type

let getDepartmentList = () => {
    return "SELECT id, name,image  FROM `departments` where active_status = 0";
}

// Client

let getClientList = () => {
    return `SELECT clients.id, clients.name, clients.primary_phone_no, clients.secondary_phone_no, clients.email,
    clients.address, clients.client_type_id,client_type.type as clientType
     FROM clients join client_type on (clients.client_type_id=client_type.id)
      where clients.active_status = 0`;
}

let getClientListForSalesPerson = () => {

    return `SELECT clients.id, clients.name, clients.primary_phone_no, clients.secondary_phone_no, clients.email,
    clients.address, clients.client_type_id,client_type.type as clientType
     FROM clients join client_type on (clients.client_type_id=client_type.id)
      where clients.active_status = 0 and clients.id IN (SELECT client_id FROM access_client where sales_person_id = ?)`;

    // return "SELECT id, name, primary_phone_no, secondary_phone_no, email, address, client_type_id FROM `clients` where active_status = 0 and id IN (SELECT client_id FROM `access_client` where sales_person_id = ?)";
}

let getClientListForSalesPersonBySearching = (salesPersonId, client_name) => {

    return `SELECT clients.id, clients.name, clients.primary_phone_no, clients.secondary_phone_no, clients.email,
    clients.address, clients.client_type_id,client_type.type as clientType
     FROM clients join client_type on (clients.client_type_id=client_type.id)
      where clients.active_status = 0 and clients.id IN (SELECT client_id FROM access_client where sales_person_id = ${salesPersonId})
      and clients.name LIKE '%${client_name}%'`;


}


let getClientListBySearching = (client_name) => {
    return `SELECT clients.id, clients.name, clients.primary_phone_no, clients.secondary_phone_no, clients.email,
    clients.address, clients.client_type_id,client_type.type as clientType
     FROM clients join client_type on (clients.client_type_id=client_type.id)
      where clients.active_status = 0 and clients.name like '%${client_name}%'`;
}


let addClient = () => {
    return "INSERT INTO `clients` SET ?";
}

let getClientByPhone = () => {
    return "SELECT * FROM `clients` where primary_phone_no = ? or secondary_phone_no = ?";
}

let deactiveClient = () => {
    return "UPDATE `clients` set active_status = 1 where id = ?";
}

let activeClient = () => {
    return "UPDATE `clients` set active_status = 0 where id = ?";
}


let updateClientProfileByID = (data) => {
    let keys = Object.keys(data);
    let query = "update `clients` set " + keys[0] + " = '" + data[keys[0]] + "'";

    for (let i = 1; i < keys.length; i++) {
        query += ", " + keys[i] + " = '" + data[keys[i]] + "'";
    }

    query += " where id = ?";

    return query;
}

let getClientByID = () => {
    return "SELECT * FROM `clients` where id = ?";
}

// access client
let addAccessClient = () => {
    return "INSERT INTO `access_client` SET ?";
}

let getAccessClientDataBysales_person_id = () => {
    return "SELECT * FROM `access_client` where sales_person_id = ? and active_status = 0 order by id desc"
}

let getAccessClientHistoryByClient_id = () => {
    return `SELECT *  FROM access_client join (select id as sales_person_id2, name as sales_person_name from sales_person) 
    sales_person ON access_client.sales_person_id = sales_person.sales_person_id2 where client_id = ? order by access_client.id desc`;
}



let getAccessClientDataByclient_idAndsales_person_id = () => {
    return "SELECT * FROM `access_client` where client_id = ? and sales_person_id = ? and active_status = 0 order by id desc"
}


let deactiveAccessClient = () => {
    return "UPDATE `access_client` set active_status = 1 where id = ?";
}


let updateAccessClientBySales_person_idAndClient_id = (data) => {
    let keys = Object.keys(data);
    let query = "update `access_client` set " + keys[0] + " = '" + data[keys[0]] + "'";

    for (let i = 1; i < keys.length; i++) {
        query += ", " + keys[i] + " = '" + data[keys[i]] + "'";
    }

    query += " where sales_person_id = ? and client_id = ? and active_status = 0";

    return query;
}

let getAccessClientInformationBySalesPersonID = (sales_person_id) => {
    return `SELECT access_client.id,access_client.client_id,clients.client_name,access_client.sales_person_id,sales_person.name as sales_person FROM access_client
    JOIN sales_person ON sales_person.id = access_client.sales_person_id
    JOIN (
        SELECT
            clients.id,
            clients.name AS client_name,
            clients.active_status
        FROM
            clients
        WHERE
            clients.active_status = 0
    ) clients
    ON
    clients.id = access_client.client_id 
    where access_client.sales_person_id = ${sales_person_id} and  access_client.active_status = 0  `;
   
}

// Task

let getTaskByClientId = () => {
    return `Select task.id, task.client_id, task.client_type_id, task.call_date, task.status,task.sales_person_id,
        sales_person.name as sales_person, task.call_time, leads.name as leads, task.created_at,  task.active_status,task.prospect_type_id
        FROM task join sales_person on (task.sales_person_id = sales_person.id)
        join leads on (task.lead_id = leads.id) where task.client_id = ?`;
}

let getTaskByClientIdAndSalesPersonId = () => {
    return `Select task.id,  task.call_date, task.status,task.sales_person_id, sales_person.name as sales_person, task.call_time,
        leads.name as leads, task.created_at,  task.active_status,task.prospect_type_id
        FROM task join sales_person on (task.sales_person_id = sales_person.id)
        join leads on (task.lead_id = leads.id) where task.client_id = ? and task.sales_person_id = ?`;
}

let addTask = () => {
    return "INSERT INTO `task` SET ?";
}



let updateTaskByID = (data) => {
    let keys = Object.keys(data);
    let query = "update `task` set " + keys[0] + " = '" + data[keys[0]] + "'";

    for (let i = 1; i < keys.length; i++) {
        query += ", " + keys[i] + " = '" + data[keys[i]] + "'";
    }

    query += " where id = ?";

    return query;
}

let getTaskByID = () => {
    return `Select task.id, task.sales_person_id, task.call_date, task.sales_person_id, task.status, task.client_type_id,
   task.client_id, task.call_time, task.prospect_type_id, task.lead_id,
    
   sales_person.name as sales_person,leads.name as leads, task.created_at,  task.active_status
   FROM task 
   join sales_person on (task.sales_person_id = sales_person.id)
   join leads on (task.lead_id = leads.id) where task.id = ?`;
}

let getTaskByIDAndSalespersonId = () => {
    return `Select task.id,  task.call_date, task.client_type_id, task.client_id,  task.call_time,
    task.prospect_type_id, task.lead_id, task.status, task.sales_person_id,
    
    sales_person.name as sales_person,leads.name as leads, task.created_at,  task.active_status
    FROM task 
    join sales_person on (task.sales_person_id = sales_person.id)
    join leads on (task.lead_id = leads.id) where task.id = ? and task.sales_person_id = ?`;
}


let searchTask = (sercheingFieldObject = {}, extraWhere = "") => {
    let discussion_type, vehicle_type, query;

    if (sercheingFieldObject.hasOwnProperty("discussion_type_id")) {
        discussion_type = sercheingFieldObject.discussion_type_id;
        delete sercheingFieldObject.discussion_type_id;
    }

    if (sercheingFieldObject.hasOwnProperty("vehicle_type_id")) {
        vehicle_type = sercheingFieldObject.vehicle_type_id;
        delete sercheingFieldObject.vehicle_type_id;
    }


    let keys = Object.keys(sercheingFieldObject);

    query = `Select task.id, task.client_type_id, task.client_id, task.sales_person_id, task.call_date, task.status, task.sales_person_id,
        task.call_time, task.prospect_type_id, task.lead_id,  sales_person.name as sales_person,
        leads.name as leads, task.created_at,  task.active_status
        FROM task join sales_person on (task.sales_person_id = sales_person.id)
        
        join leads on (task.lead_id = leads.id) `;


    let where = "";

    if (keys.length > 0) {
        where = " where  " + keys[0] + " = " + sercheingFieldObject[keys[0]] + " ";
        let keysLength = keys.length;

        for (let i = 1; i < keysLength; i++) {
            where += "and " + keys[i] + " = " + sercheingFieldObject[keys[i]] + " ";
        }
    }

    // Generate Query for Dicussion type & Vehical type
    if (discussion_type === undefined && vehicle_type === undefined) { } else if (discussion_type === undefined) {
        if (where.length < 1) where = " where ";
        else where += " and ";

        where += " task.id  IN (SELECT task_id FROM task_vehicle_types WHERE vehicle_type_id IN (" + vehicle_type.toString() + "))";
    } else if (vehicle_type === undefined) {

        if (where.length < 1) where = " where ";
        else where += " and ";

        where += "task.id  IN (SELECT task_id FROM task_discussion_types where discussion_type_id IN (" + discussion_type.toString() + "))";

    } else {
        if (where.length < 1) where = " where ";
        else where += " and ";

        where += `task.id In (SELECT task_id FROM task_discussion_types WHERE task_id IN (SELECT task_id FROM task_vehicle_types where vehicle_type_id IN ( ${vehicle_type.toString()})) and discussion_type_id IN (${discussion_type.toString()}))`;
    }

    if (extraWhere.length > 0) {
        if (where.length < 1) where = " where ";
        else where += " and ";
    }

    query += where + extraWhere + " order by task.call_date ASC";
    return query;
}


let reassignTask = (isAlltaskAssign, taskList) => {
    if (isAlltaskAssign == true) {
        return "UPDATE `task` set sales_person_id = ? where sales_person_id = ? and status ='incomplete'";
    } else {
        return "UPDATE `task` set sales_person_id = ? where sales_person_id = ? and status ='incomplete' and id IN (" + taskList.toString() + ")";
    }
}
let getIncompleteTaskListById = () => {
    return `Select task.id, task.client_id, task.client_type_id,  task.call_date, task.status, sales_person.name as sales_person, task.sales_person_id as sales_person_id ,
        task.call_time as call_time, leads.name as leads,task.prospect_type_id, task.created_at,  task.active_status FROM task 
        join sales_person on (task.sales_person_id = sales_person.id)
        join leads on (task.lead_id = leads.id) where task.sales_person_id = ? and task.status = "incomplete" `;
}

let getSalesPersonWiseTaskList = (from_date, to_date) => {
    let query = "";
    if (to_date == 0) {
        query = `SELECT COUNT(task.id) as total_task, task.sales_person_id, sales_person.name FROM task INNER JOIN sales_person on sales_person.id = task.sales_person_id  where  task.call_date = ${from_date} and task.active_status = 0 GROUP by task.sales_person_id`;
    } else {
        query = `SELECT COUNT(task.id) as total_task, task.sales_person_id, sales_person.name FROM task INNER JOIN sales_person on sales_person.id = task.sales_person_id  where  task.call_date >= ${from_date} and task.call_date <= ${to_date} and task.active_status = 0 GROUP by task.sales_person_id`;
    }
    return query;
}

let getSalesPersonWiseCompleteTaskList = (from_date, to_date) => {
    let query = "";
    if (to_date == 0) {
        query = `SELECT COUNT(task.id) as total_task, task.sales_person_id, sales_person.name FROM task INNER JOIN sales_person on sales_person.id = task.sales_person_id  where  task.call_date = ${from_date} and task.active_status = 0 and status = 'complete' GROUP by task.sales_person_id`;
    } else {
        query = `SELECT COUNT(task.id) as total_task, task.sales_person_id, sales_person.name FROM task INNER JOIN sales_person on sales_person.id = task.sales_person_id  where  task.call_date >= ${from_date} and task.call_date <= ${to_date} and task.active_status = 0 and status = 'complete' GROUP by task.sales_person_id`;
    }
    return query;
}


//// appointment

/// OLD QUERY
/* let getAppointmentList = () => {
    return `SELECT appointment.id, appointment.client_id,clients.name as client,
     appointment.appointment_date, appointment.car_number, appointment.driver_name,
      appointment.service_details,appointment.status FROM appointment 
      join clients on (appointment.client_id = clients.id)
      where appointment.sales_person_id = ? and appointment.active_status = 0 `;
} */

let getAppointmentList = (sercheingFieldObject = {}, extraWhere = "") => {

    let keys = Object.keys(sercheingFieldObject);

    let query = `Select appointment.id, appointment.client_type_id,appointment.client_id, appointment.appointment_date, 
    appointment.car_number, appointment.driver_name, appointment.service_details,appointment.discussion_type_id,
    client_type.type as client_type,
    discussion_type.type as discussion_type,
    appointment.sales_person_id,appointment.status, 
    sales_person.name as sales_person
    FROM appointment join sales_person on (appointment.sales_person_id = sales_person.id)
    join client_type on (appointment.client_type_id = client_type.id)
	
    join discussion_type on (appointment.discussion_type_id = discussion_type.id) 
    
    where appointment.active_status = 0  `;

    let where = "";

    if (keys.length > 0) {
        where = " and  " + keys[0] + " = " + sercheingFieldObject[keys[0]] + " ";
        let keysLength = keys.length;

        for (let i = 1; i < keysLength; i++) {
            where += "and " + keys[i] + " = " + sercheingFieldObject[keys[i]] + " ";
        }
    }


    if (extraWhere.length > 0) {
        extraWhere = " and " + extraWhere;
    }

    query += where + extraWhere;
    // query += " Order By appointment.id ASC";


    return query;
}


let getDeactiveAppointmentList = () => {
    return `SELECT appointment.id, appointment.client_id,clients.name as client,appointment.sales_person_id,
     sales_person.name as sales_person , appointment.appointment_date, appointment.car_number, appointment.driver_name, appointment.service_details,appointment.reason_for_delete,appointment.status
     FROM appointment join  sales_person on (appointment.sales_person_id = sales_person.id)
     join clients on (appointment.client_id = clients.id)   where   appointment.active_status = 1 `;
}

let addAppointment = () => {
    return "INSERT INTO `appointment` SET ?";
}

let updateAppointmentByID = (data) => {
    let keys = Object.keys(data);
    let query = "update `appointment` set " + keys[0] + " = '" + data[keys[0]] + "'";

    for (let i = 1; i < keys.length; i++) {
        query += ", " + keys[i] + " = '" + data[keys[i]] + "'";
    }

    query += " where id = ?";

    return query;
}


let getAppointmentByID = () => {
    return "SELECT * FROM `appointment` where id = ?";
}


let deactiveAppointment = (data) => {
    let keys = Object.keys(data);
    let query = "update `appointment` set " + keys[0] + " = '" + data[keys[0]] + "'";


    query += " , active_status = 1 ";
    query += " where id = ?";

    return query;
}




let searchAppointmentForActiveList = (sercheingFieldObject = {}, extraWhere = "") => {

    let keys = Object.keys(sercheingFieldObject);

    let query = `Select appointment.id, appointment.client_type_id,appointment.client_id, appointment.appointment_date, 
    appointment.car_number, appointment.driver_name, appointment.service_details,appointment.discussion_type_id,
    discussion_type.type as discussion_type,
    appointment.sales_person_id,appointment.status, 
    sales_person.name as sales_person
    FROM appointment join sales_person on (appointment.sales_person_id = sales_person.id)
	
    join discussion_type on (appointment.discussion_type_id = discussion_type.id) 
    where appointment.active_status = 0  `;

    let where = "";

    if (keys.length > 0) {
        where = " and  " + keys[0] + " = " + sercheingFieldObject[keys[0]] + " ";
        let keysLength = keys.length;

        for (let i = 1; i < keysLength; i++) {
            where += "and " + keys[i] + " = " + sercheingFieldObject[keys[i]] + " ";
        }
    }

    if (extraWhere.length > 0) {
        extraWhere = " and " + extraWhere;
    }

    query += where + extraWhere;
    // query += " Order By appointment.id ASC";

    return query;
}


let searchAppointmentForDeactiveList = (sercheingFieldObject = {}, extraWhere = "") => {

    let keys = Object.keys(sercheingFieldObject);

    let query = `Select appointment.id, appointment.client_type_id,appointment.client_id, appointment.appointment_date, 
    appointment.car_number, appointment.driver_name, appointment.service_details,appointment.discussion_type_id,appointment.reason_for_delete,
    clients.name as client_name,discussion_type.type as discussion_type,
    appointment.sales_person_id,appointment.status, 
    sales_person.name as sales_person
    FROM appointment join sales_person on (appointment.sales_person_id = sales_person.id)
	join (SELECT id, name, primary_phone_no, secondary_phone_no, email, address  FROM clients) clients on (appointment.client_id = clients.id)
    join discussion_type on (appointment.discussion_type_id = discussion_type.id) 
    where appointment.active_status = 1 `;

    let where = "";

    if (keys.length > 0) {
        where = " and  " + keys[0] + " = " + sercheingFieldObject[keys[0]] + " ";
        let keysLength = keys.length;

        for (let i = 1; i < keysLength; i++) {
            where += "and " + keys[i] + " = " + sercheingFieldObject[keys[i]] + " ";
        }
    }

    if (extraWhere.length > 0) {
        extraWhere = " and " + extraWhere;
    }

    query += where + extraWhere;

    return query;
}

let getSalesPersonWiseAppointmentList = (from_date, to_date) => {
    let query = "";
    if (to_date == 0) {
        query = `SELECT COUNT(appointment.id) as total_appointment, appointment.sales_person_id, sales_person.name FROM appointment INNER JOIN sales_person on sales_person.id = appointment.sales_person_id  where  appointment.appointment_date = ${from_date} and appointment.active_status = 0 GROUP by appointment.sales_person_id`;
    } else {
        query = `SELECT COUNT(appointment.id) as total_appointment, appointment.sales_person_id, sales_person.name FROM appointment INNER JOIN sales_person on sales_person.id = appointment.sales_person_id  where  appointment.appointment_date >= ${from_date} and appointment.appointment_date <= ${to_date} and appointment.active_status = 0 GROUP by appointment.sales_person_id`;
    }
    // console.log(query);
    return query;
}

let getSalesPersonWiseCompleteAppointmentList = (from_date, to_date) => {
    let query = "";
    if (to_date == 0) {
        query = `SELECT COUNT(appointment.id) as total_appointment, appointment.sales_person_id, sales_person.name FROM appointment INNER JOIN sales_person on sales_person.id = appointment.sales_person_id  where  appointment.appointment_date = ${from_date} and appointment.active_status = 0 and status = 'complete' GROUP by appointment.sales_person_id`;
    } else {
        query = `SELECT COUNT(appointment.id) as total_appointment, appointment.sales_person_id, sales_person.name FROM appointment INNER JOIN sales_person on sales_person.id = appointment.sales_person_id  where  appointment.appointment_date >= ${from_date} and appointment.appointment_date <= ${to_date} and appointment.active_status = 0 and status = 'complete' GROUP by appointment.sales_person_id`;
    }
    // console.log(query);
    return query;
}


let getDetailsForNonCorporateByIDandType = (id, client_type_id) => {

    return `SELECT
	appointment.id AS id,
	appointment.client_type_id,
    client_type.type AS ClientType,
    appointment.client_id,
	clients.name AS ClientName,
	appointment.appointment_date,
	appointment.car_number,
	discussion_type.type AS DiscussionType,
	appointment.service_details,
	appointment.active_status,
	appointment.reason_for_delete,
	appointment.status
FROM
	appointment
	JOIN client_type ON ( appointment.client_type_id = client_type.id )
	JOIN clients ON ( appointment.client_id = clients.id )
	JOIN discussion_type ON ( appointment.discussion_type_id = discussion_type.id ) 
WHERE
	appointment.id = ${id} 
	AND appointment.client_type_id = ${client_type_id} `;

}


let getDetailsForCorporateByIDandType = (id, client_type_id) => {

    return `SELECT
	appointment.id AS id,
	appointment.client_type_id,
    client_type.type AS ClientType,
    appointment.client_id,
	company_sister_concerns.id as ClientID,
	company_sister_concerns.SisterConcern,
	company_sister_concerns.Company,
	appointment.appointment_date,
	appointment.car_number,
	discussion_type.type AS DiscussionType,
	appointment.service_details,
	appointment.active_status,
	appointment.reason_for_delete,
	appointment.status 
FROM
	appointment
	JOIN client_type ON ( appointment.client_type_id = client_type.id )
	JOIN (
	SELECT
		company_sister_concerns.id  ,company_sister_concerns.title AS SisterConcern,
		companies.title AS Company 
	FROM
		company_sister_concerns
		JOIN companies ON ( companies.id = company_sister_concerns.company_id ) 
	) company_sister_concerns ON ( appointment.client_id = company_sister_concerns.id )
	JOIN discussion_type ON ( appointment.discussion_type_id = discussion_type.id ) 
WHERE
	appointment.id = ${id} 
	AND appointment.client_type_id = ${client_type_id} `;

}


// Common
let getTotalRowCount = (tableName, filterFieldObject = {}) => {
    let query = `Select count(id) as total from ${tableName} `;

    let keys = Object.keys(filterFieldObject);

    let where = "";

    if (keys.length > 0) {
        where = " where " + keys[0] + " = " + filterFieldObject[keys[0]] + " ";
        let keysLength = keys.length;

        for (let i = 1; i < keysLength; i++) {
            if (typeof (filterFieldObject[keys[i]]) == "string")
                where += "and " + keys[i] + " = '" + filterFieldObject[keys[i]] + "' ";
            else
                where += "and " + keys[i] + " = " + filterFieldObject[keys[i]] + " ";

        }
    }
    //  console.log(query + where);
    return query + where;
}

// task_discussion_types


let addTaskDiscussion_type = () => {
    return "INSERT INTO `task_discussion_types` SET ? ";
}

let getExistingDiscussionListByTask_id = () => {
    return " SELECT * from  task_discussion_types where task_id = ?";
}

let getExistingDiscussion_TypeNameAndIdListByTask_id = () => {
    return "SELECT task_discussion_types.discussion_type_id, discussion_type.type FROM `task_discussion_types` join discussion_type  On discussion_type.id = task_discussion_types.discussion_type_id where task_discussion_types.task_id = ?";
}

let deleteDiscussionTypeById = () => {
    return " DELETE From task_discussion_types where id = ?";
}

// task_vehicle_types

let addTaskVehicle_type = () => {
    return "INSERT INTO `task_vehicle_types` SET ? ";
}

let getExistingVehicleListByTask_id = () => {
    return " SELECT * from  task_vehicle_types where task_id = ?";
}


let getExistingVehicle_TypeNameAndIdListByTask_id = () => {
    return "SELECT task_vehicle_types.vehicle_type_id, vehicle_type.type FROM `task_vehicle_types` join `vehicle_type`  On vehicle_type.id = task_vehicle_types.vehicle_type_id where task_vehicle_types.task_id = ?";
}

let deleteVehicleTypeById = () => {
    return " DELETE From task_vehicle_types where id = ?";
}

// Reminder 

let addNewReminder = () => {
    return "INSERT INTO `reminders` SET ?";
}

let getRemindersListForSalesPersonByDate = (reminder_start_date, reminder_end_date) => {
    const query = `SELECT * FROM reminders where is_created_task = 0 and sales_person_id = ? and reminder_end_date >= ${reminder_start_date} and reminder_start_date <= ${reminder_end_date} `;
    return query;
}

let getRemindersClientListForAdminByDate = (reminder_start_date, reminder_end_date) => {
    const query = `SELECT * FROM reminders where is_created_task = 0 and reminder_end_date >= ${reminder_start_date} and reminder_start_date <= ${reminder_end_date}`;
    return query;
}

let getReminderById = () => {
    const query = "SELECT * FROM `reminders` WHERE id = ?"
    return query;
}
let reminderChangeIs_Creates_taskById = () => {
    return "Update `reminders` set is_created_task = 1 where id = ?";
}


// let getRemindersListForSalesPersonBySearching = (sales_person_id, search_date = "") => {

//     let query = `SELECT * FROM reminders where sales_person_id = ${sales_person_id}`;

//     let where = "";

//     if (search_date.length > 0) {
//         search_date = " and " + search_date;
//     }

//     query += where + search_date;
//     console.log(query);
//     return query;
// }



module.exports = {
    getSuperAdminList,
    getUserByPhone,
    registerUserAccount,
    registerSuperAdminAccount,
    registerSubAdminAccount,
    getSubAdminList,
    getSuperAdminDeactiveList,
    getSubAdminDeactiveList,

    registerSalesPersonAccount,
    getSalesPersonList,
    getAllSalesPersonList,
    deactiveSubAccount,
    deactiveSuperAccount,
    deactiveSalesPersonAccount,
    getSalesPersonProfileById,
    getProfileByPhoneNumber,
    getSalesPersonProfileByIdForOthers,
    getSalesPersonProfileByIdAllType,
    getSalesPersonDeleteList,

    getTaskCountBySalespersonId,
    getSuperAdminProfileById,
    getSubAdminProfileById,
    updateSubAdminProfileById,
    updateSuperAdminProfileById,
    updateSalesPersonProfileById,
    getPasswordRecoveryList,

    getClientTypeList,
    getClientTypeAllList,
    addClientType,
    getClientTypeByTypeName,
    getClientTypeByID,
    deactiveClientType,
    activeClientType,
    updateupdateClientTypeById,
    getDeactiveClientTypeList,
    getActiveClientTypeList,

    getDiscussionTypeList,
    getDiscussionTypeAllList,
    addDiscussionType,
    getDiscussionTypeByTypeName,
    deactiveDiscussionType,
    activeDiscussionType,
    updateDiscussionTypeById,
    getDiscussionTypeByID,
    getDeactiveDiscussionTypeList,
    getActiveDiscussionTypeList,

    getVehicleTypeList,
    getVehicleTypeAllList,
    addVehicleType,
    getVehicleTypeByTypeName,
    getVehicleTypeByID,
    deactiveVehicleType,
    activeVehicleType,
    updateVehicleTypeById,
    getDeactiveVehicleTypeList,
    getActiveVehicleTypeList,

    getCallTimeList,
    addCallTime,
    getCallTimeByTypeName,
    getCallTimeByID,
    deactiveCallTime,
    updateCallTimeById,

    getProspectTypeList,
    getProspectTypeAllList,
    addProspectType,
    getProspectTypeByTypeName,
    getProspectTypeByID,
    getProspectNameByID,
    deactiveProspectType,
    activeProspectType,
    updateProspectTypeById,
    getActiveProspectTypeList,
    getDeactiveProspectTypeList,

    getLeadList,
    getLeadAllList,
    getDeactiveList,
    getActiveList,
    addLead,
    getLeadByName,
    getLeadByID,
    deactiveLead,
    activeLead,
    updateLeadById,

    /// Companies
    getAllCompanyList,
    getDeactiveCompanyList,
    getActiveCompanyList,
    addCompany,
    getCompanyByTitle,
    updateCompanyByID,
    getCompanyByID,
    deactiveCompany,
    activeCompany,
    searchCompany,
    searchCompanyBySalesPerson,


    /// Sister Concern
    getAllSisterConcernList,
    getActiveSisterConcernList,
    getDeactiveSisterConcernList,
    addSisterConcern,
    getSisterConcernByTitleAndID,
    getSisterConcernByID,
    getSisterConcernAndCompanyByID,
    updateCompanySisterConcernByID,
    getSisterConcernByCompanyID,
    getSisterConcernByCompanyIDAndSalesPersonID,
    deactiveSisterConcern,
    activeSisterConcern,
    getAccessCorporateClientDataByCompanySisterConcerId,
    getAccessCompany_SisterConcern_By_SalesPersonID,
    updateAccessCorporateClientById,
    getCompanyListBySalespersonID,
    getSisterConcernListBySearching,
    getSisterConcernListBySearchingWithSisterConcern,
    getSisterConcernListBySearchingWithCompany,
    getSisterConcernListBySearchingWithCompanyID,
    getSisterConcernListBySearchingWithCompanyIDAndSalesPersonID,
    getSisterConcernListBySalespersonId,

    // Sister Concern Contact Person
    getContactDetailsByID,
    addNewContactPerson,
    updateContactPersonByID,
    getContactList,
    deleteContactPerson,

    // Access client
    addAccessCorporateClient,
    updateAccessCorporateClientBySales_person_idAndCompany_sister_concern_id,
    getAccessCorporateClientDataBycompany_sister_concern_idAndsales_person_id,
    getAccessCorporateClientDataBysales_person_id,
    getAccessClientDataBysales_person_id,
    updateAccessClientBySales_person_idAndClient_id,
    getAccessClientDataByclient_idAndsales_person_id,
    getAccessCorporateClientAllAccessHistoryByCompanySisterConcerId,
    getAccessClientHistoryByClient_id,
    getAccessClientInformationBySalesPersonID,
    deactiveAccessClient,

    getDepartmentList,
    getUserByProfileIdAndUserType,
    passwordChangeForUser,
    updateUserById,

    getClientList,
    getClientListBySearching,
    getClientListForSalesPersonBySearching,
    addClient,
    getClientByPhone,
    getClientByID,
    deactiveClient,
    activeClient,
    updateClientProfileByID,
    getPasswordRecoveryQuestionId,

    getTaskByClientId,
    addAccessClient,
    getClientListForSalesPerson,
    addTask,
    updateTaskByID,
    getIncompleteTaskListById,

    /// Appointment
    getAppointmentList,
    getDeactiveAppointmentList,
    addAppointment,
    updateAppointmentByID,
    getAppointmentByID,
    deactiveAppointment,
    searchAppointmentForActiveList,
    searchAppointmentForDeactiveList,
    getSalesPersonWiseAppointmentList,
    getSalesPersonWiseCompleteAppointmentList,
    getDetailsForNonCorporateByIDandType,
    getDetailsForCorporateByIDandType,

    // task
    getTaskByID,
    getTaskByIDAndSalespersonId,
    searchTask,
    reassignTask,
    getTaskByClientIdAndSalesPersonId,
    getSalesPersonWiseTaskList,
    getSalesPersonWiseCompleteTaskList,

    //MD Account 
    getMdSirProfileById,
    getUserByUserType,
    getTaskListBySearching,
    getTaskListBySearchingWithYear,
    getAppointmentListBySearching,
    getAppointmentListBySearchingWithYear,

    //Common
    getTotalRowCount,
    getProspectTypeWiseCount,

    // task_discussion_types
    addTaskDiscussion_type,
    getExistingDiscussionListByTask_id,
    deleteDiscussionTypeById,
    getExistingDiscussion_TypeNameAndIdListByTask_id,

    // task_vehicle_types
    addTaskVehicle_type,
    getExistingVehicleListByTask_id,
    deleteVehicleTypeById,
    getExistingVehicle_TypeNameAndIdListByTask_id,

    // Reminders
    addNewReminder,
    getRemindersListForSalesPersonByDate,
    getRemindersClientListForAdminByDate,
    getReminderById,
    reminderChangeIs_Creates_taskById

}