const connectionIntracoLPG = require('../connection/connection').connectionIntracoLPG;
const quaries = require('../query/lpg');


// Promices Method
let getList = async () => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getLeadList(), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getAllList = async () => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getLeadAllList(), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getDeactiveList = async () => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getDeactiveList(), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}


let getActiveList = async () => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getActiveList(), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let addNewLead = async (info) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.addLead(), [info], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getLeadByName = async (name) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getLeadByName(), [name], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let deactiveLead = async (id) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.deactiveLead(), [id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let activeLead = async (id) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.activeLead(), [id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let updateLeadByID = async (id, name) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.updateLeadById(), [name, id], (error, result, fields) => {
            if (error) reject(error);
            else resolve(result);
        });
    });
}

let getLeadByID = async (id) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getLeadByID(), [id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}



module.exports = {
    getList,
    getAllList,
    getDeactiveList,
    getActiveList,
    addNewLead,
    getLeadByName,
    deactiveLead,
    updateLeadByID,
    getLeadByID,
    activeLead
}