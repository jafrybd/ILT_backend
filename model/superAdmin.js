const connectionIntracoLPG = require('../connection/connection').connectionIntracoLPG;
const quaries = require('../query/lpg');


// Promices Method
let getSuperAdminList = async () => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getSuperAdminList(), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getSuperAdminDeactiveList = async () => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getSuperAdminDeactiveList(), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let addNewSuperAdmin = async (info) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.registerSuperAdminAccount(), [info], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let deactiveAccount = async (id) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.deactiveSuperAccount(), [id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getProfileById = async (id) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getSuperAdminProfileById(), [id], (error, result, fields) => {
            if (error) reject(error);
            else resolve(result);
        });
    });
}



let updateProfileById = async (id, data) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.updateSuperAdminProfileById(data), [id], (error, result, fields) => {
            if (error) reject(error);
            else resolve(result);
        });
    });
}


module.exports = {
    getSuperAdminList,
    addNewSuperAdmin,
    deactiveAccount,
    getProfileById,
    updateProfileById,
    getSuperAdminDeactiveList
}