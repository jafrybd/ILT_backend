const connectionIntracoLPG = require('../connection/connection').connectionIntracoLPG;
const quaries = require('../query/lpg');

let addNew = async (info) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.addTaskDiscussion_type(), [info], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}


let getExistingDiscussionListByTask_id = async (task_id) => {  // get only id, task_id, discussion_type_id
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getExistingDiscussionListByTask_id(), [task_id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getExistingDiscussion_TypeNameAndIdListByTask_id = async (task_id) => { // get only id, discussion_type_id, discussion_type_name
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getExistingDiscussion_TypeNameAndIdListByTask_id(), [task_id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}



let deleteDiscussionTypeById = async (id) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.deleteDiscussionTypeById(), [id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

module.exports = {
    addNew,
    getExistingDiscussionListByTask_id,
    deleteDiscussionTypeById,
    getExistingDiscussion_TypeNameAndIdListByTask_id  
}