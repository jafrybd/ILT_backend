const connectionIntracoLPG = require('../connection/connection').connectionIntracoLPG;
const quaries = require('../query/lpg');

// Promices Method
let getListByDate = async (type, salesPersonId, startDay, endDate) => {
    return new Promise((resolve, reject) => {

        if (type == 4) {
            connectionIntracoLPG.query(quaries.getRemindersListForSalesPersonByDate(startDay, endDate), [salesPersonId], (error, result, fields) => {
                if (error) reject(error)
                else resolve(result)

            });
        } else {
            connectionIntracoLPG.query(quaries.getRemindersClientListForAdminByDate(startDay, endDate), [], (error, result, fields) => {
                if (error) reject(error)
                else resolve(result)
            });
        }

    });
}


let addNewReminder = async (info) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.addNewReminder(), [info], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getReminderById = async (id) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getReminderById(), [id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let changeIs_Creates_taskById = async (reminderId) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.reminderChangeIs_Creates_taskById(), [reminderId], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

// No Need This method
// let getListBySearching = async (type, salesPersonId, search_date) => {
//     return new Promise((resolve, reject) => {
//         //console.log(search_date.from_date);
//         if (type == 4) {
//             connectionIntracoLPG.query(quaries.getRemindersListForSalesPersonBySearching(salesPersonId, search_date), (error, result, fields) => {
//                 if (error) reject(error)
//                 else resolve(result)

//             });
//         }
//         /* else {
//                    connectionIntracoLPG.query(quaries.getRemindersClientListForAdmin(), (error, result, fields) => {
//                        if (error) reject(error)
//                        else resolve(result)
//                    });
//                } */

//     });
// }


module.exports = {
    getListByDate,
    addNewReminder,
    getReminderById,
    changeIs_Creates_taskById
    // getListBySearching

}