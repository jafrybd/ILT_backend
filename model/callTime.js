const connectionIntracoLPG = require('../connection/connection').connectionIntracoLPG;
const quaries = require('../query/lpg');


// Promices Method
let getList = async () => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getCallTimeList(), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}



let addNewCallTime = async (info) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.addCallTime(), [info], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getCallTimeByType = async (type) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getCallTimeByTypeName(), [type], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let deactiveCallTime = async (id) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.deactiveCallTime(), [id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let updateCallTimeByID = async (id, type) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.updateCallTimeById(), [type, id], (error, result, fields) => {
            if (error) reject(error);
            else resolve(result);
        });
    });
}

let getCallTimeByID = async (id) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getCallTimeByID(), [id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}



module.exports = {
    getList,
    addNewCallTime,
    getCallTimeByType,
    deactiveCallTime,
    updateCallTimeByID,
    getCallTimeByID
}