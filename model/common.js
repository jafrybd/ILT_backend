const connectionIntracoLPG = require('../connection/connection').connectionIntracoLPG;
const quaries = require('../query/lpg');


// Promices Method

let getUserByPhone = async (phone) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getUserByPhone(), [phone], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}


let addNewUser = async (info) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.registerUserAccount(), [info], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getUserByProfileIdAndUserType = async (id, type) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getUserByProfileIdAndUserType(), [id, type], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let passwordChangeForUser = async (user_id, password) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.passwordChangeForUser(), [password, user_id], (error, result, fields) => {
            // connectionIntracoLPGend();
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getUserByUserType = async (type) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getUserByUserType(), type, (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getTotalCountTableRow = async (tableName, date = 0, extraWhereFieldObject = {}) => { //search from IntracoLPG Database

    let field = {};

    if (date !== 0 && tableName == 'appointment') {
        field.appointment_date = date;
    } else if (date !== 0 && tableName == 'task') {
        field.call_date = date;
    }

    const keys = Object.keys(extraWhereFieldObject);
    let keysLength = keys.length;

    if (keysLength > 0) {
        for (let i = 0; i < keysLength; i++) {
            field[keys[i]] = extraWhereFieldObject[keys[i]];
        }
    }

    // return field;
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getTotalRowCount(tableName, field), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}


let updateUserById = async (user_id, update_data) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.updateUserById(update_data), [user_id], (error, result, fields) => {
            if (error) resolve([]);
            else resolve(result);
        });
    });
}

let testCallStoredPro = async () => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query("call test()", (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}


module.exports = {
    getUserByPhone,
    addNewUser,
    getUserByProfileIdAndUserType,
    passwordChangeForUser,
    getUserByUserType,
    getTotalCountTableRow,
    updateUserById,
    testCallStoredPro
}