const connectionIntracoLPG = require('../connection/connection').connectionIntracoLPG;
const quaries = require('../query/lpg');

let addNew = async (info) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.addAccessCorporateClient(), [info], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getAccessCorporateClientDataByCompanySisterConcerId = async (CompanySisterConcerId) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getAccessCorporateClientDataByCompanySisterConcerId(), [CompanySisterConcerId], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getAccessCorporateClientAllAccessHistoryByCompanySisterConcerId = async (CompanySisterConcerId) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getAccessCorporateClientAllAccessHistoryByCompanySisterConcerId(), [CompanySisterConcerId], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getAccessCorporateClientDataBycompany_sister_concern_idAndsales_person_id = async (company_sister_concern_id, sales_person_id) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getAccessCorporateClientDataBycompany_sister_concern_idAndsales_person_id(), [company_sister_concern_id, sales_person_id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getAccessCorporateClientDataBysales_person_id = async (sales_person_id) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getAccessCorporateClientDataBysales_person_id(), [sales_person_id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}


let updateAccessCorporateClientById = async (id, updateData) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.updateAccessCorporateClientById(updateData), [id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getCompanyListBySalespersonID = async (salesPersonId) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getCompanyListBySalespersonID(salesPersonId), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}


let updateAccessCorporateClientBySales_person_idAndCompany_sister_concern_id = async (sales_person_id, company_sister_concern_id, updateData) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.updateAccessCorporateClientBySales_person_idAndCompany_sister_concern_id(updateData), [sales_person_id, company_sister_concern_id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getAccessCompany_SisterConcern_By_SalesPersonID = async (sales_person_id) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getAccessCompany_SisterConcern_By_SalesPersonID(sales_person_id), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}


module.exports = {
    addNew,
    getAccessCorporateClientDataByCompanySisterConcerId,
    updateAccessCorporateClientById,
    getCompanyListBySalespersonID,
    updateAccessCorporateClientBySales_person_idAndCompany_sister_concern_id,
    getAccessCorporateClientDataBycompany_sister_concern_idAndsales_person_id,
    getAccessCorporateClientDataBysales_person_id,
    getAccessCorporateClientAllAccessHistoryByCompanySisterConcerId,
    getAccessCompany_SisterConcern_By_SalesPersonID

}