const connectionIntracoLPG = require('../connection/connection').connectionIntracoLPG;
const quaries = require('../query/lpg');


// Promices Method
let getList = async () => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getProspectTypeList(), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getAllList = async () => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getProspectTypeAllList(), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let addNewProspectType = async (info) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.addProspectType(), [info], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getProspectTypeByType = async (type) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getProspectTypeByTypeName(), [type], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let deactiveProspectType = async (id) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.deactiveProspectType(), [id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}


let activeProspectType = async (id) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.activeProspectType(), [id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let updateProspectTypeByID = async (id, type) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.updateProspectTypeById(), [type, id], (error, result, fields) => {
            if (error) reject(error);
            else resolve(result);
        });
    });
}

let getProspectTypeByID = async (id) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getProspectTypeByID(), [id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}


let getProspectNameByID = async (id) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getProspectNameByID(), [id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}


let getDeactiveProspectTypeList = async () => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getDeactiveProspectTypeList(), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}


let getActiveProspectTypeList = async () => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getActiveProspectTypeList(), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}


module.exports = {
    getList,
    addNewProspectType,
    getProspectTypeByType,
    deactiveProspectType,
    activeProspectType,
    updateProspectTypeByID,
    getProspectTypeByID,
    getProspectNameByID,
    getDeactiveProspectTypeList,
    getActiveProspectTypeList,
    getAllList

}