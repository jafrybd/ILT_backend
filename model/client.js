const connectionIntracoLPG = require('../connection/connection').connectionIntracoLPG;
const quaries = require('../query/lpg');


// Promices Method
let getList = async (type, salesPersonId) => {
    return new Promise((resolve, reject) => {

        if (type == 4) {
            connectionIntracoLPG.query(quaries.getClientListForSalesPerson(), [salesPersonId], (error, result, fields) => {
                if (error) reject(error)
                else resolve(result)
            });
        } else {
            connectionIntracoLPG.query(quaries.getClientList(), (error, result, fields) => {
                if (error) reject(error)
                else resolve(result)
            });
        }

    });
}



let getListBySearching = async (userType, salesPersonId, client_name) => {
    return new Promise((resolve, reject) => {

        if (userType == 4) {
            connectionIntracoLPG.query(quaries.getClientListForSalesPersonBySearching(salesPersonId, client_name), (error, result, fields) => {
                if (error) reject(error)
                else resolve(result)
            });
        } else {
            connectionIntracoLPG.query(quaries.getClientListBySearching(client_name), (error, result, fields) => {
                if (error) reject(error)
                else resolve(result)
            });
        }

    });
}

let addNewClient = async (info) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.addClient(), [info], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getClientByPhone = async (phone_number) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getClientByPhone(), [phone_number, phone_number], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let deactiveClient = async (id) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.deactiveClient(), [id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let updateClientByID = async (id, data) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.updateClientProfileByID(data), [id], (error, result, fields) => {
            if (error) reject(error);
            else resolve(result);
        });
    });
}

let getClientByID = async (id) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getClientByID(), [id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}



module.exports = {
    getList,
    addNewClient,
    getClientByPhone,
    deactiveClient,
    updateClientByID,
    getClientByID,
    getListBySearching
}