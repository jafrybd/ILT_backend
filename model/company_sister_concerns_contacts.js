const connectionIntracoLPG= require('../connection/connection').connectionIntracoLPG;
const quaries = require('../query/lpg');




let getContactDetailsByID = async (id) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getContactDetailsByID(), [id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}


let addNewContactPerson = async (info) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.addNewContactPerson(), [info], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}



let getContactList = async (company_sister_concern_id) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getContactList(company_sister_concern_id), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}


let deleteContactPerson = async (id) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.deleteContactPerson(), [id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}


let updateContactPersonByID = async (id, updateData) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.updateContactPersonByID(updateData), [id], (error, result, fields) => {
            if (error) reject(error);
            else resolve(result);
        });
    });
}






module.exports = {
    getContactDetailsByID,
    addNewContactPerson,
    updateContactPersonByID,
    getContactList,
    deleteContactPerson
    

}