const connectionIntracoLPG = require('../connection/connection').connectionIntracoLPG;
const quaries = require('../query/lpg');


// Promices Method
let getSubAdminList = async () => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getSubAdminList(), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getSubAdminDeactiveList = async () => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getSubAdminDeactiveList(), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let addNewSubAdmin = async (info) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.registerSubAdminAccount(), [info], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let deactiveAccount = async (id) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.deactiveSubAccount(), [id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getProfileById = async (id) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getSubAdminProfileById(), [id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let updateProfileById = async (id, data) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.updateSubAdminProfileById(data), [id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}


module.exports = {
    getSubAdminList,
    addNewSubAdmin,
    deactiveAccount,
    getProfileById,
    updateProfileById,
    getSubAdminDeactiveList
}