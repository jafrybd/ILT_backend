const connectionIntracoLPG = require('../connection/connection').connectionIntracoLPG;
const quaries = require('../query/lpg');


// Promices Method
let getList = async () => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getClientTypeList(), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}


let getAllList = async () => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getClientTypeAllList(), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let addNewClientType = async (info) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.addClientType(), [info], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getClientTypeByType = async (type) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getClientTypeByTypeName(), [type], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let deactiveClientType = async (id) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.deactiveClientType(), [id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let activeClientType = async (id) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.activeClientType(), [id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let updateClientTypeByID = async (id, type) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.updateupdateClientTypeById(), [type, id], (error, result, fields) => {
            if (error) reject(error);
            else resolve(result);
        });
    });
}

let getClientTypeByID = async (id) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getClientTypeByID(), [id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getDeactiveClientTypeList = async () => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getDeactiveClientTypeList(), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}



let getActiveClientTypeList = async () => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getActiveClientTypeList(), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}




module.exports = {
    getList,
    getAllList,
    addNewClientType,
    getClientTypeByType,
    deactiveClientType,
    activeClientType,
    updateClientTypeByID,
    getClientTypeByID,
    getDeactiveClientTypeList,
    getActiveClientTypeList
}