const connectionIntracoLPG  = require('../connection/connection').connectionIntracoLPG;
const quaries = require('../query/lpg');


// Promices Method


let addNew = async (info) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.addAccessClient(), [info], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getAccessClientDataBysales_person_id = async (sales_person_id) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getAccessClientDataBysales_person_id(), [sales_person_id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getAccessClientInformationBySalesPersonID = async (sales_person_id) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getAccessClientInformationBySalesPersonID(sales_person_id), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getAccessClientHistoryByClient_id = async (client_id) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getAccessClientHistoryByClient_id(), [client_id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let updateAccessClientBySales_person_idAndClient_id = async (sales_person_id, client_id, updateData) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.updateAccessClientBySales_person_idAndClient_id(updateData), [sales_person_id, client_id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}


let getAccessClientDataByclient_idAndsales_person_id = async (client_id, sales_person_id) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getAccessClientDataByclient_idAndsales_person_id(), [client_id, sales_person_id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let deactiveAccessClient  = async (acces_client_id) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.deactiveAccessClient(), [acces_client_id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}


module.exports = {
    addNew,
    getAccessClientDataBysales_person_id,
    updateAccessClientBySales_person_idAndClient_id,
    getAccessClientDataByclient_idAndsales_person_id,
    getAccessClientHistoryByClient_id,
    getAccessClientInformationBySalesPersonID,
    deactiveAccessClient
}