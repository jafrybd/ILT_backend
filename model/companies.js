const connectionIntracoLPG = require('../connection/connection').connectionIntracoLPG;
const quaries = require('../query/lpg');


// Promices Method
let getAllList = async () => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getAllCompanyList(), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

 let getDeactiveCompanyList = async () => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getDeactiveCompanyList(), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}


let getActiveCompanyList = async () => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getActiveCompanyList(), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let addNewCompany = async (info) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.addCompany(), [info], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getCompanyByTitle = async (company_title) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getCompanyByTitle(), [company_title], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let updateCompanyByID = async (id, title,user_id) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.updateCompanyByID(), [title,user_id,id], (error, result, fields) => {
            if (error) reject(error);
            else resolve(result);
        });
    });
}

let getCompanyByID = async (id) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getCompanyByID(), [id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}



let deactiveCompany = async (id) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.deactiveCompany(), [id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let activeCompany = async (id) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.activeCompany(), [id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}



let getCompanyListBySearching = async (title) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.searchCompany(title), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}



let getCompanyListBySearchingBySalesPerson = async (sales_person_id,title) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.searchCompanyBySalesPerson(sales_person_id,title), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}



module.exports = {
    getAllList,
    getDeactiveCompanyList,
    getActiveCompanyList,
    addNewCompany,
    getCompanyByTitle,
    getCompanyByID,
    updateCompanyByID,
    deactiveCompany,
    activeCompany,
    getCompanyListBySearching,
    getCompanyListBySearchingBySalesPerson
    
}