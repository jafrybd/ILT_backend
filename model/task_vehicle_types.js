const connectionIntracoLPG = require('../connection/connection').connectionIntracoLPG;
const quaries = require('../query/lpg');

let addNew = async (info) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.addTaskVehicle_type(), [info], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getExistingVehicleListByTask_id = async (task_id) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getExistingVehicleListByTask_id(), [task_id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}


let getExistingVehicle_TypeNameAndIdListByTask_id = async (task_id) => { // get only id, discussion_type_id, discussion_type_name
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getExistingVehicle_TypeNameAndIdListByTask_id(), [task_id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}



let deleteVehicleTypeById = async (id) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.deleteVehicleTypeById(), [id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}


module.exports = {
    addNew  ,
    getExistingVehicleListByTask_id,
    deleteVehicleTypeById,
    getExistingVehicle_TypeNameAndIdListByTask_id
}