const express = require("express");
const bodyParser = require('body-parser');
const empty = require('is-empty');
const lpg = require('./router/lpg');
const mdSir = require('./router/md_sir');
const app = express();
var cors = require('cors');
const api_version = 2.1;
const port = process.env.PORT || 3001;
global.config = require('./jwt/config/config');

var url = "";

app.use(bodyParser.json());
// Check Request is JSON Formate
app.use((err, req, res, next) => {
    if (err) {
        res.status(400).send(
            {
                'status': 400,
                'message': "error parsing data, Request is not a JSON Formate.",
                "error": true,
                "success": true
            }
        )
    } else {
        next()
    }
});

app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors());

app.get([url + '/', url + "/home"], (req, res) => {
    return res.send({
        "Hork": "It's Work.",
        "Api v": api_version
    });
});

app.use(url + '/mdSir', mdSir);

app.use(url + '/intraco/lpg', lpg);

app.get('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "error": true
    })
});

app.post('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "error": true
    })
});

app.listen(port, () => {
    console.log(`App running port ${port}`);
});



